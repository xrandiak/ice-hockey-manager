# Match Service

This branch (and module) contain first implementation of Match Service for Ice Hockey Manager.
Persistence doesn't work properly yet and the service lacks.. well a lot of..

But tests run. 

## Instructions
Brave enough to run? Well alrighty. Do:
```
mvn clean install
mvn spring-boot:run

```

### M2
Whole app was rewritten to accomodate feedback from M1 review.

Containerization implemented. After successfull build:

```
docker compose up -d

```
or 

```
podman compose up -d

```

## Author

Andrej Dravecky, 445432@muni.cz

### DTO class diagram : 
![Match Service Dto Class Diagram](../docs/match_service_dtos_class_diagram.png "Use Case Diagram")

