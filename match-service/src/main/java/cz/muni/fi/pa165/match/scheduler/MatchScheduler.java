package cz.muni.fi.pa165.match.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cz.muni.fi.pa165.match.facade.MatchFacade;
import cz.muni.fi.pa165.match.service.MatchService;

@Component
public class MatchScheduler {
    private final MatchFacade matchFacade;

    @Autowired
    public MatchScheduler(MatchFacade matchFacade) {
        this.matchFacade = matchFacade;
    }

    @Scheduled(fixedRateString = "${config.match-scheduling-timeout}")
    public void schedule() {
        matchFacade.scheduleMatches();
        matchFacade.resolveMatches();
    }
}