package cz.muni.fi.pa165.match.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.annotation.Secured;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import cz.muni.fi.pa165.match.configuration.Auth;
import cz.muni.fi.pa165.match.data.mappers.MatchMapper;
import cz.muni.fi.pa165.match.facade.MatchFacade;
import cz.muni.fi.pa165.match.service.MatchService;
import cz.muni.fi.pa165.match.generated.api.MatchControllerApiDelegate;
import cz.muni.fi.pa165.match.generated.model.MatchDto;
import cz.muni.fi.pa165.match.generated.model.ActionDto;
import cz.muni.fi.pa165.match.generated.model.MatchStateDto;


@RestController
public class MatchController implements MatchControllerApiDelegate {
    private final MatchFacade matchFacade;
    private final MatchMapper mapper;

    
    @Autowired
    public MatchController(MatchFacade matchFacade, MatchMapper mapper) {
        this.matchFacade = matchFacade;
        this.mapper = mapper;
    }

    @Override
    public ResponseEntity<MatchDto> getMatchById(Long matchId) {
        return new ResponseEntity<>(
            mapper.toDto(matchFacade.getMatchById(matchId)),
            HttpStatus.OK
        );
    }

    @Override
    //@PreAuthorize("hasAnyAuthority('scope_admin')")
    public ResponseEntity<MatchDto> organizeMatch(MatchDto match) {
        var newMatch = matchFacade.createMatch(mapper.fromDto(match));
        return new ResponseEntity<>(mapper.toDto(newMatch), HttpStatus.OK);
    }

    @Override
    //@PreAuthorize("hasAnyAuthority('scope_user')")
    public ResponseEntity<List<MatchDto>> getMatchesByState(MatchStateDto state) {
        // TODO: Replace 1L with the actual user id.
        var matches = matchFacade.getMatchesByState(mapper.fromDto(state));
        return new ResponseEntity<>(
                mapper.toDto(matches),
                HttpStatus.OK
            );
    }

    @Override
    //@PreAuthorize("hasAnyAuthority('scope_user')")
    public ResponseEntity<List<MatchDto>> getUserMatchesByState(Long userId, MatchStateDto state) {
        var matches = matchFacade.getUserMatchesByState(userId, mapper.fromDto(state));
        return new ResponseEntity<>(
                mapper.toDto(matches),
                HttpStatus.OK
            );
    }

    @Override
    //@PreAuthorize("hasAnyAuthority('scope_user')")
    public ResponseEntity<MatchDto> proposeMatch(Long challengerId, Long opponentId) {
        // TODO: Replace with proper auth
        var match = matchFacade.proposeMatch(challengerId, opponentId);
        return new ResponseEntity<>(
            mapper.toDto(match),
            HttpStatus.OK
        );
    }

    @Override
    //@PreAuthorize("hasAnyAuthority('scope_user')")
    public ResponseEntity<Void> reactToMatchProposition(Long id, ActionDto action) {
        matchFacade.reactToMatch(id, action == ActionDto.ACCEPT);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

