package cz.muni.fi.pa165.match.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cz.muni.fi.pa165.match.data.enums.MatchState;
import cz.muni.fi.pa165.match.data.model.Match;

import java.util.List;

@Repository
public interface MatchRepository extends JpaRepository<Match, Long> {
    List<Match> findByOpponentAndState(Long id, MatchState state);

    List<Match> findByChallengerAndState(Long id, MatchState state);

    @Query("SELECT m FROM Match m WHERE state = :state AND (m.challenger = :id OR m.opponent = :id)")
    List<Match> findByUserAndState(Long id, MatchState state);

    @Query("SELECT m FROM Match m WHERE m.challenger = :id OR m.opponent = :id")
    List<Match> findByUser(Long id);

    @Query("SELECT m FROM Match m WHERE scheduled > CURRENT_TIMESTAMP")
    List<Match> findMatchesToResolve();

    List<Match> findByState(MatchState state);
}
