package cz.muni.fi.pa165.match.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import cz.muni.fi.pa165.match.generated.model.TeamPowerDto;

@Component
@DependsOn("webClientBuilder")
@FeignClient(name = "team-service-client",
        url = "${config.team-service-url}"
)
public interface TeamService {
    /**
     * Endpoint to get team power
     *
     * @param teamId The ID of the team to get power
     */
    @GetMapping("/teams/team-agents/team-power/{teamId}")
    TeamPowerDto getTeamPower(@PathVariable Long teamId);

    @GetMapping("/teams/team-agents/teams-power")
    List<TeamPowerDto> getTeamsPower(@RequestBody List<Long> teamIds);
}
