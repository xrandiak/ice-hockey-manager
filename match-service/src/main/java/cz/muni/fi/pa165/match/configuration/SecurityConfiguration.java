/* 
package cz.muni.fi.pa165.match.configuration;

import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.security.config.Customizer;
import org.springframework.http.HttpMethod;

//@Configuration
//@EnableWebSecurity
public class SecurityConfiguration {
    @Value("${config.enable-security}")
    private boolean enableSecurity;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers(HttpMethod.GET, "/matches/**").hasAuthority("scope_admin")
                        .requestMatchers(HttpMethod.GET, "/match").hasAuthority("scope_user")
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
        ;
        return http.build();
//        http
//            .authorizeHttpRequests(x -> x
//                        // allow anonymous access to listed URLs
////                        .requestMatchers(enableSecurity ? "/matches" : "/**").permitAll()
//                        .requestMatchers(HttpMethod.GET, "/matches/**").hasAuthority("SCOPE_test_1")
//                        .requestMatchers(HttpMethod.GET, "/match").hasAuthority("SCOPE_test_1")
//                        .anyRequest().permitAll()
//                        //.requestMatchers("/**").permitAll()
//                        // all other requests must be authenticated
////                        .anyRequest().permitAll()
//                )
//                .csrf(c -> c
//                        //set CSRF token cookie "XSRF-TOKEN" with httpOnly=false that can be read by JavaScript
//                        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
//                        //replace the default XorCsrfTokenRequestAttributeHandler with one that can use value from the cookie
//                        .csrfTokenRequestHandler(new CsrfTokenRequestAttributeHandler())
//                )
//        ;
//        return http.build();
    }

    /**
     * Add security definitions to generated openapi.yaml.
     *//*
    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> {
            openApi.getComponents()
                    .addSecuritySchemes("MUNI",
                            new SecurityScheme()
                                    .type(SecurityScheme.Type.OAUTH2)
                                    .description("get access token with OAuth 2 Authorization Code Grant")
                                    .flows(new OAuthFlows()
                                            .authorizationCode(new OAuthFlow()
                                                    .authorizationUrl("https://oidc.muni.cz/oidc/authorize")
                                                    .tokenUrl("https://oidc.muni.cz/oidc/token")
                                                    .scopes(new Scopes()
                                                            .addString("test_1", "manage matches")
                                                            .addString("", "propose and react to matches")
                                                    )
                                            )
                                    )
                    )
                    .addSecuritySchemes("Bearer",
                            new SecurityScheme()
                                    .type(SecurityScheme.Type.HTTP)
                                    .scheme("bearer")
                                    .description("provide a valid access token")
                    )
                    ;

        };
    }
}*/

