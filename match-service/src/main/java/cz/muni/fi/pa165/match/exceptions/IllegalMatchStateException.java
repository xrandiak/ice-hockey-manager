package cz.muni.fi.pa165.match.exceptions;

public class IllegalMatchStateException extends RuntimeException {
    public IllegalMatchStateException(String message) {
        super(message);
    }
}
