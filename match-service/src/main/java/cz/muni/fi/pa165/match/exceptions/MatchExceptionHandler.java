package cz.muni.fi.pa165.match.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import cz.muni.fi.pa165.match.generated.model.ErrorDto;

@ControllerAdvice
public class MatchExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    protected ResponseEntity<ErrorDto> handleBadRequest(RuntimeException ex) {
        ErrorDto error = new ErrorDto().message(ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ MatchNotFoundException.class, UserNotFoundException.class })
    protected ResponseEntity<ErrorDto> handleNotFound(RuntimeException ex) {
        ErrorDto error = new ErrorDto().message(ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ IllegalMatchStateException.class })
    protected ResponseEntity<ErrorDto> handleIllegalMatchState(RuntimeException ex) {
        ErrorDto error = new ErrorDto().message(ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
