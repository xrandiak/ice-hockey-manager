package cz.muni.fi.pa165.match;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FullyQualifiedAnnotationBeanNameGenerator;


@ComponentScan(
    basePackages = {"cz.muni.fi.pa165.match", "cz.muni.fi.pa165.match.generated"},
    nameGenerator = FullyQualifiedAnnotationBeanNameGenerator.class
)
@SpringBootApplication
@EnableFeignClients
@ImportAutoConfiguration({FeignAutoConfiguration.class})
public class MatchApp {
    public static void main(String[] args) {
        SpringApplication.run(MatchApp.class, args);
    }
}