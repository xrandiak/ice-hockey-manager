package cz.muni.fi.pa165.match.data.enums;

public enum MatchState {
    PROPOSED("proposed"),
    ACCEPTED("accepted"),
    REJECTED("rejected"),
    SCHEDULED("scheduled"),
    RESOLVED("resolved"),
    //CANCELLED("cancelled"),
    ALL("all");

    MatchState(String value) {}
}
