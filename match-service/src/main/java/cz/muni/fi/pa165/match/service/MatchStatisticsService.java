package cz.muni.fi.pa165.match.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import cz.muni.fi.pa165.match.generated.model.MatchDto;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

@Component
@DependsOn("webClientBuilder")
@FeignClient(name = "match-statistics-service-client",
        url = "${config.match-statistics-service-url}"
)
public interface MatchStatisticsService {
    /**
     * Endpoint to get team power
     *
     * @param teamId The ID of the team to get power
     */
    @PostMapping("/match")
    void postMatchResult(@RequestBody MatchDto match);

    @PostMapping("/matches")
    void postMatchesResults(@RequestBody List<MatchDto> match);
}
