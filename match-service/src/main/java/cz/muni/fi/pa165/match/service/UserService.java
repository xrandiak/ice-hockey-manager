package cz.muni.fi.pa165.match.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import cz.muni.fi.pa165.match.generated.model.UserDto;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

@Component
@DependsOn("webClientBuilder")
@FeignClient(name = "user-service-client",
        url = "${config.user-service-url}"
)
public interface UserService {
    /**
     * Endpoint to get a user by his/her id.
     *
     * @param userId The id of the user to get.
     * @return The user with the given id.
     */
    @GetMapping("/user")
    UserDto getUser();

    @GetMapping("/users/{userId}")
    UserDto getUserById(@PathVariable Long userId);

    @GetMapping("/users")
    List<UserDto> getUsersByIds(@RequestBody List<Long> userId);
}
