package cz.muni.fi.pa165.match.data.model;

import java.io.Serializable;
import java.time.OffsetDateTime;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import cz.muni.fi.pa165.match.data.enums.MatchResult;
import cz.muni.fi.pa165.match.data.enums.MatchState;
import jakarta.persistence.*;
import lombok.*;

@Data
@Entity(name = "Match")
@Getter @Setter 
@AllArgsConstructor @NoArgsConstructor
@Builder 
public class Match implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(nullable = true)
    private Long challenger;

    @Column(nullable = true)
    private Long opponent;

    @Column(name = "updated", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Builder.Default
    private final OffsetDateTime updated = OffsetDateTime.now();

    @Column(name = "scheduled", nullable = true)
    private OffsetDateTime scheduled;

    @JdbcTypeCode(SqlTypes.VARCHAR)
    @Column(name = "result", nullable = true)
    private MatchResult result;

    @JdbcTypeCode(SqlTypes.VARCHAR)
    @Column(name = "state", nullable = false)
    private MatchState state;
}
