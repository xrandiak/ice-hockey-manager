package cz.muni.fi.pa165.match.data.mappers;

import java.time.OffsetDateTime;
import java.util.List;

import org.mapstruct.Mapper;

import cz.muni.fi.pa165.match.data.enums.MatchResult;
import cz.muni.fi.pa165.match.data.enums.MatchState;
import cz.muni.fi.pa165.match.data.model.*;
import cz.muni.fi.pa165.match.generated.model.MatchDto;
import cz.muni.fi.pa165.match.generated.model.MatchResultDto;
import cz.muni.fi.pa165.match.generated.model.MatchStateDto;

import org.openapitools.jackson.nullable.JsonNullable;

@Mapper(componentModel = "spring")
public interface MatchMapper {
    public MatchState fromDto(MatchStateDto match);
    public MatchStateDto toDto(MatchState match);

    public MatchResult fromDto(MatchResultDto match);
    public MatchResultDto toDto(MatchResult match);

    public Match fromDto(MatchDto match);
    public MatchDto toDto(Match match);

    List<Match> fromDto(List<MatchDto> matchDtos);
    List<MatchDto> toDto(List<Match> matches);

    default public JsonNullable<OffsetDateTime> toNullable(OffsetDateTime dateTime) {
        if (dateTime == null) {
            return JsonNullable.undefined();
        }
        return JsonNullable.of(dateTime);
    }
    default public OffsetDateTime toNullable(JsonNullable<OffsetDateTime> dateTime) {
        if (dateTime.isPresent()) {
            return dateTime.get();
        }
        return null;
    }
}

