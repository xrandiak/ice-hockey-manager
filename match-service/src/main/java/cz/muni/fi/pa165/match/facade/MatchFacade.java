package cz.muni.fi.pa165.match.facade;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cz.muni.fi.pa165.match.configuration.Auth;
import cz.muni.fi.pa165.match.data.enums.MatchResult;
import cz.muni.fi.pa165.match.data.enums.MatchState;
import cz.muni.fi.pa165.match.data.mappers.MatchMapper;
import cz.muni.fi.pa165.match.data.model.Match;
import cz.muni.fi.pa165.match.generated.model.MatchDto;
import cz.muni.fi.pa165.match.generated.model.TeamPowerDto;
import cz.muni.fi.pa165.match.service.MatchService;
import cz.muni.fi.pa165.match.service.MatchStatisticsService;
import cz.muni.fi.pa165.match.service.TeamService;
import cz.muni.fi.pa165.match.service.UserService;


@Service
public class MatchFacade {
    private final MatchService matchService;
    private final UserService userService;
    private final TeamService teamService;
    private final MatchStatisticsService matchStatisticsService;
    private final MatchMapper  mapper;

    @Value("${config.testing}")
    private boolean testing;

    
    @Autowired
    public MatchFacade(MatchService matchService, UserService userService, TeamService teamService, MatchStatisticsService matchStatisticsService, MatchMapper mapper) {
        this.matchService = matchService;
        this.userService = userService;
        this.teamService = teamService;
        this.matchStatisticsService = matchStatisticsService;
        this.mapper = mapper;
    }

    public List<Match> getUserMatchesByState(Long id, MatchState state) {
        return matchService.getUserMatchesByState(id, state);
    }

    public List<Match> getMatchesByState(MatchState state) {
        return matchService.getMatchesByState(state);
    }

    public Match getMatchById(Long id) {
        return matchService.getById(id);
    }

    public Match createMatch(Match match) {
        return matchService.createMatch(match);
    }

    public Match proposeMatch(Long challengerId, Long opponentId) {
        //Long challengerId;

        if (this.testing) {
            challengerId = Auth.getUser();
        } else {
            var challenger = userService.getUserById(challengerId);
            if (challenger == null) {
                throw new IllegalArgumentException("User does not exist");
            }
            challengerId = challenger.getId();
            
            var opponent = userService.getUserById(opponentId);
            if (opponent == null) {
                throw new IllegalArgumentException("User with id " + opponentId + " does not exist");
            }
        }

        return matchService.proposeMatch(challengerId, opponentId);
    }

    public Match reactToMatch(Long matchId, boolean accepted) {
        return matchService.reactToMatch(matchId, accepted);
    }
    
    public void scheduleMatches() {
        var toSchedule = matchService.getMatchesByState(MatchState.ACCEPTED);
        for (Match match : toSchedule) {
            match.setScheduled(OffsetDateTime.now().plusMinutes(5));
            match.setState(MatchState.SCHEDULED);
            matchService.updateMatch(match);
        }
    }

    public void resolveMatches() {
        var toResolve = matchService.getMatchesToResolve();
        var users = new HashSet<Long>();
        toResolve.forEach(m -> { users.add(m.getChallenger()); users.add(m.getOpponent()); });

        var userProfiles = userService.getUsersByIds(users.stream().toList());
        var teamPowers = teamService.getTeamsPower(userProfiles.stream().map(p -> p.getTeamId()).toList());

        var teamPowerMap = new HashMap<Long, TeamPowerDto>();
        int index = 0;
        for (var user : userProfiles) {
            teamPowerMap.put(user.getId(), teamPowers.get(index++));
        }


        var results = new ArrayList<MatchDto>();
        for (Match match : toResolve) {
            match.setResult(computeResult(
                teamPowerMap.get(match.getChallenger()),
                teamPowerMap.get(match.getOpponent())
            ));
            match.setState(MatchState.RESOLVED);
            results.add(mapper.toDto(matchService.updateMatch(match)));
        }
        matchStatisticsService.postMatchesResults(results);
    }

    private MatchResult computeResult(TeamPowerDto challengerPower, TeamPowerDto opponentPower) {

        var challengerPlayScore = challengerPower.getDefense() - opponentPower.getOffense();
        var opponentPlayScore = opponentPower.getDefense() - challengerPower.getOffense();

        var score = challengerPlayScore - opponentPlayScore;

        if (score > 5) {
            return MatchResult.CHALLENGER_WIN;
        } else if (score < -5) {
            return MatchResult.OPPONENT_WIN;
        } else if (score > 0) {
            return MatchResult.CHALLENGER_WIN_OVERTIME;
        } else {
            return MatchResult.OPPONENT_WIN_OVERTIME;
        }
    }
}