package cz.muni.fi.pa165.match.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.muni.fi.pa165.match.data.enums.MatchResult;
import cz.muni.fi.pa165.match.data.enums.MatchState;
import cz.muni.fi.pa165.match.data.model.Match;
import cz.muni.fi.pa165.match.data.repository.MatchRepository;
import cz.muni.fi.pa165.match.exceptions.IllegalMatchStateException;
import cz.muni.fi.pa165.match.exceptions.MatchNotFoundException;
import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class MatchService {
    private final MatchRepository matchRepository;

    @Autowired
    public MatchService(MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    @Transactional(readOnly = true)
    public List<Match> getUserMatchesByState(Long id, MatchState state) {
        if (state == MatchState.ALL) {
            return matchRepository.findByUser(id);
        } else {
            return matchRepository.findByUserAndState(id, state);
        }
    }

    @Transactional(readOnly = true)
    public List<Match> getMatchesByState(MatchState state) {
        if (state == MatchState.ALL) {
            return matchRepository.findAll();
        } else {
            return matchRepository.findByState(state);
        }
    }

    @Transactional(readOnly = false)
    public Match deleteMatch(Long id) {
        var match = matchRepository.findById(id);
        if (match.isEmpty()) {
            throw new MatchNotFoundException("Match with id " + id + " not found");
        }

        matchRepository.deleteById(id);
        return match.get();
    }

    @Transactional(readOnly = false)
    public Match updateMatch(Match match) {
        return matchRepository.save(match);
    }

    @Transactional(readOnly = true)
    public List<Match> getMatchesToResolve() {
        return matchRepository.findMatchesToResolve();
    }

    @Transactional(readOnly = true)
    public Match createMatch(Match match) {
        if (match.getChallenger() == match.getOpponent()) {
            throw new IllegalMatchStateException("Cannot organize match with same user");
        }

        if (match.getChallenger() == null || match.getOpponent() == null) {
            throw new IllegalMatchStateException("Cannot organize match with missing user");
        }

        var newMatch = Match.builder()
            .challenger(match.getChallenger())
            .opponent(match.getOpponent())
            .state(match.getState())
            .scheduled(match.getScheduled())
            .result(match.getResult())
            .build();

        return matchRepository.save(newMatch);
    }

    @Transactional(readOnly = true)
    public Match getById(Long id) {
        var match = matchRepository.findById(id);
        if (match.isEmpty()) {
            throw new MatchNotFoundException("Match with id " + id + " not found");
        }

        return match.get();
    }

    @Transactional(readOnly = false)
    public Match proposeMatch(Long challengerId, Long opponentId) {
        var match = Match.builder()
            .challenger(challengerId).opponent(opponentId)
            .state(MatchState.PROPOSED).build();

        return matchRepository.save(match);
    }

    @Transactional(readOnly = false)
    public Match reactToMatch(Long id, boolean accepted) {
        var matchOptional = matchRepository.findById(id);
        if (matchOptional.isEmpty()) {
            throw new MatchNotFoundException("Match with id " + id + " not found");
        }
        var match = matchOptional.get();
        if (match.getState() != MatchState.PROPOSED) {
            throw new IllegalMatchStateException("Match with id " + id + " is not in proposed state");
        }
        match.setState(accepted ? MatchState.ACCEPTED : MatchState.REJECTED);
        return matchRepository.save(match);
    }
}
