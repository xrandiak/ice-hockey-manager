package cz.muni.fi.pa165.match.service;

import cz.muni.fi.pa165.match.data.enums.MatchResult;
import cz.muni.fi.pa165.match.data.enums.MatchState;
import cz.muni.fi.pa165.match.data.mappers.MatchMapper;
import cz.muni.fi.pa165.match.data.model.Match;
import cz.muni.fi.pa165.match.data.repository.MatchRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

/**
 * @author Robert Randiak
 */

@ExtendWith(MockitoExtension.class)
class MatchServiceTest {
    @Mock
    private MatchMapper matchMapper;

    @Mock
    private MatchRepository matchRepository;

    @InjectMocks
    private MatchService matchService;
/*
    @Test
    void resolveMatches_mixedResults_updatesMatches() {
        // Arrange
        List<MatchResult> results = List.of(MatchResult.CHALLENGER_WIN, MatchResult.OPPONENT_WIN, MatchResult.DRAW);
        List<Match> toResolve = new ArrayList<>();
        int numMatches = 20;
        for (int i = 0; i < numMatches; i++) {
            Match match = new Match();
            match.setResult(results.get(i % results.size()));
            toResolve.add(match);
        }

        Mockito.when(matchRepository.findByState(MatchState.SCHEDULED)).thenReturn(toResolve);
        Mockito.when(matchRepository.saveAndFlush(any()))
                .thenAnswer(invocation -> invocation.getArgument(0));

        // Act
        matchService.resolveMatches();

        // Assert
        Mockito.verify(matchRepository, Mockito.times(toResolve.size())).saveAndFlush(any());
    }

    @Test
    void scheduleMatches_validMatches_matchesSetAsScheduled() {
        // Arrange
        List<Match> toSchedule = new ArrayList<>();
        int numMatches = 20;
        for (int i = 0; i < numMatches; i++) {
            Match match = Mockito.mock(Match.class);
            match.setState(MatchState.ACCEPTED);
            toSchedule.add(match);
        }

        Mockito.when(matchRepository.findByState(MatchState.ACCEPTED)).thenReturn(toSchedule);
        Mockito.when(matchRepository.saveAndFlush(any()))
                .thenAnswer(invocation -> invocation.getArgument(0));

        // Act
        matchService.scheduleMatches();

        // Assert
        for (Match match : toSchedule) {
            Mockito.verify(matchRepository).saveAndFlush(match);
        }
    }*/

    @Test
    public void reactToMatch_validMatch_matchSetAsAccepted() {
        // Arrange
        Long matchId = 123L;
        boolean accepted = true; 
        MatchState state = MatchState.ACCEPTED;
        Match match = Mockito.mock(Match.class);
        match.setId(matchId);

        Mockito.when(matchRepository.findById(matchId)).thenReturn(Optional.of(match));
        Mockito.when(match.getState()).thenReturn(MatchState.PROPOSED);

        // Act
        Match result = matchService.reactToMatch(matchId, accepted);

        // Assert
        Mockito.verify(matchRepository).findById(matchId);
        //Mockito.verify(match).setRaeacted(any(Date.class));
        Mockito.verify(match).setState(state);
        Mockito.verify(matchRepository).save(match);
    }
}