package cz.muni.fi.pa165.match.repository;

import cz.muni.fi.pa165.match.data.enums.MatchState;
import cz.muni.fi.pa165.match.data.model.Match;
import cz.muni.fi.pa165.match.data.repository.MatchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Andrej Dravecky
 */

 @DataJpaTest
class MatchRepositoryTest {
    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private List<Match> testMatches;
    private boolean initialized = false;

    @BeforeEach
    void matchRepositoryTestSetup() {
        if (initialized) {
            return;
        }
        initialized = true;

        testMatches = List.of(
            Match.builder().challenger(1L).opponent(2L)
                .state(MatchState.PROPOSED).build(),
            Match.builder().challenger(1L).opponent(3L)
                .state(MatchState.PROPOSED).build(),
            Match.builder().challenger(2L).opponent(1L)
                .state(MatchState.PROPOSED).build(),
            Match.builder().challenger(2L).opponent(3L)
                .state(MatchState.PROPOSED).build(),

            Match.builder().challenger(1L).opponent(2L)
                .state(MatchState.ACCEPTED).build(),
            Match.builder().challenger(2L).opponent(1L)
                .state(MatchState.ACCEPTED).build(),

            Match.builder().challenger(1L).opponent(2L)
                .state(MatchState.REJECTED).build(),
            Match.builder().challenger(2L).opponent(3L)
                .state(MatchState.REJECTED).build()
        );
        for (var match : testMatches) {
            testEntityManager.persist(match);
        }
    }

    @Test
    void findByState_matchesWithStateFound_returnsMatches() {
        List<Match> matches = matchRepository.findByState(MatchState.PROPOSED);
        assertEquals(matches,
            testMatches.stream()
                .filter(m -> m.getState().equals(MatchState.PROPOSED))
                .collect(Collectors.toList())
        );
    }

    @Test
    void findByState_matchesWithStateNotFound_returnsEmptyList() {
        List<Match> matches = matchRepository.findByState(MatchState.SCHEDULED);
        assertEquals(matches, new ArrayList<>());
    }

    @Test
    void findByUser_matchesWithUserFound_returnsMatches() {
        List<Match> matches = matchRepository.findByUser(1L);
        assertEquals(matches,
            testMatches.stream()
                .filter(m -> m.getChallenger().equals(1L) || m.getOpponent().equals(1L))
                .collect(Collectors.toList())
        );
    }

    @Test
    void findByUser_matchesWithUserNotFound_returnsEmptyList() {
        List<Match> matches = matchRepository.findByUser(4L);
        assertEquals(matches, new ArrayList<>());
    }

    @Test 
    void findByUserAndState_matchesWithUserAndStateFound_returnsMatches() {
        List<Match> matches = matchRepository.findByUserAndState(1L, MatchState.PROPOSED);
        assertEquals(matches,
            testMatches.stream()
                .filter(m -> (m.getChallenger().equals(1L) || m.getOpponent().equals(1L))
                            && m.getState().equals(MatchState.PROPOSED))
                .collect(Collectors.toList())
        );
    }

    @Test 
    void findByUserAndState_matchesWithUserAndStateNotFound_returnsEmptyList() {
        List<Match> matches = matchRepository.findByUserAndState(4L, MatchState.ACCEPTED);
        assertEquals(matches, new ArrayList<>());
    }

    @Test
    void findByChallengerAndState_matchesWithChallengerAndStateFound_returnsMatches() {
        List<Match> matches = matchRepository.findByChallengerAndState(1L, MatchState.PROPOSED);
        assertEquals(matches,
            testMatches.stream()
                .filter(m -> m.getChallenger().equals(1L) && m.getState().equals(MatchState.PROPOSED))
                .collect(Collectors.toList())
        );
    }

    @Test
    void findByChallengerAndState_matchesWithChallengerAndStateNotFound_returnsEmptyList() {
        List<Match> matches = matchRepository.findByChallengerAndState(3L, MatchState.ACCEPTED);
        assertEquals(matches,  new ArrayList<>());
    }

    @Test
    void findByOpponentAndState_matchesWithOpponentAndStateFound_returnsMatches() {
        List<Match> matches = matchRepository.findByOpponentAndState(2L, MatchState.PROPOSED);
        assertEquals(matches,
            testMatches.stream()
                .filter(m -> m.getOpponent().equals(2L) && m.getState().equals(MatchState.PROPOSED))
                .collect(Collectors.toList())
        );
    }

    @Test
    void findByOpponentAndState_matchesWithOpponentAndStateNotFound_returnsEmptyList() {
        List<Match> matches = matchRepository.findByOpponentAndState(3L, MatchState.ACCEPTED);
        assertEquals(matches, new ArrayList<>());
    }
}
