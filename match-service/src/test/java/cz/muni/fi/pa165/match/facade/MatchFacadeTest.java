package cz.muni.fi.pa165.match.facade;

import cz.muni.fi.pa165.match.data.enums.MatchResult;
import cz.muni.fi.pa165.match.data.enums.MatchState;
import cz.muni.fi.pa165.match.data.mappers.MatchMapper;
import cz.muni.fi.pa165.match.data.model.Match;
import cz.muni.fi.pa165.match.exceptions.MatchNotFoundException;
import cz.muni.fi.pa165.match.service.MatchService;
import cz.muni.fi.pa165.match.generated.model.MatchDto;
import cz.muni.fi.pa165.match.generated.model.MatchResultDto;
import cz.muni.fi.pa165.match.generated.model.MatchStateDto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Robert Randiak
 */

@ExtendWith(MockitoExtension.class)
class MatchFacadeTest {
    @Mock
    private MatchService matchService;

    @Mock
    private MatchMapper matchMapper;

    @InjectMocks
    private MatchFacade matchFacade;

    @Test
    void getMatchById_matchFound_returnsMatch() {
        // Arrange
        Long matchId = 42L;

        Match testMatch = new Match();
        testMatch.setId(matchId);
        testMatch.setChallenger(1L);
        testMatch.setOpponent(2L);
        testMatch.setResult(MatchResult.OPPONENT_WIN);
        Mockito.when(matchService.getById(matchId))
                .thenReturn(testMatch);
/*
        MatchDto testMatchDto = new MatchDto()
                .id(matchId)
                .challenger(1L)
                .opponent(2L)
                .result(MatchResultDto.OPPONENT_WIN);
        //Mockito.when(matchMapper.toDto(testMatch))
        //        .thenReturn(testMatchDto);
*/

        // Act
        Match foundMatch = matchFacade.getMatchById(matchId);

        // Assert
        Assertions.assertNotNull(foundMatch);
        Assertions.assertEquals(foundMatch.getId(), testMatch.getId());
        Assertions.assertEquals(foundMatch.getChallenger(), testMatch.getChallenger());
        Assertions.assertEquals(foundMatch.getOpponent(), testMatch.getOpponent());
        Assertions.assertEquals(foundMatch.getResult(), MatchResult.OPPONENT_WIN);
    }

    @Test
    void getMatchById_notSuchMatch_returnsNull() {
        // Arrange
        Long userId = 1L;

        Mockito.when(matchService.getById(userId)).thenThrow(new MatchNotFoundException("none"));

        // Assert
        assertThrows(MatchNotFoundException.class, () -> matchFacade.getMatchById(userId));
    }

    @Test
    void getUserMatchesByState_matchesFound_returnsMatches() {
        // Arrange
        Long userId = 1L;
        MatchState state = MatchState.PROPOSED;
        //MatchStateDto stateDto = MatchStateDto.PROPOSED;
        List<Match> matches = new ArrayList<>();
        List<MatchDto> matchesDto = new ArrayList<>();
        int numMatches = 20;
        for (int i = 0; i < numMatches; i++) {
            Match match = Mockito.mock(Match.class);
            match.setId((long) i);
            matches.add(match);
            MatchDto matchDto = Mockito.mock(MatchDto.class);
            match.setId((long) i);
            matchesDto.add(matchDto);
        }

        Mockito.when(matchService.getUserMatchesByState(userId, state)).thenReturn(matches);
        //Mockito.when(matchMapper.toDto(matches)).thenReturn(matchesDto);
        //Mockito.when(matchMapper.fromDto(stateDto)).thenReturn(state);

        // Act
        List<Match> result = matchFacade.getUserMatchesByState(userId, state);

        // Assert
        Mockito.verify(matchService).getUserMatchesByState(userId, state);
        Assertions.assertEquals(matches, result);
    }

    @Test
    void getUserMatchesByState_notSuchMatches_returnsNull() {
        // Arrange
        Long userId = 1L;
        MatchState state = MatchState.PROPOSED;
        //MatchStateDto stateDto = MatchStateDto.PROPOSED;
        List<Match> expected = new ArrayList<>();
        //List<MatchDto> expectedDto = new ArrayList<>();

        //Mockito.when(matchService.getUserMatchesByState(userId, state)).thenReturn(expected);
        //Mockito.when(matchMapper.fromDto(stateDto)).thenReturn(state);

        // Act
        List<Match> result = matchFacade.getUserMatchesByState(userId, state);

        // Assert
        Assertions.assertEquals(result, expected);
        Mockito.verify(matchService).getUserMatchesByState(userId, state);
    }
}