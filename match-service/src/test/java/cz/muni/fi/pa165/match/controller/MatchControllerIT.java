package cz.muni.fi.pa165.match.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import cz.muni.fi.pa165.match.data.enums.MatchState;
import cz.muni.fi.pa165.match.data.model.Match;
import cz.muni.fi.pa165.match.data.repository.MatchRepository;
import cz.muni.fi.pa165.match.generated.model.ErrorDto;
import cz.muni.fi.pa165.match.generated.model.MatchDto;
import cz.muni.fi.pa165.match.generated.model.MatchStateDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@SpringBootTest(properties = {
    "config.testing=true",
    "config.match-scheduling-timeout=5000",
    "config.enable-security=false",
})
@AutoConfigureMockMvc
public class MatchControllerIT {
    
    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper mapper = JsonMapper.builder()
        .addModule(new JavaTimeModule())
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .propertyNamingStrategy(new PropertyNamingStrategies.SnakeCaseStrategy())
        .serializationInclusion(JsonInclude.Include.NON_NULL)
        .build();

    @BeforeAll
    static void matchRepositoryTestSetup(@Autowired MatchRepository matchRepository) {
        var testMatchesDetached = List.of(
            Match.builder().challenger(1L).opponent(2L)
                .state(MatchState.PROPOSED).build(),
            Match.builder().challenger(1L).opponent(3L)
                .state(MatchState.PROPOSED).build(),
            Match.builder().challenger(2L).opponent(1L)
                .state(MatchState.PROPOSED).build(),
            Match.builder().challenger(2L).opponent(3L)
                .state(MatchState.PROPOSED).build(),

            Match.builder().challenger(1L).opponent(2L)
                .state(MatchState.REJECTED).build(),
            Match.builder().challenger(2L).opponent(1L)
                .state(MatchState.REJECTED).build()
        );
        matchRepository.saveAll(testMatchesDetached);
    }

    @Test
    //@WithMockUser(authorities = "scope_admin")
    void getMatchById_matchFound_returnMatch() throws Exception {
        // Act
        var resStr = mockMvc.perform(
                get("/match?matchId={matchId}", 1L).accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        var match = mapper.readValue(resStr, MatchDto.class);

        // Assert
        assertThat(match.getId()).isEqualTo(1L);
    }
    @Test
    //@WithMockUser(authorities = "scope_admin")
    void getMatchById_matchNotFound_returnError() throws Exception {
        // Act
        var res= mockMvc.perform(
                get("/match?matchId={matchId}", 666L).accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNotFound())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        var match = mapper.readValue(res, ErrorDto.class);

        // Assert
        assertThat(match.getMessage()).isEqualTo("Match with id 666 not found");
    }

    @Test
    //@WithMockUser(authorities = "scope_user")
    void getMatchesByState_validStateSomeMatches_returnMatches() throws Exception {
        var res = mockMvc.perform(
                get("/matches/{state}", "rejected").accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
        var matches = mapper.readValue(res, MatchDto[].class);

        assertThat(matches.length).isEqualTo(2);
        assertThat(matches[0].getId()).isEqualTo(5L);
        assertThat(matches[1].getId()).isEqualTo(6L);

    }

    @Test
    //@WithMockUser(authorities = "scope_user")
    void getMatchesByState_validStateNoMatches_returnEmptyList() throws Exception {
        var res = mockMvc.perform(
                get("/matches/{state}", "resolved").accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
        var matches = mapper.readValue(res, MatchDto[].class);

        assertThat(matches.length).isEqualTo(0);
    }
    

    @Test
    //@WithMockUser(authorities = "scope_user")
    void getmatchesByState_invalidState_returnError() throws Exception {
        mockMvc.perform(
                get("/matches/{state}", "invalid").accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest());
        // I dunnno how to catch openapi exception 
    }

    @Test
   // @WithMockUser(authorities = "scope_admin")
    void organizeMatch_validMatch_returnMatch() throws Exception {
        var matchDto = new MatchDto()
            .challenger(11L).opponent(10L)
            .state(MatchStateDto.SCHEDULED)
            .scheduled(OffsetDateTime.parse("2025-01-01T00:00:00Z"));

        var res = mockMvc.perform(
                post("/match?opponent=10&challenger=11&state=scheduled&scheduled=2025-01-01T00:00:00Z").accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
        
        var match = mapper.readValue(res, MatchDto.class);

        assertThat(match.getChallenger()).isEqualTo(matchDto.getChallenger());
        assertThat(match.getOpponent()).isEqualTo(matchDto.getOpponent());
        assertThat(match.getScheduled()).isEqualTo(matchDto.getScheduled());
        assertThat(match.getUpdated()).isNotNull();

    }

    @Test
    //@WithMockUser(authorities = "scope_admin")
    void organizeMatch_sameUser_returnError() throws Exception {
        var res = mockMvc.perform(
                post("/match?opponent=10&challenger=10&state=scheduled&scheduled=2025-01-01T00:00:00Z").accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        var error = mapper.readValue(res, ErrorDto.class);

        assertThat(error.getMessage()).isEqualTo("Cannot organize match with same user");
    }

    @Test
   // @WithMockUser(authorities = "scope_admin")
    void organizeMatch_userNull_returnError() throws Exception {
        var res = mockMvc.perform(
                post("/match?challenger=10&state=scheduled&scheduled=2025-01-01T00:00:00Z").accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
        
        var error = mapper.readValue(res, ErrorDto.class);

        assertThat(error.getMessage()).isEqualTo("Cannot organize match with missing user");
    }

    @Test
    //@WithMockUser(authorities = "scope_user")
    void proposeMatch_validUser_returnMatch() throws Exception {
        var res = mockMvc.perform(
                post("/match/propose/1/{id}", 2L).accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        var match = mapper.readValue(res, MatchDto.class);
        assertThat(match.getState()).isEqualTo(MatchStateDto.PROPOSED);
        assertThat(match.getChallenger()).isEqualTo(1L);
        assertThat(match.getOpponent()).isEqualTo(2L);
    }

    // Cannot test yet as auth is not implemented
    //@Test void proposeMatch_invalidUser_returnError() throws Exception {}

    @Test
    //@WithMockUser(authorities = "scope_user")
    void reactToMatchProposition_validMatchState_returnMatch() throws Exception {
        mockMvc.perform(
                post("/match/1/reject").accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
        
        var res = mockMvc.perform(
                get("/match?matchId={matchId}", 1L).accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
        
        var match = mapper.readValue(res, MatchDto.class);

        assertThat(match.getId()).isEqualTo(1L);
        assertThat(match.getState()).isEqualTo(MatchStateDto.REJECTED);
    }

    @Test
    //@WithMockUser(authorities = "scope_user")
    void reactToMatchProposition_invalidMatchState_returnError() throws Exception {
        var res = mockMvc.perform(
                post("/match/{id}/reject", 6L).accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        var error = mapper.readValue(res, ErrorDto.class);

        assertThat(error.getMessage()).isEqualTo("Match with id 6 is not in proposed state");
    }

    @Test
   // @WithMockUser(authorities = "scope_user")
    void acceptMatch_propositionOk_matchIsScheduled() throws Exception {
        mockMvc.perform(
                post("/match/{id}/accept", 2L).accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
        
        Thread.sleep(10000); // wait 10 seconds for scheduler

        var resScheduled = mockMvc.perform(
                get("/match?matchId={matchId}", 2L).accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
        var match = mapper.readValue(resScheduled, MatchDto.class);

        assertThat(match.getState()).isEqualTo(MatchStateDto.SCHEDULED);
        assertThat(match.getScheduled()).isNotNull();
    }
}
