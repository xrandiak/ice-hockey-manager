package cz.muni.fi.pa165.match;

import com.fasterxml.jackson.databind.ObjectMapper;

import cz.muni.fi.pa165.match.data.repository.MatchRepository;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Integration tests. Run by "maven verify".
 */

@AutoConfigureMockMvc
public class Tests {

    private static final Logger log = LoggerFactory.getLogger(Tests.class);

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MatchRepository matchRepository;

    @Test
    void testHello() throws Exception {
        log.debug("We are testing! yay!");
        assertTrue(true);
    }
}
