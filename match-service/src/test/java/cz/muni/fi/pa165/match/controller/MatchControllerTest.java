package cz.muni.fi.pa165.match.controller;

import cz.muni.fi.pa165.match.data.enums.MatchResult;
import cz.muni.fi.pa165.match.data.mappers.MatchMapper;
import cz.muni.fi.pa165.match.data.model.Match;
import cz.muni.fi.pa165.match.facade.MatchFacade;
import cz.muni.fi.pa165.match.service.MatchService;
import cz.muni.fi.pa165.match.generated.model.MatchDto;
import cz.muni.fi.pa165.match.generated.model.MatchResultDto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.test.context.support.WithMockUser;

/**
 * @author Robert Randiak
 */

@ExtendWith(MockitoExtension.class)
class MatchControllerTest {
    @Mock
    private MatchService matchService;

    @Mock
    private MatchMapper matchMapper;

    @Mock
    private MatchFacade matchFacade;

    @InjectMocks
    private MatchController matchController;

    @Test
    //@WithMockUser(authorities = "scope_admin")
    void getMatchById_matchFound_returnsMatch() {
        // Arrange
        Long matchId = 42L;
        Match testMatch = Match.builder()
                .id(matchId)
                .challenger(1L)
                .opponent(2L)
                .result(MatchResult.OPPONENT_WIN)
                .build();
        MatchDto testMatchDto = new MatchDto()
                .id(matchId)
                .challenger(1L)
                .opponent(2L)
                .result(MatchResultDto.OPPONENT_WIN);

        Mockito.when(matchFacade.getMatchById(matchId))
                .thenReturn(testMatch);
        Mockito.when(matchMapper.toDto(testMatch)).thenReturn(testMatchDto);

        // Act
        ResponseEntity<MatchDto> foundEntity = matchController.getMatchById(matchId);

        // Assert
        Assertions.assertNotNull(foundEntity);
        Assertions.assertEquals(foundEntity.getStatusCode(), HttpStatus.OK);
        MatchDto foundMatch = foundEntity.getBody();
        Assertions.assertEquals(foundMatch.getId(), testMatchDto.getId());
        Assertions.assertEquals(foundMatch.getChallenger(), testMatchDto.getChallenger());
        Assertions.assertEquals(foundMatch.getOpponent(), testMatchDto.getOpponent());
        Assertions.assertEquals(foundMatch.getResult(), testMatchDto.getResult());
    }

    @Test
    //@WithMockUser(authorities = "scope_user")
    public void proposeMatch() {
        // Arrange
        Long userId = 2L;
        Match proposedMatch = new Match();
        MatchDto proposedMatchDto = new MatchDto();

        Mockito.when(matchFacade.proposeMatch(1L, userId)).thenReturn(proposedMatch);
        Mockito.when(matchMapper.toDto(proposedMatch)).thenReturn(proposedMatchDto);

        // Act
        ResponseEntity<MatchDto> response = matchController.proposeMatch(1L, userId);

        // Assert
        Mockito.verify(matchFacade).proposeMatch(1L, userId);
        Mockito.verify(matchMapper).toDto(proposedMatch);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals(proposedMatchDto, response.getBody());
    }
}