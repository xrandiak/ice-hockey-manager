#!/bin/bash
if [ "$1" = "--seed" ]; then
    SCRIPT="seedScript.sql"
elif [ "$1" = "--truncate" ]; then
    SCRIPT="truncateScript.sql"
else
    echo "usage: ./db_setup.sh <--seed/--truncate>"
    exit 1
fi

exists() {
    command -v "$1" >/dev/null
    return $?
}

if [ -z "$EXEC" ]; then
    if exists podman; then
        EXEC="podman"
    elif exists docker; then
        EXEC="docker"
    else
        echo "podman/docker not available"
        exit 1
    fi
fi
echo EXEC $EXEC

"$EXEC" exec -i match-statistics-db psql -U postgres -d postgres < "src/main/resources/static/$SCRIPT"
