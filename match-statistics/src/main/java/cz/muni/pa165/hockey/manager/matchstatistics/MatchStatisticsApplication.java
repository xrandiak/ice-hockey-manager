package cz.muni.pa165.hockey.manager.matchstatistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatchStatisticsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatchStatisticsApplication.class, args);
	}

}
