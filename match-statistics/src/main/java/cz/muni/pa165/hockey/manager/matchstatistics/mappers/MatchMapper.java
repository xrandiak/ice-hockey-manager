package cz.muni.pa165.hockey.manager.matchstatistics.mappers;
import cz.muni.pa165.hockey.manager.matchstatistics.data.model.Match;
import cz.muni.pa165.hockey.manager.matchstatistics.data.enums.MatchResult;
import cz.muni.pa165.hockey.manager.matchstatistics.data.enums.MatchState;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchDto;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchResultDto;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchStateDto;


import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MatchMapper {
    MatchState fromDto(MatchStateDto match);
    MatchStateDto toDto(MatchState match);

    MatchResult fromDto(MatchResultDto match);
    MatchResultDto toDto(MatchResult match);

    MatchDto mapToDto(Match match);
    Match mapFromDto(MatchDto matchDto);

    List<MatchDto> mapListToDto(List<Match> matchList);
}
