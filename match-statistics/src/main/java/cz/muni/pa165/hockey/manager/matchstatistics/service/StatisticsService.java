package cz.muni.pa165.hockey.manager.matchstatistics.service;

import cz.muni.pa165.hockey.manager.matchstatistics.data.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StatisticsService {

    private final MatchRepository matchRepository;

    @Autowired
    public StatisticsService(MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    @Transactional(readOnly = true)
    public Long countAll(Long teamId) {
        return matchRepository.countAll(teamId);
    }

    @Transactional(readOnly = true)
    public Long countWins(Long teamId) {
        return matchRepository.countWins(teamId);
    }

    @Transactional(readOnly = true)
    public Long countOvertimeWins(Long teamId) {
        return matchRepository.countWinsOvertime(teamId);
    }

    @Transactional(readOnly = true)
    public Long countOvertimeLosses(Long teamId) {
        return matchRepository.countLossesOvertime(teamId);
    }

    @Transactional(readOnly = true)
    public Long countLosses(Long teamId) {
        return matchRepository.countLosses(teamId);
    }

    @Transactional(readOnly = true)
    public Long countAllAgainstTeam(Long teamId1, Long teamId2) {
        return matchRepository.countAllAgainstTeam(teamId1, teamId2);
    }

    @Transactional(readOnly = true)
    public Long countWinsAgainstTeam(Long teamId1, Long teamId2) {
        return matchRepository.countWinsAgainstTeam(teamId1, teamId2);
    }

    @Transactional(readOnly = true)
    public Long countOvertimeWinsAgainstTeam(Long teamId1, Long teamId2) {
        return matchRepository.countWinsOvertimeAgainstTeam(teamId1, teamId2);
    }

    @Transactional(readOnly = true)
    public Long countOvertimeLossesAgainstTeam(Long teamId1, Long teamId2) {
        return matchRepository.countLossesOvertimeAgainstTeam(teamId1, teamId2);
    }

    @Transactional(readOnly = true)
    public Long countLossesAgainstTeam(Long teamId1, Long teamId2) {
        return matchRepository.countLossesAgainstTeam(teamId1, teamId2);
    }
}
