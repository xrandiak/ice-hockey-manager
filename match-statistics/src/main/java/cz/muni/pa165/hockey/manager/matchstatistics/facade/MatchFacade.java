package cz.muni.pa165.hockey.manager.matchstatistics.facade;

import cz.muni.pa165.hockey.manager.matchstatistics.mappers.MatchMapper;
import cz.muni.pa165.hockey.manager.matchstatistics.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cz.muni.pa165.hockey.manager.matchstatistics.data.model.Match;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchDto;

import java.util.List;

@Service
public class MatchFacade {

    private final MatchService matchService;

    private final MatchMapper matchMapper;

    @Autowired
    public MatchFacade(MatchService matchService, MatchMapper matchMapper) {
        this.matchService = matchService;
        this.matchMapper = matchMapper;
    }

    public MatchDto addMatch(MatchDto matchDto) {
        Match match = matchService.addMatch(matchMapper.mapFromDto(matchDto));
        return matchMapper.mapToDto(match);
    }

    public MatchDto findById(Long id) {
        return matchMapper.mapToDto(matchService.findById(id));
    }

    public List<MatchDto> findByTeams(Long challenger, Long opponent) {
        return matchMapper.mapListToDto(matchService.findByTeams(challenger, opponent));
    }
}
