package cz.muni.pa165.hockey.manager.matchstatistics.data.enums;

public enum MatchResult {
    CHALLENGER_WIN("challenger_win"),
    OPPONENT_WIN("opponent_win"),
    CHALLENGER_WIN_OVERTIME("challenger_win_overtime"),
    OPPONENT_WIN_OVERTIME("challenger_win_overtime");

    MatchResult(String value) {}
}
