package cz.muni.pa165.hockey.manager.matchstatistics.data.repository;

import cz.muni.pa165.hockey.manager.matchstatistics.data.model.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MatchRepository extends JpaRepository<Match, Long> {

    @Query("SELECT m FROM Match m WHERE m.challenger = :challenger AND m.opponent = :opponent")
    List<Match> findByChallengerAndOpponent(Long challenger, Long opponent);

    @Query("SELECT COUNT(m) FROM Match m WHERE m.challenger = :teamId OR m.opponent = :teamId")
    Long countAll(Long teamId);

    @Query("""
            SELECT COUNT(m) FROM Match m
            WHERE (m.challenger = :teamId AND m.result = MatchResult.CHALLENGER_WIN)
            OR (m.opponent = :teamId AND m.result = MatchResult.OPPONENT_WIN)
            """)
    Long countWins(Long teamId);

    @Query("""
            SELECT COUNT(m) FROM Match m
            WHERE (m.challenger = :teamId AND m.result = MatchResult.CHALLENGER_WIN_OVERTIME)
            OR (m.opponent = :teamId AND m.result = MatchResult.OPPONENT_WIN_OVERTIME)
            """)
    Long countWinsOvertime(Long teamId);

    @Query("""
            SELECT COUNT(m) FROM Match m
            WHERE (m.challenger = :teamId AND m.result = MatchResult.OPPONENT_WIN_OVERTIME)
            OR (m.opponent = :teamId AND m.result = MatchResult.CHALLENGER_WIN_OVERTIME)
            """)
    Long countLossesOvertime(Long teamId);

    @Query("""
            SELECT COUNT(m) FROM Match m
            WHERE (m.challenger = :teamId AND m.result = MatchResult.OPPONENT_WIN)
            OR (m.opponent = :teamId AND m.result = MatchResult.CHALLENGER_WIN)
            """)
    Long countLosses(Long teamId);

    @Query("""
            SELECT COUNT(m) FROM Match m
            WHERE (m.challenger = :teamId1 AND m.opponent = :teamId2)
            OR (m.challenger = :teamId2 AND m.opponent = :teamId1)
            """)
    Long countAllAgainstTeam(Long teamId1, Long teamId2);

    @Query("""
            SELECT COUNT(m) FROM Match m
            WHERE (m.challenger = :teamId1 AND m.opponent = :teamId2 AND m.result = MatchResult.CHALLENGER_WIN)
            OR (m.challenger = :teamId2 AND m.opponent = :teamId1 AND m.result = MatchResult.OPPONENT_WIN)
            """)
    Long countWinsAgainstTeam(Long teamId1, Long teamId2);

    @Query("""
            SELECT COUNT(m) FROM Match m
            WHERE (m.challenger = :teamId1 AND m.opponent = :teamId2 AND m.result = MatchResult.CHALLENGER_WIN_OVERTIME)
            OR (m.challenger = :teamId2 AND m.opponent = :teamId1 AND m.result = MatchResult.OPPONENT_WIN_OVERTIME)
            """)
    Long countWinsOvertimeAgainstTeam(Long teamId1, Long teamId2);

    @Query("""
            SELECT COUNT(m) FROM Match m
            WHERE (m.challenger = :teamId1 AND m.opponent = :teamId2 AND m.result = MatchResult.OPPONENT_WIN_OVERTIME)
            OR (m.challenger = :teamId2 AND m.opponent = :teamId1 AND m.result = MatchResult.CHALLENGER_WIN_OVERTIME)
            """)
    Long countLossesOvertimeAgainstTeam(Long teamId1, Long teamId2);

    @Query("""
            SELECT COUNT(m) FROM Match m
            WHERE (m.challenger = :teamId1 AND m.opponent = :teamId2 AND m.result = MatchResult.OPPONENT_WIN)
            OR (m.challenger = :teamId2 AND m.opponent = :teamId1 AND m.result = MatchResult.CHALLENGER_WIN)
            """)
    Long countLossesAgainstTeam(Long teamId1, Long teamId2);
}
