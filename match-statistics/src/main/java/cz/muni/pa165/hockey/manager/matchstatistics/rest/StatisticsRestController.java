package cz.muni.pa165.hockey.manager.matchstatistics.rest;

import cz.muni.pa165.hockey.manager.matchstatistics.StatisticsServiceApi;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.StatisticsDto;
import cz.muni.pa165.hockey.manager.matchstatistics.facade.StatisticsFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticsRestController implements StatisticsServiceApi {

    private final StatisticsFacade statisticsFacade;

    @Autowired
    public StatisticsRestController(StatisticsFacade statisticsFacade) {
        this.statisticsFacade = statisticsFacade;
    }

    @Override
    public ResponseEntity<StatisticsDto> getStatsForTeam(Long teamId) {
        StatisticsDto statistics = statisticsFacade.getStatsForTeam(teamId);
        return ResponseEntity.ok(statistics);
    }

    @Override
    public ResponseEntity<StatisticsDto> getStatsForTeams(Long teamId1, Long teamId2) {
        StatisticsDto statistics = statisticsFacade.getStatsForTeams(teamId1, teamId2);
        return ResponseEntity.ok(statistics);
    }
}
