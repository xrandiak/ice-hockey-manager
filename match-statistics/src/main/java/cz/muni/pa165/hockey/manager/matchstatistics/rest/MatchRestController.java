package cz.muni.pa165.hockey.manager.matchstatistics.rest;

import cz.muni.pa165.hockey.manager.matchstatistics.MatchServiceApi;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchDto;
import cz.muni.pa165.hockey.manager.matchstatistics.facade.MatchFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MatchRestController implements MatchServiceApi {
    /*
     * Technically this isn't a complete REST API, nor is it supposed to be.
     * Since this is just a "stash" for matches from match-service that have
     * been finished and won't change in the future, the only operations that
     * really make sense here are insertion and selection.
     */

    private final MatchFacade matchFacade;

    @Autowired
    public MatchRestController(MatchFacade matchFacade) {
        this.matchFacade = matchFacade;
    }

    @Override
    public ResponseEntity<MatchDto> addMatch(MatchDto matchDto) {
        MatchDto match = matchFacade.addMatch(matchDto);
        return ResponseEntity.ok(match);
    }

    @Override
    public ResponseEntity<Void> addMatches(List<MatchDto> matches) {
        for (MatchDto match : matches) {
            matchFacade.addMatch(match);
        }
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<MatchDto> getMatch(Long teamId) {
        MatchDto match = matchFacade.findById(teamId);
        return ResponseEntity.ok(match);
    }

    @Override
    public ResponseEntity<List<MatchDto>> getMatchesByTeams(Long teamId1, Long teamId2) {
        List<MatchDto> matches = matchFacade.findByTeams(teamId1, teamId2);
        return ResponseEntity.ok(matches);
    }
}
