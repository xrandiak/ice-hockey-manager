package cz.muni.pa165.hockey.manager.matchstatistics.service;

import cz.muni.pa165.hockey.manager.matchstatistics.data.model.Match;
import cz.muni.pa165.hockey.manager.matchstatistics.data.repository.MatchRepository;
import cz.muni.pa165.hockey.manager.matchstatistics.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class MatchService {

    private final MatchRepository matchRepository;

    @Autowired
    public MatchService(MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    @Transactional
    public Match addMatch(Match match) {
        return matchRepository.save(match);
    }

    @Transactional(readOnly = true)
    public Match findById(Long id) {
        Optional<Match> match = matchRepository.findById(id);
        if (match.isEmpty()) {
            throw new ResourceNotFoundException("Match with id `" + id + "` not found");
        }
        return match.get();
    }

    @Transactional(readOnly = true)
    public List<Match> findByTeams(Long challenger, Long opponent) {
        return matchRepository.findByChallengerAndOpponent(challenger, opponent);
    }
}
