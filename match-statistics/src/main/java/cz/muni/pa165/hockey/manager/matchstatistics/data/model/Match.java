package cz.muni.pa165.hockey.manager.matchstatistics.data.model;

import cz.muni.pa165.hockey.manager.matchstatistics.data.enums.MatchResult;
import cz.muni.pa165.hockey.manager.matchstatistics.data.enums.MatchState;
import jakarta.persistence.*;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Objects;

@Entity
@Table(name = "match")

public class Match implements Serializable {

    /* This is mostly copying Match from the match management service, at least
     * on the DTO level. The DTOs should be identical and the IDs preserved,
     * which is why the ID is NOT a generated value here.
     * Some Match fields in match service are nullable but matches submitted
     * in here should not have any null values, so here they are not.
     */
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "challenger")
    private Long challenger;

    @Column(name = "opponent")
    private Long opponent;

    @Column(name = "updated", nullable = false)
    private OffsetDateTime updated;

    @Column(name = "scheduled", nullable = false)
    private OffsetDateTime scheduled;

    @Enumerated(EnumType.STRING)
    @Column(name = "result", nullable = false)
    private MatchResult result;

    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false)
    private MatchState state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChallenger() {
        return challenger;
    }

    public void setChallenger(Long challenger) {
        this.challenger = challenger;
    }

    public Long getOpponent() {
        return opponent;
    }

    public void setOpponent(Long opponent) {
        this.opponent = opponent;
    }

    public OffsetDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(OffsetDateTime updated) {
        this.updated = updated;
    }

    public OffsetDateTime getScheduled() {
        return scheduled;
    }

    public void setScheduled(OffsetDateTime scheduled) {
        this.scheduled = scheduled;
    }

    public MatchResult getResult() {
        return result;
    }

    public void setResult(MatchResult result) {
        this.result = result;
    }

    public MatchState getState() {
        return state;
    }

    public void setState(MatchState state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Match match)) return false;
        return Objects.equals(getChallenger(), match.getChallenger()) && Objects.equals(getOpponent(), match.getOpponent()) && Objects.equals(getUpdated(), match.getUpdated()) && Objects.equals(getScheduled(), match.getScheduled()) && getResult() == match.getResult() && getState() == match.getState();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getChallenger(), getOpponent(), getUpdated(), getScheduled(), getResult(), getState());
    }

    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", challenger=" + challenger +
                ", opponent=" + opponent +
                ", updated=" + updated +
                ", scheduled=" + scheduled +
                ", result=" + result +
                ", state=" + state +
                '}';
    }
}
