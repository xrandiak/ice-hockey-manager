package cz.muni.pa165.hockey.manager.matchstatistics.data.enums;

public enum MatchState {
    PROPOSED("proposed"),
    ACCEPTED("accepted"),
    REJECTED("rejected"),
    SCHEDULED("scheduled"),
    RESOLVED("resolved"),
    //CANCELLED("cancelled"),
    ALL("all");

    MatchState(String value) {}
}
