package cz.muni.pa165.hockey.manager.matchstatistics.facade;

import cz.muni.pa165.hockey.manager.matchstatistics.dto.StatisticsDto;
import cz.muni.pa165.hockey.manager.matchstatistics.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatisticsFacade {

    private final StatisticsService statisticsService;

    @Autowired
    public StatisticsFacade(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    public StatisticsDto getStatsForTeam(Long teamId) {
        StatisticsDto statisticsDto = new StatisticsDto();

        statisticsDto.setMatchesPlayed(statisticsService.countAll(teamId));
        statisticsDto.setMatchesWon(statisticsService.countWins(teamId));
        statisticsDto.setMatchesWonOvertime(statisticsService.countOvertimeWins(teamId));
        statisticsDto.setMatchesLostOvertime(statisticsService.countOvertimeLosses(teamId));
        statisticsDto.setMatchesLost(statisticsService.countLosses(teamId));

        statisticsDto.setMatchesWonTotal(statisticsDto.getMatchesWon() + statisticsDto.getMatchesWonOvertime());
        statisticsDto.setPoints(
                3 * statisticsDto.getMatchesWon() +
                2 * statisticsDto.getMatchesWonOvertime() +
                statisticsDto.getMatchesLostOvertime()
        );

        return statisticsDto;
    }

    public StatisticsDto getStatsForTeams(Long teamId1, Long teamId2) {
        StatisticsDto statisticsDto = new StatisticsDto();

        statisticsDto.setMatchesPlayed(statisticsService.countAllAgainstTeam(teamId1, teamId2));
        statisticsDto.setMatchesWon(statisticsService.countWinsAgainstTeam(teamId1, teamId2));
        statisticsDto.setMatchesWonOvertime(statisticsService.countOvertimeWinsAgainstTeam(teamId1, teamId2));
        statisticsDto.setMatchesLostOvertime(statisticsService.countOvertimeLossesAgainstTeam(teamId1, teamId2));
        statisticsDto.setMatchesLost(statisticsService.countLossesAgainstTeam(teamId1, teamId2));

        statisticsDto.setMatchesWonTotal(statisticsDto.getMatchesWon() + statisticsDto.getMatchesWonOvertime());
        statisticsDto.setPoints(
                3 * statisticsDto.getMatchesWon() +
                2 * statisticsDto.getMatchesWonOvertime() +
                statisticsDto.getMatchesLostOvertime()
        );

        return statisticsDto;
    }
}
