TRUNCATE TABLE match CASCADE;
-- No ID sequences, see Match.java.

-- data from StatisticsRestControllerIT.java
INSERT INTO match (id, challenger, opponent, updated, scheduled, result, state)
VALUES (1, 1, 2, '2024-04-15T18:30Z', '2024-04-15T16:00Z', 'CHALLENGER_WIN', 'RESOLVED');
INSERT INTO match (id, challenger, opponent, updated, scheduled, result, state)
VALUES (2, 1, 2, '2024-04-15T18:30Z', '2024-04-15T16:00Z', 'DRAW', 'RESOLVED');
INSERT INTO match (id, challenger, opponent, updated, scheduled, result, state)
VALUES (3, 1, 2, '2024-04-15T18:30Z', '2024-04-15T16:00Z', 'OPPONENT_WIN', 'RESOLVED');
INSERT INTO match (id, challenger, opponent, updated, scheduled, result, state)
VALUES (4, 2, 1, '2024-04-15T18:30Z', '2024-04-15T16:00Z', 'OPPONENT_WIN', 'RESOLVED');
INSERT INTO match (id, challenger, opponent, updated, scheduled, result, state)
VALUES (5, 1, 3, '2024-04-15T18:30Z', '2024-04-15T16:00Z', 'CHALLENGER_WIN', 'RESOLVED');
