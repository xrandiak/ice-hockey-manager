package cz.muni.pa165.hockey.manager.matchstatistics.util;


import cz.muni.pa165.hockey.manager.matchstatistics.data.enums.MatchResult;
import cz.muni.pa165.hockey.manager.matchstatistics.data.enums.MatchState;
import cz.muni.pa165.hockey.manager.matchstatistics.data.model.Match;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchDto;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchResultDto;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchStateDto;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.HashMap;

@Component
public class MatchTestDataFactory {
    public static HashMap<Long, Match> matchEntities = new HashMap<>();

    public static HashMap<Long, MatchDto> matchDtos = new HashMap<>();

    public static HashMap<Long, Match> getMatchEntityFactory() {
        Match match1 = new Match();
        match1.setId(1L);
        match1.setResult(MatchResult.CHALLENGER_WIN);
        match1.setState(MatchState.RESOLVED);
        match1.setChallenger(1L);
        match1.setOpponent(2L);
        OffsetDateTime match1Updated = OffsetDateTime.parse("2024-04-15T19:30:00+01:00");
        OffsetDateTime match1Scheduled = OffsetDateTime.parse("2024-04-15T17:00:00+01:00");
        match1.setUpdated(match1Updated);
        match1.setScheduled(match1Scheduled);

        Match match2 = new Match();
        match2.setId(2L);
        match2.setResult(MatchResult.OPPONENT_WIN);
        match2.setState(MatchState.RESOLVED);
        match2.setChallenger(1L);
        match2.setOpponent(2L);
        OffsetDateTime match2Updated = OffsetDateTime.parse("2024-04-17T19:30:00+01:00");
        OffsetDateTime match2Scheduled = OffsetDateTime.parse("2024-04-17T17:00:00+01:00");
        match2.setUpdated(match2Updated);
        match2.setScheduled(match2Scheduled);

        matchEntities.put(1L, match1);
        matchEntities.put(2L, match2);

        return matchEntities;

    }

    public static HashMap<Long, MatchDto> getAgentDtosFactory() {
        MatchDto match1 = new MatchDto(1L, 2L);
        match1.setId(1L);
        match1.setResult(MatchResultDto.CHALLENGER_WIN);
        match1.setState(MatchStateDto.RESOLVED);
        OffsetDateTime match1Updated = OffsetDateTime.parse("2024-04-15T18:30Z");
        OffsetDateTime match1Scheduled = OffsetDateTime.parse("2024-04-15T16:00Z");
        match1.setUpdated(match1Updated);
        match1.setScheduled(match1Scheduled);

        MatchDto match2 = new MatchDto(1L, 2L);
        match2.setId(2L);
        match2.setResult(MatchResultDto.OPPONENT_WIN);
        match2.setState(MatchStateDto.RESOLVED);
        OffsetDateTime match2Updated = OffsetDateTime.parse("2024-04-17T18:30Z");
        OffsetDateTime match2Scheduled = OffsetDateTime.parse("2024-04-17T16:00Z");
        match2.setUpdated(match2Updated);
        match2.setScheduled(match2Scheduled);

        matchDtos.put(1L, match1);
        matchDtos.put(2L, match2);

        return matchDtos;
    }


}
