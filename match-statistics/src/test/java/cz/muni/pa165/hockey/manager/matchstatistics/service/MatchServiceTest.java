package cz.muni.pa165.hockey.manager.matchstatistics.service;


import cz.muni.pa165.hockey.manager.matchstatistics.data.enums.MatchResult;
import cz.muni.pa165.hockey.manager.matchstatistics.data.enums.MatchState;
import cz.muni.pa165.hockey.manager.matchstatistics.data.model.Match;
import cz.muni.pa165.hockey.manager.matchstatistics.data.repository.MatchRepository;
import cz.muni.pa165.hockey.manager.matchstatistics.exceptions.ResourceNotFoundException;
import cz.muni.pa165.hockey.manager.matchstatistics.util.MatchTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;
@ExtendWith(MockitoExtension.class)

public class MatchServiceTest {

    @Mock
    private MatchRepository matchRepository;

    @InjectMocks
    private MatchService matchService;


    @Test
    void findById_matchFound_returnsMatch(){
        when(matchRepository.findById(1L)).thenReturn(Optional.of(MatchTestDataFactory.getMatchEntityFactory().get(1L)));

        Match foundEntity = matchService.findById(1L);

        assertThat(foundEntity).isEqualTo(MatchTestDataFactory.matchEntities.get(1L));
    }

    @Test
    void findById_matchNotFound_throws(){
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> matchService.findById(420L));

        String expectedMessage = "Match with id `420` not found";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void findByTeamIds_matchesFound_returnsMatches(){

        when(matchRepository.findByChallengerAndOpponent(1L,2L)).thenReturn(MatchTestDataFactory.getMatchEntityFactory().values().stream().toList());

        List<Match> foundEntities = matchService.findByTeams(1L,2L);

        assertThat(foundEntities).isEqualTo(MatchTestDataFactory.matchEntities.values().stream().toList());

    }


    @Test
    void addMatch_matchAdded_returnsMatch(){
        Match match = new Match();
        match.setId(1L);
        match.setResult(MatchResult.CHALLENGER_WIN);
        match.setState(MatchState.RESOLVED);
        match.setChallenger(1L);
        match.setOpponent(2L);
        OffsetDateTime updated = OffsetDateTime.parse("2024-04-15T19:30:00+01:00");
        OffsetDateTime scheduled = OffsetDateTime.parse("2024-04-15T17:00:00+01:00");
        match.setUpdated(updated);
        match.setScheduled(scheduled);

        when(matchRepository.save(match)).thenReturn(match);

        Match addedMatch = matchService.addMatch(match);

        assertThat(addedMatch).isEqualTo(match);

    }
}
