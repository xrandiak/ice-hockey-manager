package cz.muni.pa165.hockey.manager.matchstatistics.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchDto;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchResultDto;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchStateDto;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.StatisticsDto;
import cz.muni.pa165.hockey.manager.matchstatistics.util.ObjectConverter;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Transactional
@AutoConfigureMockMvc
public class StatisticsRestControllerIT {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    void addMatch(Long id, Long challenger, Long opponent, MatchResultDto result, MatchStateDto state) throws Exception {
        String requestJson;
        MatchDto request;

        request = new MatchDto(challenger, opponent);
        request.setId(id);
        request.setResult(result);
        request.setState(state);
        OffsetDateTime updated = OffsetDateTime.parse("2024-04-15T18:30Z");
        OffsetDateTime scheduled = OffsetDateTime.parse("2024-04-15T16:00Z");
        request.setUpdated(updated);
        request.setScheduled(scheduled);

        requestJson = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/match").contentType(MediaType.APPLICATION_JSON).content(requestJson));
    }

    StatisticsDto getStatisticsForTeam(Long teamId) throws Exception {
        String responseJson = mockMvc.perform(get("/stats/team/{teamId}", teamId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        return ObjectConverter.convertJsonToObject(responseJson, StatisticsDto.class);
    }

    StatisticsDto getStatisticsForTeams(Long teamId1, Long teamId2) throws Exception {
        String responseJson = mockMvc.perform(get("/stats/teams/{teamId1}/{teamId2}", teamId1, teamId2)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        return ObjectConverter.convertJsonToObject(responseJson, StatisticsDto.class);
    }

    @Test
    void testEmptyStatistics() throws Exception {
        StatisticsDto response;

        response = getStatisticsForTeam(1L);
        assertThat(response.getMatchesPlayed()).isEqualTo(0L);
        assertThat(response.getMatchesWon()).isEqualTo(0L);
        assertThat(response.getMatchesWonOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLostOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLost()).isEqualTo(0L);
        assertThat(response.getMatchesWonTotal()).isEqualTo(0L);
        assertThat(response.getPoints()).isEqualTo(0L);

        response = getStatisticsForTeams(1L, 2L);
        assertThat(response.getMatchesPlayed()).isEqualTo(0L);
        assertThat(response.getMatchesWon()).isEqualTo(0L);
        assertThat(response.getMatchesWonOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLostOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLost()).isEqualTo(0L);
        assertThat(response.getMatchesWonTotal()).isEqualTo(0L);
        assertThat(response.getPoints()).isEqualTo(0L);
    }

    @Test
    void testSingleMatchStatistics() throws Exception {
        StatisticsDto response;

        addMatch(1L, 1L, 2L, MatchResultDto.CHALLENGER_WIN, MatchStateDto.RESOLVED);

        response = getStatisticsForTeam(1L);
        assertThat(response.getMatchesPlayed()).isEqualTo(1L);
        assertThat(response.getMatchesWon()).isEqualTo(1L);
        assertThat(response.getMatchesWonOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLostOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLost()).isEqualTo(0L);
        assertThat(response.getMatchesWonTotal()).isEqualTo(1L);
        assertThat(response.getPoints()).isEqualTo(3L);

        response = getStatisticsForTeam(2L);
        assertThat(response.getMatchesPlayed()).isEqualTo(1L);
        assertThat(response.getMatchesWon()).isEqualTo(0L);
        assertThat(response.getMatchesWonOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLostOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLost()).isEqualTo(1L);
        assertThat(response.getMatchesWonTotal()).isEqualTo(0L);
        assertThat(response.getPoints()).isEqualTo(0L);

        response = getStatisticsForTeams(1L, 2L);
        assertThat(response.getMatchesPlayed()).isEqualTo(1L);
        assertThat(response.getMatchesWon()).isEqualTo(1L);
        assertThat(response.getMatchesWonOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLostOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLost()).isEqualTo(0L);
        assertThat(response.getMatchesWonTotal()).isEqualTo(1L);
        assertThat(response.getPoints()).isEqualTo(3L);

        response = getStatisticsForTeams(2L, 1L);
        assertThat(response.getMatchesPlayed()).isEqualTo(1L);
        assertThat(response.getMatchesWon()).isEqualTo(0L);
        assertThat(response.getMatchesWonOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLostOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLost()).isEqualTo(1L);
        assertThat(response.getMatchesWonTotal()).isEqualTo(0L);
        assertThat(response.getPoints()).isEqualTo(0L);
    }

    @Test
    void testMultipleMatchStatistics() throws Exception {
        StatisticsDto response;

        addMatch(1L, 1L, 2L, MatchResultDto.CHALLENGER_WIN, MatchStateDto.RESOLVED);
        addMatch(2L, 1L, 2L, MatchResultDto.CHALLENGER_WIN_OVERTIME, MatchStateDto.RESOLVED);
        addMatch(3L, 1L, 2L, MatchResultDto.OPPONENT_WIN, MatchStateDto.RESOLVED);
        addMatch(4L, 2L, 1L, MatchResultDto.OPPONENT_WIN, MatchStateDto.RESOLVED);
        addMatch(5L, 1L, 3L, MatchResultDto.CHALLENGER_WIN, MatchStateDto.RESOLVED);

        response = getStatisticsForTeam(1L);
        assertThat(response.getMatchesPlayed()).isEqualTo(5L);
        assertThat(response.getMatchesWon()).isEqualTo(3L);
        assertThat(response.getMatchesWonOvertime()).isEqualTo(1L);
        assertThat(response.getMatchesLostOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLost()).isEqualTo(1L);
        assertThat(response.getMatchesWonTotal()).isEqualTo(4L);
        assertThat(response.getPoints()).isEqualTo(11L);

        response = getStatisticsForTeam(2L);
        assertThat(response.getMatchesPlayed()).isEqualTo(4L);
        assertThat(response.getMatchesWon()).isEqualTo(1L);
        assertThat(response.getMatchesWonOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLostOvertime()).isEqualTo(1L);
        assertThat(response.getMatchesLost()).isEqualTo(2L);
        assertThat(response.getMatchesWonTotal()).isEqualTo(1L);
        assertThat(response.getPoints()).isEqualTo(4L);

        response = getStatisticsForTeams(1L, 2L);
        assertThat(response.getMatchesPlayed()).isEqualTo(4L);
        assertThat(response.getMatchesWon()).isEqualTo(2L);
        assertThat(response.getMatchesWonOvertime()).isEqualTo(1L);
        assertThat(response.getMatchesLostOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLost()).isEqualTo(1L);
        assertThat(response.getMatchesWonTotal()).isEqualTo(3L);
        assertThat(response.getPoints()).isEqualTo(8L);

        response = getStatisticsForTeams(2L, 1L);
        assertThat(response.getMatchesPlayed()).isEqualTo(4L);
        assertThat(response.getMatchesWon()).isEqualTo(1L);
        assertThat(response.getMatchesWonOvertime()).isEqualTo(0L);
        assertThat(response.getMatchesLostOvertime()).isEqualTo(1L);
        assertThat(response.getMatchesLost()).isEqualTo(2L);
        assertThat(response.getMatchesWonTotal()).isEqualTo(1L);
        assertThat(response.getPoints()).isEqualTo(4L);
    }
}
