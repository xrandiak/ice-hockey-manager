package cz.muni.pa165.hockey.manager.matchstatistics.rest;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchDto;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchResultDto;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchStateDto;
import cz.muni.pa165.hockey.manager.matchstatistics.exceptions.ResourceNotFoundException;
import cz.muni.pa165.hockey.manager.matchstatistics.facade.MatchFacade;

import cz.muni.pa165.hockey.manager.matchstatistics.util.MatchTestDataFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.util.List;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
public class MatchRestControllerWebMvcTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MatchFacade matchFacade;

    @Test
    void addMatch_matchAdded_returnsMatch() throws Exception {
        MatchDto match = new MatchDto(1L, 2L);
        match.setId(20L);
        match.setResult(MatchResultDto.CHALLENGER_WIN);
        match.setState(MatchStateDto.RESOLVED);
        OffsetDateTime updated = OffsetDateTime.parse("2024-04-15T18:30Z");
        OffsetDateTime scheduled = OffsetDateTime.parse("2024-04-15T16:00Z");
        match.setUpdated(updated);
        match.setScheduled(scheduled);

        when(matchFacade.addMatch(match)).thenReturn(match);

        String matchJson = objectMapper.writeValueAsString(match);

        String response = mockMvc.perform(post("/match")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(matchJson))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        MatchDto resultDto = objectMapper.readValue(response, MatchDto.class);

        assertThat(resultDto.getId()).isEqualTo(match.getId());
        assertThat(resultDto.getOpponent()).isEqualTo(match.getOpponent());
        assertThat(resultDto.getChallenger()).isEqualTo(match.getChallenger());
        assertThat(resultDto.getResult()).isEqualTo(match.getResult());
        assertThat(resultDto.getState()).isEqualTo(match.getState());
        assertThat(resultDto.getUpdated()).isEqualTo(match.getUpdated());
        assertThat(resultDto.getScheduled()).isEqualTo(match.getScheduled());
    }


    @Test
    void findMatch_matchFound_returnsMatch() throws Exception {
        when(matchFacade.findById(1L)).thenReturn(MatchTestDataFactory.getAgentDtosFactory().get(1L));

        String responseJson = mockMvc.perform(get("/match/{id}",1L)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        MatchDto responseMatchDto = objectMapper.readValue(responseJson,MatchDto.class);

        assertThat(responseMatchDto).isEqualTo(MatchTestDataFactory.matchDtos.get(1L));

    }

    @Test
    void findMatch_matchNotFound_returnsNotFound() throws Exception {
        long nonExistentMatchId = 999L;
        String expectedErrorMessage = "Match not found";

        when(matchFacade.findById(nonExistentMatchId)).thenThrow(new ResourceNotFoundException(expectedErrorMessage));

        String responseBody = mockMvc.perform(get("/match/{id}", nonExistentMatchId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        assertThat(responseBody).isEqualTo("");
    }


    @Test
    void findMatchesDtosOfTeams_matchesDtoFound_returnsMatchesDtos() throws Exception {
        when(matchFacade.findByTeams(1L,2L)).thenReturn(MatchTestDataFactory.getAgentDtosFactory().values().stream().toList());


        String responseJson = mockMvc.perform(get("/matches/{teamId1}/{teamId2}",1L,2L)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        List<MatchDto> mappedObjects = objectMapper.readValue(responseJson, new TypeReference<>(){});

        assertThat(mappedObjects).isEqualTo(MatchTestDataFactory.getAgentDtosFactory().values().stream().toList());
    }


}



