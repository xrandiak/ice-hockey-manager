package cz.muni.pa165.hockey.manager.matchstatistics.facade;

import cz.muni.pa165.hockey.manager.matchstatistics.data.enums.MatchResult;
import cz.muni.pa165.hockey.manager.matchstatistics.data.model.Match;
import cz.muni.pa165.hockey.manager.matchstatistics.dto.MatchDto;
import cz.muni.pa165.hockey.manager.matchstatistics.mappers.MatchMapper;
import cz.muni.pa165.hockey.manager.matchstatistics.service.MatchService;
import cz.muni.pa165.hockey.manager.matchstatistics.util.MatchTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;
@ExtendWith(MockitoExtension.class)
public class MatchFacadeTest {


    @Mock
    private MatchService matchService;

    @Mock
    private MatchMapper matchMapper;

    @InjectMocks
    private MatchFacade matchFacade;


    @Test
    void findMatchById_matchFound_returnsMatchDto(){
        when(matchService.findById(1L)).thenReturn(MatchTestDataFactory.getMatchEntityFactory().get(1L));
        when(matchMapper.mapToDto(any())).thenReturn(MatchTestDataFactory.getAgentDtosFactory().get(1L));

        // Act
        MatchDto foundEntity = matchFacade.findById(1L);

        // Assert

        assertThat(foundEntity).isEqualTo(MatchTestDataFactory.matchDtos.get(1L));
    }

    @Test
    void findMatchById_matchNotFound_returnsNull(){
        when(matchService.findById(420L)).thenReturn(MatchTestDataFactory.getMatchEntityFactory().get(420L));
        when(matchMapper.mapToDto(any())).thenReturn(MatchTestDataFactory.getAgentDtosFactory().get(420L));

        // Act
        MatchDto foundEntity = matchFacade.findById(420L);

        // Assert

        assertThat(foundEntity).isEqualTo(null);
    }

    @Test
    void findMatchesDtosOfTeams_matchesDtoFound_returnsMatchesDtos(){
        when(matchService.findByTeams(1L,2L)).thenReturn(MatchTestDataFactory.getMatchEntityFactory().values().stream().toList());
        when(matchMapper.mapListToDto(any())).thenReturn(MatchTestDataFactory.getAgentDtosFactory().values().stream().toList());

        List<MatchDto> foundEntities = matchMapper.mapListToDto(matchService.findByTeams(1L,2L));

        assertThat(foundEntities).isEqualTo(MatchTestDataFactory.matchDtos.values().stream().toList());
    }


}
