# Match statistics service

This service has two main purposes. The first one is to provide a long-term storage for results of complete matches, which are generated in the match service. As the number of matches in the database grows, it would be beneficial to erase the old matches, while storing them here for long-term usage.

The second purpose is to provide summary statistics for matches that have been played in the past. This can either be a complete summary for a single team, or a breakdown of matches between a pair of teams. The points gained from the matches are returned as well.

Many parts of this service have been purposefully modeled after the match service, especially the DTOs have been deliberately kept the same. This will ensure easy integration between these two services in the future.

## Database operations

```bash
./db_setup.sh --seed
./db_setup.sh --truncate
```

The script picks podman or docker, whichever is available (in that order). You can also force one or the other via `EXEC=podman` or `EXEC=docker` in front of the command.

If the script fails for any reason, you can always do e.g.:

```bash
podman exec -i match-statistics-db psql -U postgres -d postgres < "src/main/resources/static/seedScript.sql"
```

## Class diagram
![Statistics Service Dto Class Diagram](../docs/statistics_service_dtos_class_diagram.png "Use Case Diagram")
