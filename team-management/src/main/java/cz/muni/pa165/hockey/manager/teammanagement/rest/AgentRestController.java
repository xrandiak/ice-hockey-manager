package cz.muni.pa165.hockey.manager.teammanagement.rest;

import cz.muni.pa165.hockey.manager.teammanagement.facade.AgentFacade;
import cz.muni.pa165.team.management.AgentsApi;
import cz.muni.pa165.team.management.dto.AgentDto;
import cz.muni.pa165.team.management.dto.AgentPositionDto;
import cz.muni.pa165.team.management.dto.CreateAgentDto;
import cz.muni.pa165.team.management.dto.TeamDto;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AgentRestController implements AgentsApi {


    private final AgentFacade agentFacade;

    @Autowired
    public AgentRestController(AgentFacade agentFacade) {
        this.agentFacade = agentFacade;
    }

    @Override
    public ResponseEntity<AgentDto> createAgent(CreateAgentDto agentDto) {
        AgentDto agent = agentFacade.addAgent(agentDto);
        return ResponseEntity.status(HttpStatusCode.valueOf(201)).body(agent);
    }


    @Override
    public ResponseEntity<Void> deleteAgent(Long agentId) {
        agentFacade.deleteAgent(agentId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<List<AgentDto>> freeAgents() {
        List<AgentDto> agents = agentFacade.getFreeAgents();
        return ResponseEntity.ok(agents);
    }

    @Override
    public ResponseEntity<AgentDto> getAgentById(Long id) {
        return ResponseEntity.ok(agentFacade.findById(id));
    }

    @Override
    public ResponseEntity<List<AgentDto>> getAgentsOfTeam(Long teamId) {
        return ResponseEntity.ok(agentFacade.getAgentsOfTeam(teamId));
    }

    @Override
    public ResponseEntity<List<AgentDto>> getAllAgents() {
        return ResponseEntity.ok(agentFacade.getAllAgents());
    }

    @Override
    public ResponseEntity<AgentDto> updateAgent(AgentDto agentDto) {
        return ResponseEntity.ok(agentFacade.updateAgent(agentDto));
    }


}
