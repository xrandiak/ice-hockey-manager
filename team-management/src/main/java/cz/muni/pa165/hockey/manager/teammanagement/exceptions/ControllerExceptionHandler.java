package cz.muni.pa165.hockey.manager.teammanagement.exceptions;

import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object> handle(ResourceNotFoundException resourceNotFoundException) {
        return new ResponseEntity<>(resourceNotFoundException.getMessage(), HttpStatusCode.valueOf(404));
    }

    @ExceptionHandler(BadInputException.class)
    public ResponseEntity<Object> handle(BadInputException badInputException) {
        return new ResponseEntity<>(badInputException.getMessage(), HttpStatusCode.valueOf(400));
    }

}
