package cz.muni.pa165.hockey.manager.teammanagement.service;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.TeamPower;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.AgentPosition;

import java.util.List;

public class TeamStrengthCalculator {

    public static TeamPower calculateTeamStrength(List<Agent> agents){

        TeamPower teamPower = new TeamPower();
        int defence = 0;
        int offense = 0;


        for (var agent : agents){
            AgentPosition currAgentPos = agent.getPosition();
            if (currAgentPos == AgentPosition.DEFENSE || currAgentPos == AgentPosition.GOALIE){
                defence += agent.getRating();
            }
            else if(currAgentPos == AgentPosition.FORWARD){
                offense += agent.getRating();
            }
        }
        teamPower.setDefense(defence);
        teamPower.setOffense(offense);

        return teamPower;


    }

}


