package cz.muni.pa165.hockey.manager.teammanagement.service;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.TeamRepository;
import cz.muni.pa165.hockey.manager.teammanagement.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TeamService {


    private final TeamRepository teamRepositoryy;

    @Autowired
    public TeamService(TeamRepository teamRepositoryy) {
        this.teamRepositoryy = teamRepositoryy;
    }

    public Optional<Team> findById(long teamId) {
        return teamRepositoryy.findById(teamId);
    }

    @Transactional
    public Team addTeam(Team team) {
        return teamRepositoryy.save(team);
    }

    public List<Team> getAllTeams() {return teamRepositoryy.findAll();}

    @Transactional
    public Integer deleteTeam(long teamId){
         return teamRepositoryy.deleteTeamById(teamId);
    }
    @Transactional
    public Team updateTeam(Team team){return teamRepositoryy.save(team);}
    public List<Team> getUnpickedTeams(){
        return teamRepositoryy.findTeamsByTakenFalse();
    }
    @Transactional
    public Integer takeTeam(long teamId){
        if (teamRepositoryy.existsById(teamId)){
        return teamRepositoryy.takeTeam(teamId);
        }
        throw new ResourceNotFoundException("Team with id : " + teamId + " not found");
    }
    @Transactional
    public Integer untakeTeam(long teamId){
        if (teamRepositoryy.existsById(teamId)){
            return teamRepositoryy.untakeTeam(teamId);
        }
        throw new ResourceNotFoundException("Team with id : " + teamId + " not found");
    }







}
