package cz.muni.pa165.hockey.manager.teammanagement.exceptions;

public class BadInputException extends RuntimeException{
    public BadInputException(String message) {
        super(message);
    }

}
