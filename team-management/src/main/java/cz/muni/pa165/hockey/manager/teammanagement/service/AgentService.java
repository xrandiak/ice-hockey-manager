package cz.muni.pa165.hockey.manager.teammanagement.service;


import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.AgentRepository;
import cz.muni.pa165.hockey.manager.teammanagement.exceptions.ResourceNotFoundException;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AgentService {


    private final AgentRepository repository;

    @Autowired
    public AgentService(AgentRepository agentRepositoryy) {
        this.repository = agentRepositoryy;
    }

    public List<Agent> getAllAgents() {
        return repository.findAll();
    }


    public List<Agent> getFreeAgents() {
        return repository.findAgentsByTeamIsNull();
    }

    @Transactional(readOnly = true)
    public List<Agent> getAgentsOfTeam(long teamId) {
        return repository.findAllByTeamId(teamId);
    }

    @Transactional
    public Agent addAgent(Agent agent) {
        return repository.save(agent);
    }

    @Transactional
    public void deleteAgent(long agentId) {
        repository.findById(agentId).orElseThrow();
        repository.deleteById(agentId);
    }

    public Agent findById(long agentId) {
        Optional<Agent> foundAgent = repository.findById(agentId);
        if (foundAgent.isEmpty()) throw new ResourceNotFoundException("Agent with id : " + agentId + " not found");
        return foundAgent.get();
    }

    @Transactional
    public Agent updateAgent(Agent agent) {
        repository.findById(agent.getId()).orElseThrow();
        return repository.save(agent);
    }

}
