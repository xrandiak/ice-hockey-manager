package cz.muni.pa165.hockey.manager.teammanagement.data.repository;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface AgentRepository extends JpaRepository<Agent,Long> {
    List<Agent> findAgentsByTeamIsNull();

    List<Agent> findAllByTeamId(long teamId);
}
