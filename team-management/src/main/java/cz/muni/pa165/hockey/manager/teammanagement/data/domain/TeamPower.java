package cz.muni.pa165.hockey.manager.teammanagement.data.domain;

import java.util.Objects;

public class TeamPower {
    private int defense;
    private int offense;

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getOffense() {
        return offense;
    }

    public void setOffense(int offense) {
        this.offense = offense;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof  TeamPower)) return false;
        TeamPower teamPower = (TeamPower) o;
        return defense == teamPower.defense && offense == teamPower.offense;
    }

    @Override
    public int hashCode() {
        return Objects.hash(defense, offense);
    }

    @Override
    public String toString() {
        return "TeamPower{" +
                "defense=" + defense +
                ", offense=" + offense +
                '}';
    }
}
