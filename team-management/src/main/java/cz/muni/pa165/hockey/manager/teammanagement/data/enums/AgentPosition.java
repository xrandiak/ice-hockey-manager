package cz.muni.pa165.hockey.manager.teammanagement.data.enums;

public enum AgentPosition {
    FORWARD,
    DEFENSE,
    GOALIE
}
