package cz.muni.pa165.hockey.manager.teammanagement.facade;

import cz.muni.pa165.hockey.manager.teammanagement.mappers.AgentMapper;
import cz.muni.pa165.hockey.manager.teammanagement.mappers.TeamPowerMapper;
import cz.muni.pa165.hockey.manager.teammanagement.service.TeamsAgentsService;
import cz.muni.pa165.team.management.dto.AgentDto;
import cz.muni.pa165.team.management.dto.TeamPowerDto;
import jakarta.annotation.PostConstruct;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TeamsAgentsFacade {

    private final TeamsAgentsService teamsAgentsService;

    private final TeamPowerMapper teamPowerMapper;

    private final AgentMapper agentMapper;


    @Autowired
    public TeamsAgentsFacade(TeamsAgentsService teamsAgentsService, TeamPowerMapper teamPowerMapper, AgentMapper agentMapper) {
        this.teamsAgentsService = teamsAgentsService;
        this.teamPowerMapper = teamPowerMapper;
        this.agentMapper = agentMapper;
    }

    public TeamPowerDto getTeamPower(long teamId) {
        return teamPowerMapper.mapToDto(teamsAgentsService.getTeamPower(teamId));
    }

    public Integer signUpAgent(long teamId, long agentId) {
        return teamsAgentsService.signUpAgent(teamId, agentId);
    }

    public Integer cutContract(long teamId, long agentId) {
        return teamsAgentsService.cutContract(teamId, agentId);
    }
}
