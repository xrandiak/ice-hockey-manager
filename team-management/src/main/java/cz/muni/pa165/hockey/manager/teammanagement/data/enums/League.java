package cz.muni.pa165.hockey.manager.teammanagement.data.enums;

public enum League {
    KHL,
    SHL,
    LIIGA,
    CZECHEXTRALIGA,
    SLOVAKEXTRALIGA,
    DEL,
    SWISSNATIONALLEAGUE
}
