package cz.muni.pa165.hockey.manager.teammanagement.mappers;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import cz.muni.pa165.team.management.dto.CreateTeamDto;
import cz.muni.pa165.team.management.dto.TeamDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CreateTeamMapper {

    CreateTeamDto mapToDto(Team team);

    @Mapping(target = "id",ignore = true)
    @Mapping(target = "agents",ignore = true)
    Team mapFromDto(CreateTeamDto createTeamDto);

}
