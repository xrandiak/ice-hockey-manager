package cz.muni.pa165.hockey.manager.teammanagement.mappers;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import cz.muni.pa165.team.management.dto.TeamDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TeamMapper {

    TeamDto mapToDto(Team team);

    List<TeamDto> maplistToDto(List<Team> teamList);

    @Mapping(target = "agents",ignore = true)
    Team mapFromDto(TeamDto teamDto);

}
