package cz.muni.pa165.hockey.manager.teammanagement.mappers;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.TeamPower;
import cz.muni.pa165.team.management.dto.TeamPowerDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TeamPowerMapper {

    TeamPowerDto mapToDto(TeamPower teamPower);

    TeamPower mapFromDto(TeamPowerDto teamPowerDto);
}
