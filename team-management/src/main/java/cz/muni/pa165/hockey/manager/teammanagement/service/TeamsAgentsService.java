package cz.muni.pa165.hockey.manager.teammanagement.service;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.TeamPower;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.AgentRepository;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.TeamRepository;
import cz.muni.pa165.hockey.manager.teammanagement.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TeamsAgentsService {

    private final TeamRepository teamRepository;

    private final AgentRepository agentRepository;

    @Autowired
    public TeamsAgentsService(TeamRepository teamRepository, AgentRepository agentRepository) {
        this.teamRepository = teamRepository;
        this.agentRepository = agentRepository;
    }

    public TeamPower getTeamPower(long teamId) {
        List<Agent> agentsOfTeam = agentRepository.findAllByTeamId(teamId);
        return TeamStrengthCalculator.calculateTeamStrength(agentsOfTeam);
    }

    @Transactional
    public Integer signUpAgent(long teamId, long agentId){
        Agent agent = checkIfExist(teamId,agentId);
        Team team = teamRepository.findById(teamId).get();
        if (agent.getTeam() == null){
            agent.setTeam(team);
            agentRepository.save(agent);
            return 1;
        }
        return 0;
    }

    @Transactional
    public Integer cutContract(long teamId, long agentId)  {
        Agent agent = checkIfExist(teamId,agentId);
        if (agent.getTeam() != null) {
            agent.setTeam(null);
            agentRepository.save(agent);
            return 1;
        }
        return 0;
    }

    @Transactional
    protected Agent checkIfExist(long teamId, long agentId){
        Optional<Agent> agent = agentRepository.findById(agentId);
        if (agent.isEmpty()) throw new ResourceNotFoundException("agent with id: " + agentId + " was not found.");
        if (teamRepository.findById(teamId).isEmpty())
            throw new ResourceNotFoundException("team with id: " + agentId + " was not found.");
        return agent.get();
    }
}
