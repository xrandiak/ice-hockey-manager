package cz.muni.pa165.hockey.manager.teammanagement.facade;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.mappers.AgentMapper;
import cz.muni.pa165.hockey.manager.teammanagement.mappers.CreateAgentMapper;
import cz.muni.pa165.hockey.manager.teammanagement.service.AgentService;
import cz.muni.pa165.team.management.dto.AgentDto;
import cz.muni.pa165.team.management.dto.CreateAgentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AgentFacade {

    private final AgentService agentService;

    private final AgentMapper agentMapper;

    private final CreateAgentMapper createAgentMapper;


    @Autowired
    public AgentFacade(AgentService agentService, AgentMapper agentMapper, CreateAgentMapper createAgentMapper){
        this.agentService = agentService;
        this.agentMapper = agentMapper;
        this.createAgentMapper = createAgentMapper;
    }

    public List<AgentDto> getFreeAgents(){
        return agentMapper.mapListToDto(agentService.getFreeAgents());
    }

    public List<AgentDto> getAgentsOfTeam(long teamId){return agentMapper.mapListToDto(agentService.getAgentsOfTeam(teamId));}

    public AgentDto addAgent(CreateAgentDto agentDto){
       Agent agent = agentService.addAgent(createAgentMapper.mapFromDto(agentDto));
       return agentMapper.mapToDto(agent);
    }

    public List<AgentDto> getAllAgents(){return agentMapper.mapListToDto(agentService.getAllAgents());}

    public void deleteAgent(long agentId){
        agentService.deleteAgent(agentId);
    }

    public AgentDto updateAgent(AgentDto agentDto){
        Agent agent = agentService.updateAgent(agentMapper.mapFromDto(agentDto));
        return agentMapper.mapToDto(agent);
    }

    public AgentDto findById(long agentId){
        Agent agent = agentService.findById(agentId);
        return agentMapper.mapToDto(agent);
    }


}
