package cz.muni.pa165.hockey.manager.teammanagement.mappers;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.team.management.dto.AgentDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AgentMapper {

     AgentDto mapToDto(Agent agent);

     List<AgentDto> mapListToDto(List<Agent> agentList);

     @Mapping(target = "team",ignore = true)
     Agent mapFromDto(AgentDto agentDto);


}
