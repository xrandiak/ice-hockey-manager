package cz.muni.pa165.hockey.manager.teammanagement;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.AgentRepository;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamManagementApplication  {
    public static void main(String[] args) {
        SpringApplication.run(TeamManagementApplication.class, args);
    }

}
