package cz.muni.pa165.hockey.manager.teammanagement.data.domain;


import cz.muni.pa165.hockey.manager.teammanagement.data.enums.AgentPosition;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Objects;

@Entity
@Table(name = "agent")
public class Agent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "agent_id")
    private long id;


    @Column(name = "rating")
    private int rating;

    @Enumerated(EnumType.STRING)
    @Column(name = "position")
    private AgentPosition position;

    @Column(name = "first_name", length = 50)
    private String firstName;


    @Column(name = "last_name", length = 50)
    private String lastName;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "team_id")
    @OnDelete(action = OnDeleteAction.SET_NULL)
    private Team team;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public AgentPosition getPosition() {
        return position;
    }

    public void setPosition(AgentPosition position) {
        this.position = position;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public String toString() {
        return "Agent{" +
                "id=" + id +
                ", rating=" + rating +
                ", position=" + position +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", team=" + team +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Agent)) return false;
        Agent agent = (Agent) o;
        return id == agent.id && rating == agent.rating && position == agent.position && Objects.equals(firstName, agent.firstName) && Objects.equals(lastName, agent.lastName) && Objects.equals(team, agent.team);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, rating, position, firstName, lastName, team);
    }
}
