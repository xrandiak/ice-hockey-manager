package cz.muni.pa165.hockey.manager.teammanagement.rest;

import cz.muni.pa165.hockey.manager.teammanagement.facade.AgentFacade;
import cz.muni.pa165.hockey.manager.teammanagement.facade.TeamFacade;
import cz.muni.pa165.hockey.manager.teammanagement.facade.TeamsAgentsFacade;
import cz.muni.pa165.hockey.manager.teammanagement.mappers.AgentMapper;
import cz.muni.pa165.hockey.manager.teammanagement.mappers.TeamMapper;
import cz.muni.pa165.team.management.TeamsAgentsApi;
import cz.muni.pa165.team.management.dto.AgentDto;
import cz.muni.pa165.team.management.dto.TeamPowerDto;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeamsAgentsRestController implements TeamsAgentsApi {

    private final TeamsAgentsFacade teamsAgentsFacade;

    public TeamsAgentsRestController(TeamsAgentsFacade teamsAgentsFacade) {
        this.teamsAgentsFacade = teamsAgentsFacade;
    }

    @Override
    public ResponseEntity<Void> cutContract(Long teamId, Long agentId) {

        if (teamsAgentsFacade.cutContract(teamId, agentId) == 1) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @Override
    public ResponseEntity<TeamPowerDto> getTeamPower(Long teamId) {
        TeamPowerDto teamPowerDto = teamsAgentsFacade.getTeamPower(teamId);
        return ResponseEntity.ok(teamPowerDto);
    }
    
    @Override
    public ResponseEntity<List<TeamPowerDto>> getTeamsPower(List<Long> teamIds) {
        return ResponseEntity.ok(teamIds.stream().map(teamId -> teamsAgentsFacade.getTeamPower(teamId)).toList());
    }


    @Override
    public ResponseEntity<Void> signUpAgent(Long teamId, Long agentId) {
        if (teamsAgentsFacade.signUpAgent(teamId, agentId) == 1) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }
}
