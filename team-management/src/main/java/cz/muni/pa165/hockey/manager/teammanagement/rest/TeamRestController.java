package cz.muni.pa165.hockey.manager.teammanagement.rest;

import cz.muni.pa165.hockey.manager.teammanagement.facade.TeamFacade;
import cz.muni.pa165.team.management.TeamApi;
import cz.muni.pa165.team.management.dto.CreateTeamDto;
import cz.muni.pa165.team.management.dto.TeamDto;
import cz.muni.pa165.team.management.dto.TeamPowerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class TeamRestController implements TeamApi{

    private final TeamFacade teamFacade;

    @Autowired
    public TeamRestController(TeamFacade teamFacade) {
        this.teamFacade = teamFacade;
    }


    @Override
    public ResponseEntity<TeamDto> createTeam(CreateTeamDto teamDto) {
        TeamDto team = teamFacade.addTeam(teamDto);
        return ResponseEntity.ok(team);
    }

    @Override
    public ResponseEntity<Void> deleteTeam(Long id) {
        if(teamFacade.deleteTeam(id) == 1){
        return ResponseEntity.ok().build();
        }
        else return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<List<TeamDto>> getAllTeams() {
        List<TeamDto> teams = teamFacade.getAllTeams();
        return ResponseEntity.ok(teams);
    }

    @Override
    public ResponseEntity<TeamDto> getTeamById(Long id) {
        return ResponseEntity.ok(teamFacade.findById(id));
    }

    @Override
    public ResponseEntity<List<TeamDto>> getUntakenTeams() {
        List<TeamDto> teams = teamFacade.getUnpickedTeams();
        return ResponseEntity.ok(teams);
    }


    @Override
    public ResponseEntity<Void> takeTeam(Long teamId) {
        if(teamFacade.takeTeam(teamId) == 1){
        return ResponseEntity.ok().build();
        }
        else return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<Void> untakeTeam(Long teamId) {
        if(teamFacade.untakeTeam(teamId) == 1){
            return ResponseEntity.ok().build();
        }
        else return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<TeamDto> updateTeam(TeamDto teamDto) {
        return ResponseEntity.ok(teamFacade.updateTeam(teamDto));
    }


}
