package cz.muni.pa165.hockey.manager.teammanagement.facade;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.League;
import cz.muni.pa165.hockey.manager.teammanagement.mappers.CreateTeamMapper;
import cz.muni.pa165.hockey.manager.teammanagement.mappers.TeamMapper;
import cz.muni.pa165.hockey.manager.teammanagement.service.TeamService;
import cz.muni.pa165.team.management.dto.CreateTeamDto;
import cz.muni.pa165.team.management.dto.TeamDto;
import cz.muni.pa165.team.management.dto.TeamLeagueDto;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TeamFacade {

    private final TeamService teamService;

    private final TeamMapper teamMapper;

    private final CreateTeamMapper createTeamMapper;

    @Autowired
    public TeamFacade(TeamService teamService, TeamMapper teamMapper, CreateTeamMapper createTeamMapper) {
        this.teamService = teamService;
        this.teamMapper = teamMapper;
        this.createTeamMapper = createTeamMapper;
    }

    public List<TeamDto> getAllTeams(){
        return teamMapper.maplistToDto(teamService.getAllTeams());
    }
    public TeamDto findById(long teamId) {
        Optional<Team> foundTeam = teamService.findById(teamId);
        return foundTeam.map(teamMapper::mapToDto).orElse(null);
    }

    public TeamDto addTeam(CreateTeamDto teamDto) {
        Team team = teamService.addTeam(createTeamMapper.mapFromDto(teamDto));
        return teamMapper.mapToDto(team);
    }

    public Integer deleteTeam(long teamId) {
         return teamService.deleteTeam(teamId);
    }

    public TeamDto updateTeam(TeamDto teamDto) {
        Team team = teamService.updateTeam(teamMapper.mapFromDto(teamDto));
        return teamMapper.mapToDto(team);

    }

    public List<TeamDto> getUnpickedTeams() {
        return teamMapper.maplistToDto(teamService.getUnpickedTeams());
    }

    public Integer takeTeam(long teamId){
        return teamService.takeTeam(teamId);
    }
    public Integer untakeTeam(long teamId){
        return teamService.untakeTeam(teamId);
    }
}
