package cz.muni.pa165.hockey.manager.teammanagement.data.repository;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TeamRepository extends JpaRepository<Team,Long> {
    Integer deleteTeamById(long id);

    List<Team> findTeamsByTakenFalse();
    @Modifying
    @Query("UPDATE Team t SET t.taken = true WHERE t.id = :teamId")
    Integer takeTeam(@Param("teamId") long teamId);
    @Modifying
    @Query("UPDATE Team t SET t.taken = false WHERE t.id = :teamId")
    Integer untakeTeam(@Param("teamId") long teamId);
}
