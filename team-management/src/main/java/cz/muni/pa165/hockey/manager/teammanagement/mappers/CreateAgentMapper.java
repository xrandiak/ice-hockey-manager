package cz.muni.pa165.hockey.manager.teammanagement.mappers;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import cz.muni.pa165.team.management.dto.CreateAgentDto;
import cz.muni.pa165.team.management.dto.CreateTeamDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CreateAgentMapper {

    CreateAgentDto mapToDto(Agent agent);

    @Mapping(target = "id",ignore = true)
    @Mapping(target = "team",ignore = true)
    Agent mapFromDto(CreateAgentDto createAgentDto);
}
