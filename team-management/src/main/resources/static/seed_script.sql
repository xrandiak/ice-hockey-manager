TRUNCATE TABLE team CASCADE;
TRUNCATE TABLE agent CASCADE;
ALTER SEQUENCE agent_agent_id_seq RESTART WITH 1;
ALTER SEQUENCE team_team_id_seq RESTART WITH 1;

-- Insert teams
INSERT INTO team (name, league, taken)
VALUES ('CSKA Moscow', 'KHL', true);
INSERT INTO team (name, league, taken)
VALUES ('Frolunda HC', 'SHL', true);
INSERT INTO team (name, league, taken)
VALUES ('Tappara', 'LIIGA', false);
INSERT INTO team (name, league, taken)
VALUES ('HC Sparta Praha', 'CZECHEXTRALIGA', true);
INSERT INTO team (name, league, taken)
VALUES ('HC Slovan Bratislava', 'SLOVAKEXTRALIGA', true);
INSERT INTO team (name, league, taken)
VALUES ('EHC Biel-Bienne', 'SWISSNATIONALLEAGUE', false);
INSERT INTO team (name, league, taken)
VALUES ('Djurgårdens IF', 'SHL', false);
INSERT INTO team (name, league, taken)
VALUES ('SKA Saint Petersburg', 'KHL', true);
INSERT INTO team (name, league, taken)
VALUES ('TPS', 'LIIGA', false);
INSERT INTO team (name, league, taken)
VALUES ('Kometa Brno', 'CZECHEXTRALIGA', true);

-- Insert agents assigned to teams
-- Team 1
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (100, 'FORWARD', 'Sidney', 'Crosby', 1);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (85, 'FORWARD', 'Alexander', 'Ovechkin', 1);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (45, 'DEFENSE', 'Erik', 'Karlsson', 1);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (78, 'DEFENSE', 'Brent', 'Burns', 1);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (79, 'GOALIE', 'Henrik', 'Lundqvist', 1);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (90, 'FORWARD', 'Nikita', 'Kucherov', 1);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (33, 'FORWARD', 'Steven', 'Stamkos', 1);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (83, 'FORWARD', 'Brad', 'Marchand', 1);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (59, 'DEFENSE', 'Shea', 'Weber', 1);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (85, 'DEFENSE', 'Duncan', 'Keith', 1);

-- Team 2
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (96, 'FORWARD', 'Patrick', 'Kane', 2);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (83, 'FORWARD', 'Connor', 'Hellebuyck', 2);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (87, 'DEFENSE', 'Arnold', 'Schwarzeneger', 2);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (86, 'DEFENSE', 'Alex', 'Pietrangelo', 2);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (78, 'GOALIE', 'Corey', 'Crawford', 2);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (89, 'FORWARD', 'Jonathan', 'Toews', 2);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (67, 'FORWARD', 'Patrick', 'Marleau', 2);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (80, 'FORWARD', 'Zach', 'Parise', 2);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (33, 'DEFENSE', 'Ryan', 'Suter', 2);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (78, 'DEFENSE', 'Drew', 'Doughty', 2);

-- Team 3
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (93, 'FORWARD', 'Nathan', 'MacKinnon', 3);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (69, 'FORWARD', 'Leon', 'Draisaitl', 3);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (75, 'DEFENSE', 'Roman', 'Josie', 3);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (91, 'DEFENSE', 'Morgan', 'Rielly', 3);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (44, 'GOALIE', 'Connor', 'Price', 3);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (88, 'FORWARD', 'Mitch', 'Marner', 3);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (59, 'FORWARD', 'Andrei', 'Svechnikov', 3);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (87, 'FORWARD', 'Filip', 'Forsberg', 3);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (85, 'DEFENSE', 'Zach', 'Werenski', 3);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (71, 'DEFENSE', 'Dougie', 'Hamilton', 3);

-- Team 4
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (98, 'FORWARD', 'Evgeni', 'Malkin', 4);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (90, 'FORWARD', 'Artemi', 'Panarin', 4);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (77, 'DEFENSE', 'John', 'Carlson', 4);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (25, 'DEFENSE', 'Seth', 'Jones', 4);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (48, 'GOALIE', 'Tuukka', 'Rask', 4);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (86, 'FORWARD', 'Michelle', 'Obama', 4);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (84, 'FORWARD', 'Gabriel', 'Landeskog', 4);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (81, 'FORWARD', 'Taylor', 'Hall', 4);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (86, 'DEFENSE', 'Torey', 'Krug', 4);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (99, 'DEFENSE', 'Quinn', 'Hughes', 4);

-- Team 5
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (88, 'FORWARD', 'David', 'Pastrnak', 5);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (85, 'FORWARD', 'Johnny', 'Gaudreau', 5);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (82, 'DEFENSE', 'Magnus', 'Carlsen', 5);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (79, 'DEFENSE', 'Jozef', 'StillBurns', 5);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (55, 'GOALIE', 'Andrei', 'Vasilevskiy', 5);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (55, 'FORWARD', 'Sebastian', 'Aho', 5);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (90, 'FORWARD', 'Zdeno', 'Chára', 5);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (80, 'FORWARD', 'Blake', 'Wheeler', 5);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (84, 'DEFENSE', 'Cale', 'Makar', 5);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (38, 'DEFENSE', 'Jaccob', 'Slavin', 5);

-- Team 6
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (88, 'FORWARD', 'Matthew', 'Tkachuk', 6);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (65, 'FORWARD', 'Brayden', 'Point', 6);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (45, 'DEFENSE', 'Roman', 'Romanov', 6);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (69, 'DEFENSE', 'Adam', 'Fox', 6);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (79, 'GOALIE', 'Tristan', 'Jarry', 6);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (88, 'FORWARD', 'Alex', 'DefinitelyNotOvechkin', 6);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (82, 'FORWARD', 'Sebastian', 'Ahoj', 6);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (80, 'FORWARD', 'David', 'Sopel', 6);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (84, 'DEFENSE', 'Kasper', 'Daugevins', 6);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (43, 'DEFENSE', 'Jaccob', 'Hrad', 6);

-- Team 7
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (60, 'FORWARD', 'Radek ', 'Faksa', 7);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (70, 'FORWARD', 'Elias', 'Pettersson', 7);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (88, 'DEFENSE', 'Branko', 'Radivojevic', 7);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (83, 'DEFENSE', 'Seth', 'Vokoun', 7);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (81, 'GOALIE', 'Jordan', 'Binnington', 7);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (90, 'FORWARD', 'Leon', 'Zweisaitl', 7);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (65, 'DEFENSE', 'Gabriel', 'Beukkaboom', 7);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (60, 'FORWARD', 'Patrik', 'Laine', 7);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (71, 'DEFENSE', 'Zach', 'Galifianakis', 7);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (100, 'FORWARD', 'Miroslav', 'Šatan', 7);

-- Team 8
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (95, 'FORWARD', 'Sergei', 'Krivokrasov', 8);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (86, 'FORWARD', 'Andrei', 'Pavolson', 8);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (88, 'DEFENSE', 'Olaf', 'Kölzig', 8);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (83, 'DEFENSE', 'Seth', 'Wolski', 8);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (82, 'GOALIE', 'Michael', 'Bellington', 8);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (90, 'FORWARD', 'Leon', 'Einsaitl', 8);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (84, 'FORWARD', 'Gabriel', 'Puppa', 8);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (77, 'FORWARD', 'Raitis', 'Ivanas', 8);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (69, 'DEFENSE', 'Zac', 'Efron', 8);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (100, 'DEFENSE', 'Libor', 'Hudáček', 8);

-- Team 9
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (95, 'FORWARD', 'Connor', 'McDavid', 9);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (85, 'FORWARD', 'Imro', 'Kuchar', 9);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (84, 'DEFENSE', 'Victor', 'Hedman', 9);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (83, 'DEFENSE', 'Tomáš', 'Tatár', 9);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (50, 'GOALIE', 'Andrei', 'Giguere', 9);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (90, 'FORWARD', 'Marián', 'Gáborík', 9);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (84, 'FORWARD', 'Gabriel', 'Zherdev', 9);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (73, 'FORWARD', 'Aki', 'Berg', 9);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (44, 'DEFENSE', 'Sergei', 'Samsonov', 9);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (83, 'DEFENSE', 'Jaccob', 'Brashear', 9);

-- Team 10
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (87, 'FORWARD', 'Nathan', 'Nathanovski', 10);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (85, 'FORWARD', 'Steven', 'Stark', 10);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (86, 'DEFENSE', 'Roman', 'Namornik', 10);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (90, 'DEFENSE', 'Bill', 'Quackenbush', 10);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (64, 'GOALIE', 'Ryan', 'Miller', 10);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (74, 'FORWARD', 'Alex', 'Kozovitch', 10);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (100, 'FORWARD', 'Juraj', 'Slafkovský', 10);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (90, 'FORWARD', 'David', 'Kenins', 10);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (88, 'DEFENSE', 'Harold', 'Druken', 10);
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (65, 'DEFENSE', 'Allan ', 'Bester', 10);


-- Insert free agents
INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (79, 'FORWARD', 'Alexander', 'Johnson', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (77, 'DEFENSE', 'Roman', 'Cechmanek', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (88, 'FORWARD', 'James', 'Lee', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (60, 'DEFENSE', 'Liam', 'Smith', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (50, 'FORWARD', 'Matthew', 'Martinez', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (87, 'DEFENSE', 'Daniel', 'Gonzalez', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (77, 'FORWARD', 'Christopher', 'Brown', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (83, 'DEFENSE', 'Jacob', 'Davis', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (80, 'FORWARD', 'Richard', 'Panik', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (48, 'DEFENSE', 'Ethan', 'Wilson', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (78, 'FORWARD', 'Joseph', 'Hernandez', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (82, 'DEFENSE', 'Alexander', 'Thomas', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (84, 'FORWARD', 'Ryan', 'Moore', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (75, 'DEFENSE', 'David', 'White', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (90, 'FORWARD', 'Nicholas', 'Clark', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (75, 'DEFENSE', 'Tyler', 'Anderson', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (91, 'FORWARD', 'Jonathan', 'Allen', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (74, 'DEFENSE', 'Brandon', 'King', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (60, 'FORWARD', 'William', 'Young', NULL);

INSERT INTO agent (rating, position, first_name, last_name, team_id)
VALUES (50, 'DEFENSE', 'Austin', 'Lewis', NULL);
