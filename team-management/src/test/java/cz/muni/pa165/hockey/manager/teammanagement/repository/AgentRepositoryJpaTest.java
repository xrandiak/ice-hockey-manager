package cz.muni.pa165.hockey.manager.teammanagement.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.AgentPosition;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.League;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.AgentRepository;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.TeamRepository;
import cz.muni.pa165.hockey.manager.teammanagement.mappers.AgentMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class AgentRepositoryJpaTest {

    @Autowired
    private AgentRepository agentRepository;

    @Autowired
    private TeamRepository teamRepository;

    @BeforeAll
    public static void initDb(@Autowired AgentRepository agentRepository,
                              @Autowired TeamRepository teamRepository,
                              @Autowired TransactionTemplate transactionTemplate) {

        transactionTemplate.execute(status -> {
            Team team1 = new Team();
            team1.setName("SLOVAN");
            team1.setTaken(false);
            team1.setLeague(League.SLOVAKEXTRALIGA);

            team1 = teamRepository.save(team1);


            Agent agent1 = new Agent();
            agent1.setFirstName("Filip");
            agent1.setLastName("Filipovsky");
            agent1.setPosition(AgentPosition.FORWARD);
            agent1.setRating(150);

            Agent agent2 = new Agent();
            agent2.setFirstName("Peter");
            agent2.setLastName("Petrovsky");
            agent2.setPosition(AgentPosition.GOALIE);
            agent2.setRating(120);

            Agent slovanAgent = new Agent();
            slovanAgent.setFirstName("FirstNameSlovan");
            slovanAgent.setLastName("LastNameSlovan");
            slovanAgent.setPosition(AgentPosition.DEFENSE);
            slovanAgent.setRating(85);
            slovanAgent.setTeam(team1);

            agent1 =  agentRepository.save(agent1);
            agent2 = agentRepository.save(agent2);
            slovanAgent = agentRepository.save(slovanAgent);
            return null;
        });


    }

    @Test
    public void getFreeAgents_agentsFound_returnsAgents() {
        List<Agent> freeAgents = agentRepository.findAgentsByTeamIsNull();
        for (var agent : freeAgents) {
            assertThat(agent.getTeam()).isEqualTo(null);
        }
    }

    @Test
    public void deleteAgent_agentDeleted_returns1() {
        List<Agent> agentsBeforeDeletion = agentRepository.findAll();
        agentRepository.deleteById(1L);
        List<Agent> agentsAfterDeletion = agentRepository.findAll();
        assertThat(agentsBeforeDeletion.size()).isEqualTo(agentsAfterDeletion.size() + 1);
    }


}



