package cz.muni.pa165.hockey.manager.teammanagement.util;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.AgentPosition;
import cz.muni.pa165.team.management.dto.AgentDto;
import cz.muni.pa165.team.management.dto.AgentPositionDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class AgentTestDataFactory {
    public static HashMap<Long, Agent> agentEntities = new HashMap<>();

    public static HashMap<Long, AgentDto> agentDtos = new HashMap<>();


    public static HashMap<Long, Agent> getAgentEntityFactory(){
        Agent agent1 = new Agent();
        agent1.setId(1L);
//        agent1.setTeamIdcko(-1);
        agent1.setRating(99);
        agent1.setPosition(AgentPosition.FORWARD);
        agent1.setFirstName("Jarda");
        agent1.setLastName("Jágr");


        Agent agent2 = new Agent();
        agent2.setId(2L);
//        agent2.setTeamIdcko(1);
        agent2.setRating(85);
        agent2.setPosition(AgentPosition.DEFENSE);
        agent2.setFirstName("Zdeno");
        agent2.setLastName("Chára");

        Agent agent3 = new Agent();
        agent3.setId(3L);
//        agent3.setTeamIdcko(-1);
        agent3.setRating(75);
        agent3.setPosition(AgentPosition.GOALIE);
        agent3.setFirstName("Jaroslav");
        agent3.setLastName("Halák");

        agentEntities.put(1L,agent1);
        agentEntities.put(2L,agent2);
        agentEntities.put(3L,agent3);

        return agentEntities;

    }


    public static HashMap<Long, AgentDto> getAgentDtoFactory(){
        AgentDto agent1 = new AgentDto(1L,99,"Jarda","Jágr", AgentPositionDto.FORWARD);
        AgentDto agent2 = new AgentDto(2L,85,"Zdeno","Chára", AgentPositionDto.DEFENSE);
        AgentDto agent3 = new AgentDto(3L,75,"Jaroslav","Halák", AgentPositionDto.GOALIE);

        agentDtos.put(1L,agent1);
        agentDtos.put(2L,agent2);
        agentDtos.put(3L, agent3);

        return agentDtos;

    }








}
