package cz.muni.pa165.hockey.manager.teammanagement.facade;

import cz.muni.pa165.hockey.manager.teammanagement.mappers.AgentMapper;
import cz.muni.pa165.hockey.manager.teammanagement.service.AgentService;
import cz.muni.pa165.hockey.manager.teammanagement.util.AgentTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.AgentPosition;
import cz.muni.pa165.team.management.dto.AgentDto;
import cz.muni.pa165.team.management.dto.AgentPositionDto;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)

public class AgentFacadeTest {

    @Mock
    private AgentService agentService;

    @Mock
    private AgentMapper agentMapper;

    @InjectMocks
    private AgentFacade agentFacade;


    @Test
    void findById_agentFound_returnsAgent() {
        // Arrange
        when(agentService.findById(1L)).thenReturn(AgentTestDataFactory.getAgentEntityFactory().get(1L));
        when(agentMapper.mapToDto(any())).thenReturn(AgentTestDataFactory.getAgentDtoFactory().get(1L));

        // Act
        AgentDto foundEntity = agentFacade.findById(1L);

        // Assert

        assertThat(foundEntity).isEqualTo(AgentTestDataFactory.getAgentDtoFactory().get(1L));
    }

    @Test
    void findById_agentNotFound_returnsNull() {
        // Arrange
        when(agentService.findById(4L)).thenReturn(AgentTestDataFactory.getAgentEntityFactory().get(4L));
//        when(agentMapper.mapToDto(any())).thenReturn(AgentTestDataFactory.getAgentDtoFactory().get(4L));

        // Act
        AgentDto foundEntity = agentFacade.findById(4L);

        // Assert

        assertThat(foundEntity).isEqualTo(AgentTestDataFactory.getAgentDtoFactory().get(4L));
        assertThat(foundEntity).isEqualTo(null);
    }

    @Test
    void updateAgent_agentFound_returnsUpdatedAgent() {
        // Arrange
        Agent agent4 = new Agent();
        agent4.setId(1);
//        agent4.setTeamIdcko(-1);
        agent4.setRating(90);
        agent4.setPosition(AgentPosition.FORWARD);
        agent4.setFirstName("Sidney");
        agent4.setLastName("Crosby");
        AgentDto agentDto4 = new AgentDto(1L,90,"Sidney","Crosby", AgentPositionDto.FORWARD);

        when(agentService.updateAgent(any())).thenReturn(agent4);
        when(agentMapper.mapToDto(any())).thenReturn(agentDto4);

        // Act
        AgentDto updatedEntity = agentFacade.updateAgent(agentDto4);

        // Assert
        assertThat(updatedEntity).isEqualTo(agentDto4);
    }

    @Test
    void updateAgent_agentNotFound_returnsNewAgent() {
        // Arrange
        Agent agent4 = new Agent();
        agent4.setId(4);
//        agent4.setTeamIdcko(-1);
        agent4.setRating(90);
        agent4.setPosition(AgentPosition.FORWARD);
        agent4.setFirstName("Sidney");
        agent4.setLastName("Crosby");
        AgentDto agentDto4 = new AgentDto(4L,90,"Sidney","Crosby", AgentPositionDto.FORWARD);

        when(agentService.updateAgent(any())).thenReturn(agent4);
        when(agentMapper.mapToDto(any())).thenReturn(agentDto4);

        // Act
        AgentDto updatedEntity = agentFacade.updateAgent(agentDto4);

        // Assert
        assertThat(updatedEntity).isEqualTo(agentDto4);
    }

    @Test
    void addAgent_ok_returnsNewAgent() {
        // Arrange
        Agent agent4 = new Agent();
        agent4.setId(4);
//        agent4.setTeamIdcko(-1);
        agent4.setRating(90);
        agent4.setPosition(AgentPosition.FORWARD);
        agent4.setFirstName("Sidney");
        agent4.setLastName("Crosby");
        AgentDto agentDto4 = new AgentDto(4L,90,"Sidney","Crosby", AgentPositionDto.FORWARD);

        when(agentService.updateAgent(any())).thenReturn(agent4);
        when(agentMapper.mapToDto(any())).thenReturn(agentDto4);

        // Act
        AgentDto updatedEntity = agentFacade.updateAgent(agentDto4);

        // Assert
        assertThat(updatedEntity).isEqualTo(agentDto4);
    }

//    @Test
//    void deleteAgent_agentFound_deletesAgent() {
//        // Arrange
//        when(agentService.deleteAgent(1L)).thenReturn(1);
//
//        // Act
//        agentFacade.deleteAgent(1L);
//
//        // Assert
//        verify(agentService).deleteAgent(1L);
//    }
//
//    @Test
//    void deleteAgent_agentNotFound_doesNothing() {
//        // Arrange
//        when(agentService.deleteAgent(4L)).thenReturn(null);
//
//        // Act
//        agentFacade.deleteAgent(4L);
//
//        // Assert
//        verify(agentService).deleteAgent(4L);
//    }
}
