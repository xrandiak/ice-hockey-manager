package cz.muni.pa165.hockey.manager.teammanagement.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.facade.AgentFacade;
import cz.muni.pa165.hockey.manager.teammanagement.util.AgentTestDataFactory;
import cz.muni.pa165.hockey.manager.teammanagement.util.ObjectConverter;
import cz.muni.pa165.team.management.dto.AgentDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebMvcTest(controllers = {AgentRestController.class})
@SpringBootTest
@AutoConfigureMockMvc
public class AgentRestControllerWebMvcTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AgentFacade agentFacade;

    @Test
    void getFreeAgents_agentsFound_returnsAgents() throws Exception {
        when(agentFacade.getFreeAgents()).thenReturn(AgentTestDataFactory.getAgentDtoFactory().values().stream().toList());

        String responseJson = mockMvc.perform(get("/agents/free-agents")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);


        ObjectMapper objectMapper = new ObjectMapper();
        List<AgentDto> mappedObjects = objectMapper.readValue(responseJson, new TypeReference<>(){});
        System.out.println(mappedObjects);
        System.out.println(AgentTestDataFactory.getAgentDtoFactory().values().stream().toList());

        assertThat(mappedObjects).isEqualTo(AgentTestDataFactory.getAgentDtoFactory().values().stream().toList());
    }



}
