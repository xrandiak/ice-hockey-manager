package cz.muni.pa165.hockey.manager.teammanagement.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.AgentPosition;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.League;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.AgentRepository;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.TeamRepository;
import cz.muni.pa165.hockey.manager.teammanagement.mappers.AgentMapper;
import cz.muni.pa165.team.management.dto.AgentDto;
import cz.muni.pa165.team.management.dto.AgentPositionDto;
import cz.muni.pa165.team.management.dto.CreateAgentDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.support.TransactionTemplate;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
public class AgentControllerIT {

    @Autowired
    private MockMvc mockMvc;


    @Autowired
    private AgentRepository agentRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private AgentMapper agentMapper;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeAll
    public static void initDb(@Autowired AgentRepository agentRepository,
                              @Autowired TeamRepository teamRepository,
                              @Autowired TransactionTemplate transactionTemplate) {

        transactionTemplate.execute(status -> {
            Agent agent1 = new Agent();
            agent1.setFirstName("Filip");
            agent1.setLastName("Filipovsky");
            agent1.setPosition(AgentPosition.FORWARD);
            agent1.setRating(150);

            Agent agent2 = new Agent();
            agent2.setFirstName("Peter");
            agent2.setLastName("Petrovsky");
            agent2.setPosition(AgentPosition.GOALIE);
            agent2.setRating(120);


            Agent forDeletion = new Agent();
            forDeletion.setFirstName("Deleter");
            forDeletion.setLastName("Deleted");
            forDeletion.setPosition(AgentPosition.GOALIE);
            forDeletion.setRating(15);

            agentRepository.save(agent1);
            agentRepository.save(agent2);
            agentRepository.save(forDeletion);


            Team team1 = new Team();
            team1.setName("SLOVAN");
            team1.setTaken(false);
            team1.setLeague(League.SLOVAKEXTRALIGA);

            team1 = teamRepository.save(team1);


            Agent slovanAgent = new Agent();
            slovanAgent.setFirstName("FirstNameSlovan");
            slovanAgent.setLastName("LastNameSlovan");
            slovanAgent.setPosition(AgentPosition.DEFENSE);
            slovanAgent.setRating(85);
            slovanAgent.setTeam(team1);

            Agent slovanAgent2 = new Agent();
            slovanAgent2.setFirstName("FirstNameSlovan2");
            slovanAgent2.setLastName("LastNameSlovan");
            slovanAgent2.setPosition(AgentPosition.FORWARD);
            slovanAgent2.setRating(99);
            slovanAgent2.setTeam(team1);

            agentRepository.save(slovanAgent);
            agentRepository.save(slovanAgent2);
            return null;
        });


    }


    @Test
    void getAllAgents_agentsFound_returnsAgents() throws Exception {

        List<Agent> agents = agentRepository.findAll();

        String response = mockMvc.perform(get("/agents/all-agents")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().
                getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        List<AgentDto> mappedObjects = objectMapper.readValue(response, new TypeReference<>() {
        });

        assertThat(mappedObjects).isEqualTo(agentMapper.mapListToDto(agents));
        assertThat(mappedObjects.size()).isEqualTo(agents.size());
    }

    @Test
    void getFreeAgents_freeAgentsFound_returnAgents() throws Exception {
        List<Agent> agents = agentRepository.findAgentsByTeamIsNull();

        String response = mockMvc.perform(get("/agents/free-agents")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().
                getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        List<AgentDto> mappedObjects = objectMapper.readValue(response, new TypeReference<>() {
        });

        assertThat(mappedObjects).isEqualTo(agentMapper.mapListToDto(agents));
        assertThat(mappedObjects.size()).isEqualTo(agents.size());
    }




    @Test
    void getAgentsOfTeam_agentsFound_returnsAgents() throws Exception {
        long slovanTeamId = 1L;
        List<Agent> agents = agentRepository.findAllByTeamId(slovanTeamId);

        String response = mockMvc.perform(get("/agents/agents-of-team/{teamId}", slovanTeamId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().
                getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        List<AgentDto> mappedObjects = objectMapper.readValue(response, new TypeReference<>() {
        });

        assertThat(mappedObjects).isEqualTo(agentMapper.mapListToDto(agents));
        assertThat(mappedObjects.size()).isEqualTo(2);
    }

    @Test
    void getAgentById_agentFound_returnsAgent() throws Exception {
        long id = 1;
        String response = mockMvc.perform(get("/agents/get-agent/{id}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().
                getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        AgentDto agentDto = objectMapper.readValue(response, AgentDto.class);
        assertThat(agentDto.getFirstName()).isEqualTo("Filip");
        assertThat(agentDto.getLastName()).isEqualTo("Filipovsky");
        assertThat(agentDto.getRating()).isEqualTo(150);
        assertThat(agentDto.getPosition()).isEqualTo(AgentPositionDto.FORWARD);
    }

    @Test
    void getAgentById_agentNotFound_ReturnsStatus404() throws Exception {
        long id = 9999;
        MockHttpServletResponse response = mockMvc.perform(get("/agents/get-agent/{id}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound()).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(404);
    }

    @Test
    void addAgent_agentAdded_returnsAddedAgent() throws Exception {
        CreateAgentDto newAgent = new CreateAgentDto(50, "Added", "Agent", AgentPositionDto.FORWARD);
        String newAgentJson = objectMapper.writeValueAsString(newAgent);
        String response = mockMvc.perform(post("/agents/add-agent")
                        .content(newAgentJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(201))
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);


        AgentDto addedAgentDto = objectMapper.readValue(response, AgentDto.class);
        assertThat(addedAgentDto.getRating()).isEqualTo(50);
        assertThat(addedAgentDto.getFirstName()).isEqualTo("Added");
        assertThat(addedAgentDto.getLastName()).isEqualTo("Agent");
        assertThat(addedAgentDto.getPosition()).isEqualTo(AgentPositionDto.FORWARD);
    }


    @Test
    void updateAgent_agentUpdated_returnsUpdatedAgent() throws Exception {
        AgentDto agentToUpdate = new AgentDto(2L,500,"UpgradedPeter","Petrovsky",AgentPositionDto.GOALIE);

        String agentToUpdateJson = objectMapper.writeValueAsString(agentToUpdate);
        String response = mockMvc.perform(patch("/agents/update-agent")
                        .content(agentToUpdateJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200)).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);


        AgentDto addedAgentDto = objectMapper.readValue(response, AgentDto.class);
        assertThat(addedAgentDto.getRating()).isEqualTo(500);
        assertThat(addedAgentDto.getFirstName()).isEqualTo("UpgradedPeter");
        assertThat(addedAgentDto.getLastName()).isEqualTo("Petrovsky");
        assertThat(addedAgentDto.getPosition()).isEqualTo(AgentPositionDto.GOALIE);

        Optional<Agent> newPeter = agentRepository.findById(2L);
        if (newPeter.isPresent()){
            assertThat(newPeter.get().getFirstName()).isEqualTo(agentToUpdate.getFirstName());
            assertThat(newPeter.get().getLastName()).isEqualTo(agentToUpdate.getLastName());
            assertThat(newPeter.get().getRating()).isEqualTo(agentToUpdate.getRating());
        }

    }


    @Test
    void deleteAgent_agentDeleted_returnsStatus200() throws Exception {
        long agentToDeleteId = 3;


        String beforeDelete = mockMvc.perform(get("/agents/all-agents")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().
                getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        List<AgentDto> agentsBeforeDelete = objectMapper.readValue(beforeDelete, new TypeReference<>() {
        });
        assertThat(agentsBeforeDelete.size()).isEqualTo(5);

        MockHttpServletResponse response = mockMvc.perform(delete("/agents/delete-agent/{agentId}", agentToDeleteId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent()).andReturn().getResponse();


        assertThat(response.getStatus()).isEqualTo(204);


        String afterDelete = mockMvc.perform(get("/agents/all-agents")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().
                getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        List<AgentDto> agentsAfterDelete = objectMapper.readValue(afterDelete, new TypeReference<>() {
        });

        assertThat(agentsAfterDelete.size()).isEqualTo(4);
    }


}








