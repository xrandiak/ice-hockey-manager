package cz.muni.pa165.hockey.manager.teammanagement.service;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.TeamPower;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.AgentPosition;
import org.junit.jupiter.api.Test;

import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

public class TeamStregthCalculatorTest {


    @Test
    public void getTeamStrength_returnsStrength_correctCalculation(){
        Agent agent1 = new Agent();
        agent1.setFirstName("Filip");
        agent1.setLastName("Filipovsky");
        agent1.setPosition(AgentPosition.FORWARD);
        agent1.setRating(150);

        Agent agent2 = new Agent();
        agent2.setFirstName("Peter");
        agent2.setLastName("Petrovsky");
        agent2.setPosition(AgentPosition.GOALIE);
        agent2.setRating(120);

        Agent slovanAgent = new Agent();
        slovanAgent.setFirstName("FirstNameSlovan");
        slovanAgent.setLastName("LastNameSlovan");
        slovanAgent.setPosition(AgentPosition.DEFENSE);
        slovanAgent.setRating(85);

        TeamPower teamPower = TeamStrengthCalculator.calculateTeamStrength(List.of(agent1,agent2,slovanAgent));
        assertThat(teamPower.getDefense()).isEqualTo(205);
        assertThat(teamPower.getOffense()).isEqualTo(150);
    }
}
