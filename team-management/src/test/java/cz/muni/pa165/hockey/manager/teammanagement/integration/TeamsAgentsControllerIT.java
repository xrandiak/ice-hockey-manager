package cz.muni.pa165.hockey.manager.teammanagement.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
//TODO more integration testing in the next milestone

@SpringBootTest
@AutoConfigureMockMvc
public class TeamsAgentsControllerIT {
    @Autowired
    private MockMvc mockMvc;
}
