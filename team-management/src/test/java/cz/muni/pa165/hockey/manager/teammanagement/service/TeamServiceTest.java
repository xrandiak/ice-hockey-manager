package cz.muni.pa165.hockey.manager.teammanagement.service;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.AgentRepository;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.TeamRepository;
import cz.muni.pa165.hockey.manager.teammanagement.util.AgentTestDataFactory;
import cz.muni.pa165.hockey.manager.teammanagement.util.TeamTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)

public class TeamServiceTest {

    @Mock
    private TeamRepository teamRepository;

    @InjectMocks
    private TeamService teamService;

    @Test
    void findById_teamFound_returnsTeam(){
        when(teamRepository.findById(1L)).thenReturn(Optional.ofNullable(TeamTestDataFactory.getTeamEntityFactory().get(1L)));

        Optional<Team> foundEntity = teamService.findById(1L);

        assertThat(foundEntity.get()).isEqualTo(TeamTestDataFactory.getTeamEntityFactory().get(1L));
    }

}
