package java.cz.muni.pa165.hockey.manager.teammanagement.util;

import java.util.HashMap;

public class HashMapUtils {
    public static <K, V> HashMap<K, V> removeKey(HashMap<K, V> map, K key) {
        map.remove(key);
        return map;
    }
}
