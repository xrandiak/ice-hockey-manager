package cz.muni.pa165.hockey.manager.teammanagement.util;


import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.League;
import cz.muni.pa165.team.management.dto.TeamDto;
import cz.muni.pa165.team.management.dto.TeamLeagueDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class TeamTestDataFactory {


    public static final HashMap<Long, Team> teamEntities = new HashMap<>();
    public static final HashMap<Long, TeamDto> teamDtos = new HashMap<>();


    public static HashMap<Long, Team> getTeamEntityFactory() {
        Team team1 = new Team();
        team1.setName("Slovan");
        team1.setTaken(false);
        team1.setLeague(League.SLOVAKEXTRALIGA);
        team1.setId(1L);

        Team team2 = new Team();
        team2.setName("Michalovce");
        team2.setTaken(true);
        team2.setLeague(League.SLOVAKEXTRALIGA);
        team2.setId(2L);

        Team team3 = new Team();
        team3.setName("Tappara Tampere");
        team3.setTaken(false);
        team3.setLeague(League.LIIGA);
        team3.setId(3L);

        teamEntities.put(1L,team1);
        teamEntities.put(2L,team2);
        teamEntities.put(3L,team3);


        return teamEntities;

    }


    public static HashMap<Long, TeamDto> getTeamDtoFactory() {
        TeamDto teamDto1 = new TeamDto("Slovan", 1L);
        teamDto1.setLeague(TeamLeagueDto.SLOVAKEXTRALIGA);
        teamDto1.setTaken(false);

        TeamDto teamDto2 = new TeamDto("Michalovce", 2L);
        teamDto2.setLeague(TeamLeagueDto.SLOVAKEXTRALIGA);
        teamDto2.setTaken(true);

        TeamDto teamDto3 = new TeamDto("Tappara Tampere", 3L);
        teamDto3.setLeague(TeamLeagueDto.LIIGA);
        teamDto3.setTaken(false);

        teamDtos.put(1L,teamDto1);
        teamDtos.put(2L,teamDto2);
        teamDtos.put(3L,teamDto3);

        return teamDtos;
    }
}
