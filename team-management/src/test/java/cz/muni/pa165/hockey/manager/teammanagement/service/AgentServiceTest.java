package cz.muni.pa165.hockey.manager.teammanagement.service;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.AgentPosition;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.AgentRepository;
import cz.muni.pa165.hockey.manager.teammanagement.util.AgentTestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AgentServiceTest {

    @Mock
    private AgentRepository agentRepository;

    @InjectMocks
    private AgentService agentService;
    @Test
    void findById_AgentFound_returnsAgent(){
        when(agentRepository.findById(1L)).thenReturn(Optional.ofNullable(AgentTestDataFactory.getAgentEntityFactory().get(1L)));

        Agent foundEntity = agentService.findById(1L);

        assertThat(foundEntity).isEqualTo(AgentTestDataFactory.agentEntities.get(1L));
    }

    @Test
    void addAgent_agentAdded_returnsNewAgent(){
        Agent agent = new Agent();
        agent.setId(4L);
//        agent.setTeamIdcko(-1);
        agent.setRating(120);
        agent.setPosition(AgentPosition.GOALIE);
        agent.setFirstName("Branko");
        agent.setLastName("Brankarsky");

        when(agentRepository.save(agent)).thenReturn(agent);

        Agent addedAgent = agentService.addAgent(agent);

        assertThat(addedAgent).isEqualTo(agent);
    }
}
