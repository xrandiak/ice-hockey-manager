package cz.muni.pa165.hockey.manager.teammanagement.repository;

import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Agent;
import cz.muni.pa165.hockey.manager.teammanagement.data.domain.Team;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.AgentPosition;
import cz.muni.pa165.hockey.manager.teammanagement.data.enums.League;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.AgentRepository;
import cz.muni.pa165.hockey.manager.teammanagement.data.repository.TeamRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class TeamRepositoryJpaTest {
    @Autowired
    private TeamRepository teamRepository;

    @BeforeAll
    public static void initDb(
            @Autowired TeamRepository teamRepository,
            @Autowired TransactionTemplate transactionTemplate) {
        transactionTemplate.execute(status -> {


            Team team1 = new Team();
            team1.setName("SLOVAN");
            team1.setTaken(false);
            team1.setLeague(League.SLOVAKEXTRALIGA);

            team1 = teamRepository.save(team1);

            Team team2 = new Team();
            team2.setName("Humenne");
            team2.setTaken(true);
            team2.setLeague(League.SLOVAKEXTRALIGA);

            team2 = teamRepository.save(team2);

            Team teamForDeletion = new Team();
            teamForDeletion.setName("FORDELETION");
            teamForDeletion.setTaken(false);
            teamForDeletion.setLeague(League.SLOVAKEXTRALIGA);

            teamForDeletion = teamRepository.save(teamForDeletion);

            return null;
        });


    }

    @Test
    public void getUntakenTeams_findsUntakenTeams_returnsUntakenTeams() {
        List<Team> untakenTeams = teamRepository.findTeamsByTakenFalse();
        for (var team : untakenTeams){
            assertThat(team.isTaken()).isEqualTo(false);
        }
    }

    @Test
    public void takeTeam_takesTeam_returnsIsTaken() {
        assertThat(teamRepository.takeTeam(1)).isEqualTo(1);
        Optional<Team> team = teamRepository.findById(1L);
        team.ifPresent(value -> assertThat(value.isTaken()).isEqualTo(true));
    }
    @Test
    public void untakeTeam_untakesTeam_returnsIsUnTaken() {
        assertThat(teamRepository.untakeTeam(1)).isEqualTo(1);
        Optional<Team> team = teamRepository.findById(1L);
        team.ifPresent(value -> assertThat(value.isTaken()).isEqualTo(false));

    }

    @Test
    public void deleteTeam_deletesTeam_returnsEmpty() {
        assertThat(teamRepository.deleteTeamById(3)).isEqualTo(1);
        Optional<Team> team = teamRepository.findById(3L);
        assertThat(team.isEmpty()).isEqualTo(true);
    }


}
