##### Team Management Service
The Team Management Service is responsible for overseeing the administration of teams and
players within the system. With this service, administrators or users with the permissions 
to manage their teams can effectively create, retrieve, update, and delete teams and players as needed.

##### Team Management Service DTO Class Diagram
![Team Management Service Dto Class Diagram](../docs/team-management_service_dto_class_diagram.png "Use Case Diagram")

#### How to run

The requirement for configuration for the microservice was to use Podman/Docker and
Podman Compose/Docker Compose to package and run your services. To run the app alongside with the postgres database,
a docker-compose file was created. In the docker-compose.yaml file, the images for both the application and the
database get built automatically. To run it, simply run mvn clean install so the jar file gets created in target,
and then run 'podman compose up' (or docker if you are not using podman).

When the container is up and running, you can try out the app accessing swaggerUI (http://localhost:8081/swagger-ui/index.html)

#### Testing
Since the module was lacking in the tests sphere in the last milestone, i decided to write more tests. So far, 8 Integration Tests
were written, and in the next milestone/s all will be written.

(The integration tests should run on their own when calling mvn clean install)

#### Additions
More endpoints for some missing CRUD operations and DTO's were also added, like CreateAgentDto and CreateTeamDto, which do not accept Id as a parameter and
lets the persistence layer generate it on its own.

If there are any issues, please contact me on discord :)


#### SEEDING AND CLEARING THE DATABASE OF TEAM-MANAGEMENT SERVICE

To fill the database with some data, you can run this command in the terminal from the root directory :

```cat ./team-management/src/main/resources/static/seed_script.sql | podman exec -i team-management-db psql -U postgres -d postgres```

And similarly, to truncate all the tables of the service database, run this command :

```cat ./team-management/src/main/resources/static/truncate_script.sql | podman exec -i team-management-db psql -U postgres -d postgres```

(use docker exec if you do not have podman on your machine)
