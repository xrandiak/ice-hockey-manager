# Ice Hockey Manager


## Assignment
Several human players (at least two) can manage their hockey teams out of a list of real ice hockey teams of several championships across Europe. Human players can pick their team and add / remove ice hockey players from a list of available free agents. There is a schedule of games and results will be generated taking into account the players characteristics (no need to have some advanced algorithm simulating games: just a simple randomization will do it!). Admin can put new hockey players in the main list of free agents and change their attributes before they are selected by other human players. If you want, you can implement a budget system for each team, so that players can be bought and sold based on the financial availability of teams.


## Services

##### Team Management Service
The Team Management Service is responsible for overseeing the administration of teams and players within the system. With this service, administrators or users with the permissions to manage their teams can effectively create, retrieve, update, and delete teams and players as needed.

##### Match Service
The Match Service is designed to manage information related to scheduled matches and proposed match fixtures. This service facilitates the organization and coordination of matches, allowing users to view scheduled matches, propose new matches and accept or reject proposed matches. Additionally, the Match Service includes functionality for simulating matches.

##### Statistics Service
The Statistics Service focuses on collecting and managing statistics related to matches, teams, and players. This service provides APIs for accessing and analyzing match statistics.

##### User Service
The User Service is responsible for managing user information within the system. It maintains user profiles, authentication credentials, and other relevant details. Additionally, this service handles the association between users and their selected team.

## Use Case Diagram
![Use Case Diagram](docs/PA165_Project_use_case.vpd.png "Use Case Diagram")


## Running
From the root directory, execute the following commands :

1,  ```mvn clean install```

2, ```podman compose up``` (or docker if you dont have podman)


To access swaggerUI to see the endpoints, you can use these URL's :

- user service : http://localhost:8080/swagger-ui/index.html
- team-management service : http://localhost:8081/swagger-ui/index.html
- match service : http://localhost:8082/swagger-ui/index.html
- match statistics service : http://localhost:8083/swagger-ui/index.html

Observability :

- Prometheus is running at  http://localhost:9090 

- Grafana at  http://localhost:3000
  - Normally, the dashboard should be initialized via docker compose run (grafana-init), but :
  - If you are running a windows computer, then you will sadly have to manually 
    configure the dashboard :
    - log into grafana with credentials : username:admin, password:admin at http://localhost:3000/login
    - create your first dashboard
    - add empty panel
    - change datasource from Grafana to Prometheus
    - now, via the metrics browser you can view scraped data from prometheus
  - you can access Grafana without logging in via link http://localhost:3000/?orgId=1&search=open -> Search Dashboard -> Services

## Running Locust script
To run Locust script you have to build services, then build images and then seed team management service.

Seeding team management service: 
```
cat ./team-management/src/main/resources/static/seed_script.sql | podman exec -i team-management-db psql -U postgres -d postgres
```

After that you can run locust as:
```
python -m locust [--challenger-token TOKEN --opponent-token TOKEN]

```