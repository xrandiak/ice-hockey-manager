CONTAINER_ENGINE := $(shell command -v podman 2> /dev/null || echo docker)
COMPOSE_ENGINE := $(shell command -v podman-compose 2> /dev/null || echo docker-compose)

# Team Management PostgreSQL configuration
POSTGRES_CONTAINER_NAME := team-management-db
POSTGRES_DB := postgres
POSTGRES_USER := postgres

# Database queries
SEED_TEAM_MANAGEMENT_SQL := ./team-management/src/main/resources/static/seed_script.sql 

.PHONY: test build clean start stop run run-locust locust

test:
		mvn install

build:
		mvn install -DskipTests

clean:
		$(CONTAINER_ENGINE) images | grep "ice-hockey-manager" | awk '{print $$3F}' | xargs $(CONTAINER_ENGINE) rmi -f

start:
		$(COMPOSE_ENGINE) up -d

stop:
		$(COMPOSE_ENGINE) down

run: stop clean build start

wait-for-tm-db:
	@echo "Waiting for Team Management database to load up"
	sleep 20

run-locust:
	cat $(SEED_TEAM_MANAGEMENT_SQL) | $(CONTAINER_ENGINE) exec -i $(POSTGRES_CONTAINER_NAME) psql -U $(POSTGRES_USER) -d $(POSTGRES_DB)
	python -m locust

locust: run wait-for-tm-db run-locust

