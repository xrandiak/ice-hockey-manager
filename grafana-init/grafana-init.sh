#!/bin/sh
TRIES=10
for I in $(seq $TRIES); do
    echo ":: Checking if Grafana API is available ($I/$TRIES)"
    curl --no-progress-meter -u admin:admin "grafana:3000/api/admin/stats" && break 2
    if [ "$I" == "$TRIES" ]; then
        echo ":: Grafana API not available"
        exit 1
    fi
    sleep 3
done
echo
echo ":: Setting up dashboard"
curl --no-progress-meter -u admin:admin -X POST -H "Content-Type: application/json" -d @dashboard.json "grafana:3000/api/dashboards/db"
echo
