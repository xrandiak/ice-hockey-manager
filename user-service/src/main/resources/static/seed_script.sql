TRUNCATE TABLE users;
ALTER SEQUENCE users_user_id_seq RESTART WITH 1;

INSERT INTO users (disabled,name,role,team_id) VALUES (false,'Morsilus','User',null);
INSERT INTO users (disabled,name,role,team_id) VALUES (false,'Domino','User',null);
INSERT INTO users (disabled,name,role,team_id) VALUES (false,'p1r0h','User',null);
INSERT INTO users (disabled,name,role,team_id) VALUES (false,'Peder','User',null);

INSERT INTO users (disabled,name,role,team_id) VALUES (true,'Admin','Admin',null);


