package cz.muni.fi.pa165.services;

import cz.muni.fi.pa165.configs.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

/**
 * @author Robert Randiak
 *
 * Interface for interacting with the Team Service.
 */

@Component
@DependsOn("webClientBuilder")
@FeignClient(name = "team-service-client",
        url = "${team-service.url}",
        configuration = FeignConfig.class
)
public interface TeamService {
    /**
     * Endpoint to set a team as assigned.
     *
     * @param teamId The ID of the team to set as assigned.
     */
    @PatchMapping("/teams/take-team/{teamId}")
    void setTeamAssigned(@PathVariable Long teamId);

    /**
     * Endpoint to unset a team as assigned.
     *
     * @param teamId The ID of the team to unset as assigned.
     */
    @PatchMapping("/teams/untake-team/{teamId}")
    void unsetTeamAssigned(@PathVariable Long teamId);

    /**
     * Endpoint to add a player to a team.
     *
     * @param playerId The ID of the player to add to the team.
     * @param teamId   The ID of the team to add the player to.
     */
    @PutMapping("/teams-agents/sign-up-agent/{teamId}/{playerId}")
    void addPlayerToTeam(@PathVariable Long playerId, @PathVariable Long teamId);

    /**
     * Endpoint to remove a player from a team.
     *
     * @param playerId The ID of the player to remove from the team.
     * @param teamId   The ID of the team to remove the player from.
     */
    @PutMapping("/teams-agents/cut-contract/{teamId}/{playerId}")
    void removePlayerFromTeam(@PathVariable Long playerId, @PathVariable Long teamId);
}
