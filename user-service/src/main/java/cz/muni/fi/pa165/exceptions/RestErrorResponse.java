package cz.muni.fi.pa165.exceptions;

import java.time.LocalDateTime;

/**
 * @author Robert Randiak
 */
public class RestErrorResponse {
    private int status;
    private String error;
    private String message;
    private LocalDateTime timestamp;

    public RestErrorResponse(int status, String error, String message) {
        this.status = status;
        this.error = error;
        this.message = message;
        this.timestamp = LocalDateTime.now();
    }

    public int getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }
}
