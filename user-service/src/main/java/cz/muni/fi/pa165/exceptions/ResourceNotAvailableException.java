package cz.muni.fi.pa165.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Robert Randiak
 */

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class ResourceNotAvailableException extends RestException {
    public ResourceNotAvailableException(String message) {
        super(message, HttpStatus.SERVICE_UNAVAILABLE);
    }

    public ResourceNotAvailableException(String message, Throwable cause) {
        super(message, cause, HttpStatus.SERVICE_UNAVAILABLE);
    }
}
