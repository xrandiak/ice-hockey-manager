package cz.muni.fi.pa165.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Robert Randiak
 */

@ResponseStatus(HttpStatus.CONFLICT)
public class UniqueViolationException extends RestException {
    public UniqueViolationException(String message) {
        super(message, HttpStatus.CONFLICT);
    }
}
