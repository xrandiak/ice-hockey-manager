package cz.muni.fi.pa165.configs;

import cz.muni.fi.pa165.exceptions.RestErrorResponse;
import cz.muni.fi.pa165.exceptions.RestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.ConnectException;

/**
 * @author Robert Randiak
 */

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<RestErrorResponse> handleConnectException(ConnectException exception) {
        return new ResponseEntity<>(
                new RestErrorResponse(
                        HttpStatus.SERVICE_UNAVAILABLE.value(),
                        HttpStatus.SERVICE_UNAVAILABLE.name(),
                        exception.toString()),
                HttpStatus.SERVICE_UNAVAILABLE
        );
    }

    @ExceptionHandler
    public ResponseEntity<RestErrorResponse> handleRestException(RestException exception) {
        return new ResponseEntity<>(
                new RestErrorResponse(
                        exception.getHttpStatus().value(),
                        exception.getHttpStatus().name(),
                        exception.getMessage()),
                exception.getHttpStatus()
        );
    }
}
