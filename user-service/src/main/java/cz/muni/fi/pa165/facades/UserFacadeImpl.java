package cz.muni.fi.pa165.facades;

import cz.muni.fi.pa165.exceptions.UsernameAlreadyTakenException;
import cz.muni.fi.pa165.mappers.UserMapper;
import cz.muni.fi.pa165.services.UserService;
import cz.muni.pa165.generated.rest.model.CreateUserRequest;
import cz.muni.pa165.generated.rest.model.Role;
import cz.muni.pa165.generated.rest.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Robert Randiak
 */

@Service
public class UserFacadeImpl implements UserFacade {
    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public UserFacadeImpl(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @Override
    public UserDTO createUser(CreateUserRequest createUserRequest) {
        UserDTO user = this.userMapper.createUserRequestToUserDto(createUserRequest);

        if (this.userService.userExists(user)) {
            throw new UsernameAlreadyTakenException();
        }

        // TODO: find out why defaulting in model does not work (for enums).
        if (user.getRole() == null) {
            user.role(Role.USER);
        }

        return this.userMapper.userModelToUserDto(
                this.userService.createUser(this.userMapper.userDtoToUserModel(user))
        );
    }

    @Override
    public UserDTO getUserByName(String username) {
        UserDTO user = this.userMapper.usernameToUserDto(username);

        return this.userMapper.userModelToUserDto(
                this.userService.getUserByName(user)
        );
    }
}
