package cz.muni.fi.pa165.controllers;

import cz.muni.fi.pa165.facades.UserCatalogFacade;
import cz.muni.pa165.generated.rest.api.UserCatalogControllerApiDelegate;
import cz.muni.pa165.generated.rest.model.Role;
import cz.muni.pa165.generated.rest.model.UserDTO;
import cz.muni.pa165.generated.rest.model.UsersPageDTO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

/**
 * @author Robert Randiak
 */

@Controller
public class UserCatalogController implements UserCatalogControllerApiDelegate {
    private UserCatalogFacade userCatalogFacade;

    @Autowired
    public UserCatalogController(UserCatalogFacade userCatalogFacade) {
        this.userCatalogFacade = userCatalogFacade;
    }

    @Override
    public ResponseEntity<UsersPageDTO> getUsers(Integer page, Integer limit, String name, Role role, Integer teamId, Boolean disabled) {
        // TODO: Only Admin can filter disabled users. Implement in milestone 3.
        boolean filterByDisabled = false;

        if (filterByDisabled) {
            return new ResponseEntity<>(this.userCatalogFacade.getUsers(
                    page, limit, name, role, teamId, true
            ), HttpStatus.OK);
        }

        return new ResponseEntity<>(this.userCatalogFacade.getUsers(
                page, limit, name, role, teamId, false
        ), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDTO> getUser(Long id) {
        return new ResponseEntity<>(this.userCatalogFacade.getUserById(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<UserDTO>> getUsersByIds(List<Long> ids) {
        return new ResponseEntity<>(ids.stream().map(id -> this.userCatalogFacade.getUserById(id)).toList(), HttpStatus.OK);
    }

    // TODO: Inject permission enforcement (current user or admin) in milestone 3.
    @Override
    public ResponseEntity<UserDTO> updateUser(Long id, UserDTO user) {
        return new ResponseEntity<>(this.userCatalogFacade.updateUser(id, user), HttpStatus.OK);
    }

    // TODO: Inject permissions enforcement in milestone 3.
    @Override
    public ResponseEntity<Void> deleteUser(Long id) {
        this.userCatalogFacade.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
