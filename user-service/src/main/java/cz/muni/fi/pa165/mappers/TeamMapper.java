package cz.muni.fi.pa165.mappers;

import cz.muni.pa165.generated.rest.model.PickPlayerRequest;
import cz.muni.pa165.generated.rest.model.PickTeamRequest;
import org.mapstruct.Mapper;

/**
 * @author Robert Randiak
 */

@Mapper(componentModel = "spring")
public interface TeamMapper {
    default Long pickTeamRequestToTeamId(PickTeamRequest pickTeamRequest) {
        return pickTeamRequest.getTeamId();
    }

    default Long pickPlayerRequestToPlayerId(PickPlayerRequest pickPlayerRequest) {
        return pickPlayerRequest.getPlayerId();
    }
}
