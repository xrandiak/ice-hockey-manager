package cz.muni.fi.pa165.services;

import cz.muni.fi.pa165.repositories.models.UserModel;
import cz.muni.fi.pa165.repositories.models.RoleType;
import cz.muni.pa165.generated.rest.model.UserDTO;
import org.springframework.data.domain.Page;

/**
 * @author Robert Randiak
 *
 * Interface for managing user-related operations.
 */
public interface UserService {

    /**
     * Checks if a user exists.
     *
     * @param user The user DTO object.
     * @return True if the user exists, false otherwise.
     */
    boolean userExists(UserDTO user);

    /**
     * Creates a new user.
     *
     * @param user The user model object to create.
     * @return The created user model.
     */
    UserModel createUser(UserModel user);

    /**
     * Updates an existing user.
     *
     * @param id   The ID of the user to update.
     * @param user The updated user model object.
     * @return The updated user model.
     */
    UserModel updateUser(Long id, UserModel user);

    /**
     * Deletes a user.
     *
     * @param id The ID of the user to delete.
     * @return The deleted user model.
     */
    UserModel deleteUser(Long id);

    /**
     * Retrieves a user by ID.
     *
     * @param id The ID of the user to retrieve.
     * @return The retrieved user model.
     */
    UserModel getUser(Long id);

    /**
     * Retrieves a user by name.
     *
     * @param user The user DTO object containing the name.
     * @return The retrieved user model.
     */
    UserModel getUserByName(UserDTO user);

    /**
     * Retrieves a page of users based on filtering criteria.
     *
     * @param page      The page number.
     * @param pageSize  The size of each page.
     * @param name      The name to filter users by.
     * @param role      The role to filter users by.
     * @param teamId    The team ID to filter users by.
     * @param disabled  The disabled status to filter users by.
     * @return A page of user models.
     */
    Page<UserModel> getUsers(int page, int pageSize,
                             String name, RoleType role, Integer teamId, boolean disabled);

    /**
     * Retrieves the total count of users.
     *
     * @return The total count of users.
     */
    long getUserCount();
}
