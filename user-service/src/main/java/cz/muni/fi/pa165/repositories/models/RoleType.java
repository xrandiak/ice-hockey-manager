package cz.muni.fi.pa165.repositories.models;

/**
 * @author Robert Randiak
 */
public enum RoleType {
    Admin("Admin"), User("User");

    private String value;

    RoleType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
