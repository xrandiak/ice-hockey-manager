package cz.muni.fi.pa165.exceptions;

import org.springframework.http.HttpStatus;

/**
 * @author Robert Randiak
 */

public class RestException extends RuntimeException {
    private final HttpStatus httpStatus;

    public RestException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public RestException(String message, Throwable cause, HttpStatus httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
