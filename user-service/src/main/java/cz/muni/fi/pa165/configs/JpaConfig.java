package cz.muni.fi.pa165.configs;

/**
 * @author Robert Randiak
 */

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "cz.muni.fi.pa165")
@EntityScan(basePackages = "cz.muni.fi.pa165.repositories.models")
public class JpaConfig {
}
