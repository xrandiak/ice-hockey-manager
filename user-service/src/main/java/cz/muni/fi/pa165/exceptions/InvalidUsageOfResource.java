package cz.muni.fi.pa165.exceptions;

/**
 * @author Robert Randiak
 */
public class InvalidUsageOfResource extends BadRequestException {
    public InvalidUsageOfResource(String message) {
        super(message);
    }
}
