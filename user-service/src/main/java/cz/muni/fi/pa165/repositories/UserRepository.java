package cz.muni.fi.pa165.repositories;

import cz.muni.fi.pa165.repositories.models.UserModel;
import cz.muni.fi.pa165.repositories.models.RoleType;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Robert Randiak
 */

@Repository
public interface UserRepository extends JpaRepository<UserModel, Long> {
    @Query("SELECT u FROM UserModel u WHERE :name = u.name")
    Optional<UserModel> findByName(String name);

    @Query("SELECT u FROM UserModel u " +
            "WHERE (:name is null or u.name LIKE :name)" +
            "AND (:role is null or u.role = :role) " +
            "AND (:teamId is null or u.teamId = :teamId) " +
            "AND (:disabled is null or u.disabled = :disabled)")
    Page<UserModel> getUsers(Pageable pageable,
                             String name, RoleType role, Integer teamId, boolean disabled);
}
