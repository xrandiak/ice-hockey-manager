package cz.muni.fi.pa165.facades;

import cz.muni.pa165.generated.rest.model.PickPlayerRequest;
import cz.muni.pa165.generated.rest.model.PickTeamRequest;

/**
 * @author Robert Randiak
 *
 * Interface for managing user teams.
 */
public interface UserTeamFacade {
    /**
     * Picks a team for the user.
     *
     * @param pickTeamRequest The request containing information about the team to pick.
     * @param username        The username of the user picking the team.
     * @return Void
     */
    Void pickTeam(PickTeamRequest pickTeamRequest, String username);

    /**
     * Drops the team assigned to the user.
     *
     * @param username The username of the user dropping the team.
     * @return Void
     */
    Void dropTeam(String username);

    /**
     * Picks a player for the user's team.
     *
     * @param pickPlayerRequest The request containing information about the player to pick.
     * @param username          The username of the user picking the player.
     * @return Void
     */
    Void pickPlayer(PickPlayerRequest pickPlayerRequest, String username);

    /**
     * Drops a player from the user's team.
     *
     * @param pickPlayerRequest The request containing information about the player to drop.
     * @param username          The username of the user dropping the player.
     * @return Void
     */
    Void dropPlayer(PickPlayerRequest pickPlayerRequest, String username);
}
