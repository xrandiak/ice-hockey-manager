package cz.muni.fi.pa165.facades;

import cz.muni.fi.pa165.exceptions.BadRequestException;
import cz.muni.fi.pa165.mappers.TeamMapper;
import cz.muni.fi.pa165.mappers.UserMapper;
import cz.muni.fi.pa165.repositories.models.UserModel;
import cz.muni.fi.pa165.services.TeamService;
import cz.muni.fi.pa165.services.UserService;
import cz.muni.pa165.generated.rest.model.PickPlayerRequest;
import cz.muni.pa165.generated.rest.model.PickTeamRequest;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Robert Randiak
 */

@Service
public class UserTeamFacadeImpl implements UserTeamFacade {
    private UserService userService;
    private TeamService teamService;
    private UserMapper userMapper;
    private TeamMapper teamMapper;

    @Autowired
    public UserTeamFacadeImpl(UserService userService, TeamService teamService,
                              UserMapper userMapper, TeamMapper teamMapper) {
        this.userService = userService;
        this.teamService = teamService;
        this.userMapper = userMapper;
        this.teamMapper = teamMapper;
    }

    @Transactional
    @Override
    public Void pickTeam(PickTeamRequest pickTeamRequest, String username) {
        UserModel user = this.userService.getUserByName(this.userMapper.usernameToUserDto(username));

        if (user.getTeamId() != null) {
            throw new BadRequestException("User have already team assigned");
        }

        Long teamId = this.teamMapper.pickTeamRequestToTeamId(pickTeamRequest);
        user.setTeamId(teamId);

        this.userService.updateUser(user.getId(), user);
        this.teamService.setTeamAssigned(teamId);

        return null;
    }

    @Transactional
    @Override
    public Void dropTeam(String username) {
        UserModel user = this.userService.getUserByName(this.userMapper.usernameToUserDto(username));

        if (user.getTeamId() == null) {
            throw new BadRequestException("User does not have any team assigned");
        }

        Long teamId = user.getTeamId();
        user.setTeamId(null);

        this.userService.updateUser(user.getId(), user);
        this.teamService.unsetTeamAssigned(teamId);

        return null;
    }

    @Override
    public Void pickPlayer(PickPlayerRequest pickPlayerRequest, String username) {
        UserModel user = this.userService.getUserByName(this.userMapper.usernameToUserDto(username));

        if (user.getTeamId() == null) {
            throw new BadRequestException("User does not have any team assigned");
        }

        this.teamService.addPlayerToTeam(
                this.teamMapper.pickPlayerRequestToPlayerId(pickPlayerRequest),
                user.getTeamId()
        );

        return null;
    }

    @Override
    public Void dropPlayer(PickPlayerRequest pickPlayerRequest, String username) {
        UserModel user = this.userService.getUserByName(this.userMapper.usernameToUserDto(username));

        if (user.getTeamId() == null) {
            throw new BadRequestException("User does not have any team assigned");
        }

        this.teamService.removePlayerFromTeam(
                this.teamMapper.pickPlayerRequestToPlayerId(pickPlayerRequest),
                user.getTeamId()
        );

        return null;
    }
}
