package cz.muni.fi.pa165.mappers;

import cz.muni.fi.pa165.repositories.models.UserModel;
import cz.muni.fi.pa165.repositories.models.RoleType;
import cz.muni.pa165.generated.rest.model.CreateUserRequest;
import cz.muni.pa165.generated.rest.model.Role;
import cz.muni.pa165.generated.rest.model.UserDTO;
import cz.muni.pa165.generated.rest.model.UsersPageDTO;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;

/**
 * @author Robert Randiak
 */

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDTO createUserRequestToUserDto(CreateUserRequest createUserRequest);

    default UserDTO usernameToUserDto(String username) {
        return new UserDTO()
                .name(username);
    }

    default UserDTO userModelToUserDto(UserModel user) {
        return new UserDTO()
                .id(user.getId())
                .name(user.getName())
                .role(this.roleTypeToDtoRole(user.getRole()))
                .teamId(user.getTeamId());
    }

    UserModel userDtoToUserModel(UserDTO user);

    default UsersPageDTO userModelsToUserPageDto(Page<UserModel> userModels, long numFound, long numTotal) {
        return new UsersPageDTO()
                .users(userModels.getContent().stream().map(this::userModelToUserDto).toList())
                .numFound(numFound)
                .numTotal(numTotal);
    }

    default Role roleTypeToDtoRole(RoleType role) {
        return role != null ? switch (role) {
            case Admin -> Role.ADMIN;
            case User -> Role.USER;
        } : null;
    }

    default RoleType dtoRoleToRoleType(Role role) {
        return role != null ? switch (role) {
            case ADMIN -> RoleType.Admin;
            case USER -> RoleType.User;
        } : null;
    }
}
