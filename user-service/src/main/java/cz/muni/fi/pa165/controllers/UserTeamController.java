package cz.muni.fi.pa165.controllers;

import cz.muni.fi.pa165.facades.UserTeamFacade;
import cz.muni.fi.pa165.security.AuthProvider;
import cz.muni.pa165.generated.rest.api.UserTeamControllerApiDelegate;
import cz.muni.pa165.generated.rest.model.DropTeamRequest;
import cz.muni.pa165.generated.rest.model.PickPlayerRequest;
import cz.muni.pa165.generated.rest.model.PickTeamRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

/**
 * @author Robert Randiak
 */

// TODO: Inject permissions enforcement in milestone 2 to all api calls in this controller.

@Controller
public class UserTeamController implements UserTeamControllerApiDelegate {
    private final AuthProvider authProvider;
    private final UserTeamFacade userTeamFacade;

    @Autowired
    public UserTeamController(AuthProvider authProvider, UserTeamFacade userTeamFacade) {
        this.authProvider = authProvider;
        this.userTeamFacade = userTeamFacade;
    }

    @Override
    public ResponseEntity<Void> pickTeam(PickTeamRequest pickTeamRequest) {
        // TODO: fix after security is working
        return new ResponseEntity<>(
                this.userTeamFacade.pickTeam(
                        pickTeamRequest,
                        pickTeamRequest.getUsername()
//                        this.authProvider.getUsername()
                        ),
                HttpStatus.OK
        );
    }

    @Override
    public ResponseEntity<Void> dropTeam(DropTeamRequest dropTeamRequest) {
        // TODO: fix after security is working
        return new ResponseEntity<>(
                this.userTeamFacade.dropTeam(
                        dropTeamRequest.getUsername()
//                        this.authProvider.getUsername()
                        ),
                HttpStatus.OK
        );
    }

    @Override
    public ResponseEntity<Void> pickPlayer(PickPlayerRequest pickPlayerRequest) {
        // TODO: fix after security is working
        return new ResponseEntity<>(
                this.userTeamFacade.pickPlayer(
                        pickPlayerRequest,
                        pickPlayerRequest.getUsername()
//                        this.authProvider.getUsername()
                ),
                HttpStatus.OK
        );
    }

    @Override
    public ResponseEntity<Void> dropPlayer(PickPlayerRequest pickPlayerRequest) {
        // TODO: fix after security is working
        return new ResponseEntity<>(
                this.userTeamFacade.dropPlayer(
                        pickPlayerRequest,
                        pickPlayerRequest.getUsername()
//                        this.authProvider.getUsername()
                ),
                HttpStatus.OK
        );
    }
}
