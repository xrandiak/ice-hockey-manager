package cz.muni.fi.pa165.exceptions;

/**
 * @author Robert Randiak
 */
public class UserNotFoundException extends ResourceNotFoundException {
    public UserNotFoundException() {
        super("User not found");
    }
}
