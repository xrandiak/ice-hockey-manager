package cz.muni.fi.pa165.exceptions;

/**
 * @author Robert Randiak
 */
public class TeamNotFoundException extends ResourceNotFoundException {
    public TeamNotFoundException() {
        super("Team not found");
    }
}
