package cz.muni.fi.pa165.facades;

import cz.muni.pa165.generated.rest.model.Role;
import cz.muni.pa165.generated.rest.model.UserDTO;
import cz.muni.pa165.generated.rest.model.UsersPageDTO;

/**
 * @author Robert Randiak
 *
 * Interface for managing user catalog operations.
*/
public interface UserCatalogFacade {
    /**
     * Retrieves a page of users based on the provided parameters.
     *
     * @param page     The page number for pagination.
     * @param limit    The number of users per page.
     * @param name     Filter users by name.
     * @param role     Filter users by role.
     * @param teamId   Filter users by team ID.
     * @param disabled Filter users by disabled status.
     * @return A DTO representing the page of users.
     */
    UsersPageDTO getUsers(Integer page, Integer limit, String name, Role role, Integer teamId, boolean disabled);

    /**
     * Retrieves a user by their ID.
     *
     * @param id The ID of the user to retrieve.
     * @return The DTO representing the retrieved user.
     */
    UserDTO getUserById(Long id);

    /**
     * Updates a user with the provided information.
     *
     * @param id   The ID of the user to update.
     * @param user The DTO containing the updated user information.
     * @return The DTO representing the updated user.
     */
    UserDTO updateUser(Long id, UserDTO user);

    /**
     * Deletes a user by their ID.
     *
     * @param id The ID of the user to delete.
     */
    void deleteUser(Long id);
}
