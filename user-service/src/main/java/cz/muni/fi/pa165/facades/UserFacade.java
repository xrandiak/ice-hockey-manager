package cz.muni.fi.pa165.facades;

import cz.muni.pa165.generated.rest.model.CreateUserRequest;
import cz.muni.pa165.generated.rest.model.UserDTO;

/**
 * @author Robert Randiak
 *
 * Interface for managing user-related operations.
 */
public interface UserFacade {
    /**
     * Creates a new user based on the provided request.
     *
     * @param createUserRequest The request containing information to create the user.
     * @return The DTO representing the created user.
     */
    UserDTO createUser(CreateUserRequest createUserRequest);

    /**
     * Retrieves a user by their username.
     *
     * @param username The username of the user to retrieve.
     * @return The DTO representing the retrieved user.
     */
    UserDTO getUserByName(String username);
}
