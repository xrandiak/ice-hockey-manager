package cz.muni.fi.pa165.facades;

import cz.muni.fi.pa165.mappers.UserMapper;
import cz.muni.fi.pa165.repositories.models.UserModel;
import cz.muni.fi.pa165.services.UserServiceImpl;
import cz.muni.fi.pa165.repositories.models.RoleType;
import cz.muni.pa165.generated.rest.model.Role;
import cz.muni.pa165.generated.rest.model.UserDTO;
import cz.muni.pa165.generated.rest.model.UsersPageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

/**
 * @author Robert Randiak
 */

@Service
public class UserCatalogFacadeImpl implements UserCatalogFacade {
    private final int DEFAULT_PAGE_SIZE = 20;
    private static final int MAX_PAGE_SIZE = 100;

    private final UserServiceImpl userService;
    private final UserMapper userMapper;

    @Autowired
    public UserCatalogFacadeImpl(UserServiceImpl userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @Override
    public UsersPageDTO getUsers(Integer page, Integer limit,
                                 String name, Role role, Integer teamId, boolean disabled) {
        int page_ = page != null ? page - 1 : 0;
        int pageSize = limit != null && limit > 0
                ? limit < MAX_PAGE_SIZE
                    ? limit : MAX_PAGE_SIZE
                : DEFAULT_PAGE_SIZE;
        RoleType roleType = this.userMapper.dtoRoleToRoleType(role);
        Page<UserModel> users = this.userService.getUsers(page_, pageSize, name, roleType, teamId, disabled);
        return this.userMapper.userModelsToUserPageDto(
                users,
                users.getTotalElements(),
                this.userService.getUserCount()
        );
    }

    @Override
    public UserDTO getUserById(Long id) {
        return this.userMapper.userModelToUserDto(this.userService.getUser(id));
    }

    @Override
    public UserDTO updateUser(Long id, UserDTO user) {
        return this.userMapper.userModelToUserDto(
            this.userService.updateUser(id, this.userMapper.userDtoToUserModel(user))
        );
    }

    @Override
    public void deleteUser(Long id) {
        this.userMapper.userModelToUserDto(this.userService.deleteUser(id));
    }
}
