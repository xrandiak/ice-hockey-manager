package cz.muni.fi.pa165.exceptions;

/**
 * @author Robert Randiak
 */
public class UsernameAlreadyTakenException extends UniqueViolationException {
    public UsernameAlreadyTakenException() {
        super("Username is already taken");
    }
}
