package cz.muni.fi.pa165.controllers;

import cz.muni.fi.pa165.facades.UserCatalogFacade;
import cz.muni.fi.pa165.facades.UserFacade;
import cz.muni.fi.pa165.security.AuthProvider;
import cz.muni.pa165.generated.rest.api.UserControllerApiDelegate;
import cz.muni.pa165.generated.rest.model.CreateUserRequest;
import cz.muni.pa165.generated.rest.model.UserDTO;
import cz.muni.pa165.generated.rest.model.UsersPageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.core.annotation.AuthenticationPrincipal;
//import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.servlet.ModelAndView;

/**
 * @author Robert Randiak
 */

@Controller
public class UserController implements UserControllerApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    private final AuthProvider authProvider;
    private UserFacade userFacade;
    // TODO: Temporary fix with catalog facade. Remove after security is implemented.
    private UserCatalogFacade userCatalogFacade;

    @Autowired
    public UserController(
            AuthProvider authProvider,
            UserFacade userFacade,
            UserCatalogFacade userCatalogFacade) {
        this.authProvider = authProvider;
        this.userFacade = userFacade;
        this.userCatalogFacade = userCatalogFacade;
    }

    /**
     * Home page accessible even to non-authenticated users. Displays user personal data.
     */
    /* @GetMapping("/")
    public ModelAndView index(Model model, @AuthenticationPrincipal OidcUser user) {
        log.info("********************************************************");
        log.info("* index() called                                       *");
        log.info("********************************************************");

        // put obtained user data into a model attribute named "user"
        if (user != null) {
            log.info("TOKEN: "+ user.getIdToken().getTokenValue());
            model.addAttribute("user", user);
        }

        return new ModelAndView("index");
    } */


    @Override
    public ResponseEntity<UserDTO> createUser(CreateUserRequest createUserRequest) {
        return new ResponseEntity<>(
                this.userFacade.createUser(createUserRequest),
                HttpStatus.CREATED
        );
    }

    @Override
    public ResponseEntity<UserDTO> getCurrentUser() {
        // TODO: Temporary fix with catalog facade. Remove after security is implemented.
		if (this.userCatalogFacade != null) {
			UsersPageDTO users = this.userCatalogFacade.getUsers(
					1, 10, null, null, null, false
			);
			if (!users.getUsers().isEmpty()) {
				return new ResponseEntity<>(
						users.getUsers().get(0),
						HttpStatus.OK
				);
			}
		}
        return new ResponseEntity<>(
                this.userFacade.getUserByName(this.authProvider.getUsername()),
                HttpStatus.OK
        );
    }
}
