package cz.muni.fi.pa165.configs;

import feign.okhttp.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Robert Randiak
 */

@Configuration
public class FeignConfig {
    @Bean
    public OkHttpClient client() {
        return new OkHttpClient();
    }

}
