package cz.muni.fi.pa165.services;

import cz.muni.fi.pa165.exceptions.InvalidUsageOfResource;
import cz.muni.fi.pa165.exceptions.UserNotFoundException;
import cz.muni.fi.pa165.exceptions.UsernameAlreadyTakenException;
import cz.muni.fi.pa165.repositories.models.UserModel;
import cz.muni.fi.pa165.repositories.UserRepository;
import cz.muni.fi.pa165.repositories.models.RoleType;
import cz.muni.pa165.generated.rest.model.UserDTO;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author Robert Randiak
 */

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean userExists(UserDTO user) {
        return this.userRepository.findByName(user.getName()).isPresent();
    }

    @Transactional
    @Override
    public UserModel createUser(UserModel user) {
        if (this.userRepository.findByName(user.getName()).isPresent()) {
            throw new UsernameAlreadyTakenException();
        }
        return this.userRepository.save(user);
    }

    @Transactional
    @Override
    public UserModel getUserByName(UserDTO user) {
        return this.userRepository.findByName(user.getName())
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public Page<UserModel> getUsers(int page, int pageSize,
                                    String name, RoleType role, Integer teamId, boolean disabled) {
        return this.userRepository.getUsers(PageRequest.of(page, pageSize), name, role, teamId, disabled);
    }

    @Override
    public long getUserCount() {
        return this.userRepository.count();
    }

    @Override
    public UserModel getUser(Long id) {
        return this.userRepository.findById(id)
                .orElseThrow(UserNotFoundException::new);
    }

    @Transactional
    @Override
    public UserModel updateUser(Long id, UserModel user) {
        UserModel userModel = this.userRepository.findById(id)
                .orElseThrow(UserNotFoundException::new);

        if (!Objects.equals(userModel.getTeamId(), user.getTeamId())) {
            throw new InvalidUsageOfResource(
                    "Changing team for a user should be performed through one of dedicated methods: " +
                    "TeamService.setTeamAssigned, TeamService.unsetTeamAssigned"
            );
        }

        return this.userRepository.save(user);
    }

    @Transactional
    @Override
    public UserModel deleteUser(Long id) {
        UserModel user = this.userRepository.findById(id)
                .orElseThrow(UserNotFoundException::new);

        user.setDisabled(true);

        return this.userRepository.save(user);
    }
}
