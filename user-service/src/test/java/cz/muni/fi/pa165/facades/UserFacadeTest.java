package cz.muni.fi.pa165.facades;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import cz.muni.fi.pa165.exceptions.UsernameAlreadyTakenException;
import cz.muni.fi.pa165.mappers.UserMapper;
import cz.muni.fi.pa165.repositories.models.UserModel;
import cz.muni.fi.pa165.services.UserService;
import cz.muni.pa165.generated.rest.model.CreateUserRequest;
import cz.muni.pa165.generated.rest.model.UserDTO;

@ExtendWith(MockitoExtension.class)
public class UserFacadeTest {
    @Mock
    private UserService userService;
    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserFacadeImpl userFacade;

    UserDTO testUserDto;
    UserModel testUserModel;

    @BeforeEach
    void userFacadeTestsSetup() {
        testUserDto = new UserDTO();
        testUserDto.setId(1L);
        testUserDto.setName("Test");

        testUserModel = new UserModel();
        testUserModel.setId(1L);
        testUserModel.setName("Test");
    }

    @Test
    void createUser_newUser_returnsUserDto() {
        // TODO: update test. Repository interface was changed.
//        var req = new CreateUserRequest();
//        req.setName(testUserModel.getName());
//
//        Mockito.when(userMapper.createUserRequestToUserDto(req)).thenReturn(testUserDto);
//        Mockito.when(userService.userExists(testUserDto)).thenReturn(false);
//        Mockito.when(userService.createUser(testUserDto)).thenReturn(testUserModel);
//        Mockito.when(userMapper.userModelToUserDto(testUserModel)).thenReturn(testUserDto);
//
//        UserDTO createdUser = userFacade.createUser(req);
//
//        assertNotNull(createdUser);
//        assertThat(createdUser.getName()).isEqualTo(testUserDto.getName());
//        assertThat(createdUser.getId()).isEqualTo(testUserDto.getId());
    }

    @Test
    void createUser_existingUser_throwUsernameAlreadyTaken() {
        var req = new CreateUserRequest();
        req.setName(testUserModel.getName());

        Mockito.when(userMapper.createUserRequestToUserDto(req)).thenReturn(testUserDto);
        Mockito.when(userService.userExists(testUserDto)).thenReturn(true);
        
        assertThrows(UsernameAlreadyTakenException.class, 
            () -> userFacade.createUser(req));
    }
    
    @Test
    void getUserByName_existingUser_returnsUser() {

        Mockito.when(userMapper.usernameToUserDto(testUserDto.getName())).thenReturn(testUserDto);
        Mockito.when(userService.getUserByName(testUserDto)).thenReturn(testUserModel);
        Mockito.when(userMapper.userModelToUserDto(testUserModel)).thenReturn(testUserDto);

        var user = userFacade.getUserByName(testUserDto.getName());

        assertNotNull(user);
        assertThat(user.getName()).isEqualTo(testUserDto.getName());
        assertThat(user.getId()).isEqualTo(testUserDto.getId());
    }

}

