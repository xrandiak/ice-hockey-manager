package cz.muni.fi.pa165.integrations;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.fi.pa165.repositories.UserRepository;
import cz.muni.fi.pa165.repositories.models.RoleType;
import cz.muni.fi.pa165.repositories.models.UserModel;
import cz.muni.fi.pa165.services.TeamService;
import cz.muni.pa165.generated.rest.model.PickTeamRequest;
import cz.muni.pa165.generated.rest.model.UsersPageDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.support.TransactionTemplate;

import java.nio.charset.StandardCharsets;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author Robert Randiak
 */

@SpringBootTest
@AutoConfigureMockMvc
class UserCatalogIntegrationsTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TeamService teamService;

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            .registerModule(new JavaTimeModule())
            .setPropertyNamingStrategy(new PropertyNamingStrategies.SnakeCaseStrategy())
            .setSerializationInclusion(JsonInclude.Include.NON_NULL);

    @BeforeAll
    public static void initData(@Autowired UserRepository userRepository,
                                @Autowired TransactionTemplate transactionTemplate
    ) {
        transactionTemplate.execute(status -> {
            UserModel userModel1 = new UserModel();
            userModel1.setId(1L);
            userModel1.setName("Robert");
            userModel1.setRole(RoleType.Admin);
            userModel1.setTeamId(3L);
            userRepository.save(userModel1);

            UserModel userModel2 = new UserModel();
            userModel2.setId(2L);
            userModel2.setName("Andrej");
            userModel2.setRole(RoleType.User);
            userModel2.setDisabled(true);
            userRepository.save(userModel2);

            UserModel userModel3 = new UserModel();
            userModel3.setId(4L);
            userModel3.setName("Pedro");
            userModel3.setRole(RoleType.User);
            userModel3.setTeamId(11L);
            userModel3.setDisabled(false);
            userRepository.save(userModel3);

            UserModel userModel4 = new UserModel();
            userModel3.setId(5L);
            userModel3.setName("user");
            userModel3.setRole(RoleType.User);
            userRepository.save(userModel4);

            return null;
        });
    }

    // Does not work, because of OAuth2
    // TODO: fix
    /* @Test
    void deleteUser_userExists_returnsNoContent() throws Exception {
        // Act
        String responseJson = mockMvc.perform(
                delete("/users/{id}", 1L))
                .andExpect(status().isNoContent())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        // Assert
        assertThat(responseJson).isEmpty();
    }

    @Test
    void getUsers_filteredUsers_returnsNonEmpty() throws Exception {
        // Act
        String responseJson = mockMvc.perform(
                        get("/users")
                                .param("role", "User")
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        // Assert
        UsersPageDTO response = OBJECT_MAPPER.readValue(responseJson, UsersPageDTO.class);
        assertThat(response.getNumFound()).isEqualTo(2);
        assertThat(response.getNumTotal()).isEqualTo(4);
    } */


    /* Working auth is probably needed for this test to pass
    @Test
    void pickTeam_userAndTeamExists_returnsOK() throws Exception {
        // Arrange
        PickTeamRequest pickTeamRequest = new PickTeamRequest().teamId(1L);

        // Act & Assert
        String responseJson = mockMvc.perform(
                        delete("/user/team")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(pickTeamRequest.toString()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        assertThat(responseJson).isEmpty();
    }
     */
}