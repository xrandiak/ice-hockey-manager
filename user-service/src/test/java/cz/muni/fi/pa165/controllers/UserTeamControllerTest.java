package cz.muni.fi.pa165.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import cz.muni.fi.pa165.facades.UserTeamFacade;
import cz.muni.fi.pa165.security.AuthProvider;
import cz.muni.pa165.generated.rest.model.PickPlayerRequest;
import cz.muni.pa165.generated.rest.model.PickTeamRequest;
import cz.muni.pa165.generated.rest.model.UserDTO;

@ExtendWith(MockitoExtension.class)
public class UserTeamControllerTest {
    @Mock
    private AuthProvider authProvider;

    @Mock
    private UserTeamFacade userTeamFacade;

    @InjectMocks
    private UserTeamController userTeamController;

    UserDTO testUser;
    @BeforeEach
    void userControllerTestsSetup() {
        testUser = new UserDTO();
        testUser.setId(1L);
        testUser.setName("Test");
    }

    @Test
    void pickTeam_teamPickedSuccessfully() {
        var req = new PickTeamRequest();
        req.setTeamId(1L);
        // TODO: remove after security is working
        req.setUsername("user");

//        Mockito.when(authProvider.getUsername()).thenReturn(testUser.getName());
        var responseEntity = userTeamController.pickTeam(req);
        
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
    
    @Test
    void dropTeam_teamPickedSuccessfully() {
        var req = new PickTeamRequest();
        req.setTeamId(1L);
        // TODO: remove after security is working
        req.setUsername("user");

//        Mockito.when(authProvider.getUsername()).thenReturn(testUser.getName());
        var responseEntity = userTeamController.pickTeam(req);
        
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void pickPlayer_teamPickedSuccessfully() {
        var req = new PickPlayerRequest();
        req.setPlayerId(1L);
        // TODO: remove after security is working
        req.setUsername("user");

//        Mockito.when(authProvider.getUsername()).thenReturn(testUser.getName());
        var responseEntity = userTeamController.pickPlayer(req);
        
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void dropPlayer_teamPickedSuccessfully() {
        var req = new PickPlayerRequest();
        req.setPlayerId(1L);
        // TODO: remove after security is working
        req.setUsername("user");

//        Mockito.when(authProvider.getUsername()).thenReturn(testUser.getName());
        var responseEntity = userTeamController.dropPlayer(req);
        
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
    
}
