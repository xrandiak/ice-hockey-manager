package cz.muni.fi.pa165.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import cz.muni.fi.pa165.facades.UserFacade;
import cz.muni.fi.pa165.security.AuthProvider;
import cz.muni.pa165.generated.rest.model.CreateUserRequest;
import cz.muni.pa165.generated.rest.model.UserDTO;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {
    @Mock
    private UserFacade userFacade;

    @Mock
    private AuthProvider authProvider;

    @InjectMocks
    private UserController userController;


    UserDTO testUser;
    @BeforeEach
    void userControllerTestsSetup() {
        testUser = new UserDTO();
        testUser.setId(1L);
        testUser.setName("Test");
    }

    @Test
    void createUser_validName_returnsUserDTO() {
        var req = new CreateUserRequest();
        req.setName(testUser.getName());

        Mockito.when(userFacade.createUser(req)).thenReturn(testUser);
        ResponseEntity<UserDTO> responseEntity = userController.createUser(req);
        
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(responseEntity.getBody().getName()).isEqualTo(testUser.getName());
    }
    
    @Test
    void getCurrentUser_returnsCurrentUser() {

        Mockito.when(authProvider.getUsername()).thenReturn(testUser.getName());
        Mockito.when(userFacade.getUserByName(testUser.getName())).thenReturn(testUser);
        ResponseEntity<UserDTO> responseEntity = userController.getCurrentUser();

        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody().getName()).isEqualTo(testUser.getName());
    }

}

