package cz.muni.fi.pa165.facades;

import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import cz.muni.fi.pa165.exceptions.BadRequestException;
import cz.muni.fi.pa165.mappers.TeamMapper;
import cz.muni.fi.pa165.mappers.UserMapper;
import cz.muni.fi.pa165.repositories.models.UserModel;
import cz.muni.fi.pa165.services.TeamService;
import cz.muni.fi.pa165.services.UserService;
import cz.muni.pa165.generated.rest.model.PickTeamRequest;
import cz.muni.pa165.generated.rest.model.UserDTO;

@ExtendWith(MockitoExtension.class)
public class UserTeamFacadeTest {
    @Mock
    private UserService userService;
    @Mock
    private TeamService teamService;
    @Mock
    private UserMapper userMapper;
    @Mock
    private TeamMapper teamMapper;

    @InjectMocks
    private UserTeamFacadeImpl userTeamFacade;

    UserDTO testUserDto;
    UserModel testUserModel;

    @BeforeEach
    void userFacadeTestsSetup() {
        testUserDto = new UserDTO();
        testUserDto.setId(1L);
        testUserDto.setName("Test");

        testUserModel = new UserModel();
        testUserModel.setId(1L);
        testUserModel.setName("Test");
    }

    @Test
    void pickTeam_hasTeam_teamPickedSuccessfully() {
        var req = new PickTeamRequest();
        req.setTeamId(1L);
        testUserModel.setTeamId(1L);

        Mockito.when(userService.getUserByName(testUserDto))
            .thenReturn(testUserModel);
        Mockito.when(userMapper.usernameToUserDto(testUserDto.getName()))
            .thenReturn(testUserDto);
        
        assertThrows(BadRequestException.class,
            () -> userTeamFacade.pickTeam(req, testUserDto.getName()));
    }
}
