package cz.muni.fi.pa165.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import cz.muni.fi.pa165.exceptions.UserNotFoundException;
import cz.muni.fi.pa165.exceptions.UsernameAlreadyTakenException;
import cz.muni.fi.pa165.repositories.UserRepository;
import cz.muni.fi.pa165.repositories.models.UserModel;
import cz.muni.pa165.generated.rest.model.UserDTO;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    UserDTO testUserDto;
    UserModel testUserModel;

    @BeforeEach
    void userFacadeTestsSetup() {
        testUserDto = new UserDTO();
        testUserDto.setId(1L);
        testUserDto.setName("Test");

        testUserModel = new UserModel();
        testUserModel.setId(1L);
        testUserModel.setName("Test");
    }

    @Test
    void userExists_existingUser_returnsTrue() {
        Mockito.when(userRepository.findByName(testUserDto.getName()))
            .thenReturn(Optional.of(testUserModel));
        
        assertTrue(userService.userExists(testUserDto));
    }

    @Test
    void userExists_unknownUser_returnsFalse() {
        Mockito.when(userRepository.findByName(testUserDto.getName()))
            .thenReturn(Optional.empty());
        
        assertFalse(userService.userExists(testUserDto));
    }

    @Test
    void createUser_newUser_returnsUser() {
        Mockito.when(userRepository.findByName(testUserDto.getName()))
            .thenReturn(Optional.empty());
        Mockito.when(userRepository.save(testUserModel))
                .thenReturn(testUserModel);

        var user = userService.createUser(testUserModel);

        assertThat(user.getId()).isEqualTo(testUserModel.getId());
    }

    @Test
    void createUser_existingUser_throwsUsernameAlreadyTakenException() {
        Mockito.when(userRepository.findByName(testUserDto.getName()))
            .thenReturn(Optional.of(testUserModel));

        assertThrows(UsernameAlreadyTakenException.class,
            () -> userService.createUser(testUserModel));
    }

    @Test
    void getUserByName_knownUser_returnsUser() {
        Mockito.when(userRepository.findByName(testUserDto.getName()))
            .thenReturn(Optional.of(testUserModel));
        
        var user = userService.getUserByName(testUserDto);

        assertThat(user.getId()).isEqualTo(testUserModel.getId());
    }

    @Test
    void getUserByName_unknownUser_thrownUserNotFoundException() {
        Mockito.when(userRepository.findByName(testUserDto.getName()))
            .thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, 
            () -> userService.getUserByName(testUserDto));
    }
}
