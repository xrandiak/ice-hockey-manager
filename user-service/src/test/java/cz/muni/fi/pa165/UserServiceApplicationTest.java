package cz.muni.fi.pa165;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Unit test for simple UserServiceApplication.
 */

@SpringBootTest(classes = UserServiceApplication.class)
public class UserServiceApplicationTest {
    /**
     * Testing project settings validity
     */
    @Test
    void emptyTest() {
    }
}
