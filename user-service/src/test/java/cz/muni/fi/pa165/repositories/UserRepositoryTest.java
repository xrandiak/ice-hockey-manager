package cz.muni.fi.pa165.repositories;

import cz.muni.fi.pa165.repositories.models.RoleType;
import cz.muni.fi.pa165.repositories.models.UserModel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Robert Randiak
 */

@DataJpaTest
@ImportAutoConfiguration({FeignAutoConfiguration.class})
class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @BeforeAll
    public static void initData(@Autowired UserRepository userRepository,
                                @Autowired TransactionTemplate transactionTemplate
    ) {
        transactionTemplate.execute(status -> {
            UserModel userModel1 = new UserModel();
            userModel1.setId(1L);
            userModel1.setName("Robert");
            userModel1.setRole(RoleType.Admin);
            userModel1.setTeamId(3L);
            userRepository.save(userModel1);

            UserModel userModel2 = new UserModel();
            userModel2.setId(2L);
            userModel2.setName("Andrej");
            userModel2.setRole(RoleType.User);
            userModel2.setDisabled(true);
            userRepository.save(userModel2);

            UserModel userModel3 = new UserModel();
            userModel3.setId(4L);
            userModel3.setName("Pedro");
            userModel3.setRole(RoleType.User);
            userModel3.setTeamId(11L);
            userModel3.setDisabled(false);
            userRepository.save(userModel3);

            return null;
        });
    }

    @Test
    void findById_userFound_returnsNonEmpty() {
        // Act
        Optional<UserModel> foundUser = userRepository.findById(1L);

        // Assert
        assertTrue(foundUser.isPresent());
        assertThat(foundUser.get().getName()).isEqualTo("Robert");
    }

    @Test
    void findByName_userFound_returnsNonEmpty() {
        // Act
        Optional<UserModel> foundUser = userRepository.findByName("Andrej");

        // Assert
        assertTrue(foundUser.isPresent());
        assertThat(foundUser.get().getName()).isEqualTo("Andrej");
    }

    @Test
    void getUsers_filteredUsers_returnsNonEmptyPage() {
        // Act
        Page<UserModel> users = userRepository.getUsers(
                PageRequest.of(0, 10),
                "Robert",
                RoleType.Admin,
                null,
                false);

        // Assert
        assertThat(users).isNotEmpty();
        assertThat(users.getContent()).extracting(UserModel::getName).contains("Robert");
    }

    @Test
    void findById_userNotFound_returnsEmpty() {
        // Act
        Optional<UserModel> foundUser = userRepository.findById(999L);

        // Assert
        assertFalse(foundUser.isPresent());
    }

    @Test
    void findByName_userNotFound_returnsEmpty() {
        // Act
        Optional<UserModel> foundUser = userRepository.findByName("Dominik");

        // Assert
        assertFalse(foundUser.isPresent());
    }

    @Test
    void getUsers_WrongName_returnsEmptyPage() {
        // Act
        Page<UserModel> users = userRepository.getUsers(
                PageRequest.of(0, 10),
                "Dominik",
                RoleType.User,
                null,
                false);

        // Assert
        assertThat(users).isEmpty();
    }

    @Test
    void getUsers_WrongRole_returnsEmptyPage() {
        // Act
        Page<UserModel> users = userRepository.getUsers(
                PageRequest.of(0, 10),
                "Robert",
                RoleType.User, // Wrong role
                null,
                false);

        // Assert
        assertThat(users).isEmpty();
    }

    @Test
    void getUsers_WrongTeamId_returnsEmptyPage() {
        // Act
        Page<UserModel> users = userRepository.getUsers(
                PageRequest.of(0, 10),
                "Robert",
                RoleType.User,
                999, // Wrong team ID
                false);

        // Assert
        assertThat(users).isEmpty();
    }

    @Test
    void getUsers_WrongDisabledFlag_returnsEmptyPage() {
        // Act
        Page<UserModel> users = userRepository.getUsers(
                PageRequest.of(0, 10),
                "Robert",
                RoleType.User,
                null,
                true); // Wrong disabled flag

        // Assert
        assertThat(users).isEmpty();
    }
}