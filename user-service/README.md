### All commands available in Makefile!

### POPULATING AND TRUNCATING THE DATABASE

To seed the database execute command from root of the project :

```cat ./user-service/src/main/resources/static/seed_script.sql | podman exec -i user-service-postgres psql -U userAdmin -d userService```


Vice versa, to remove all the data, execute :

```cat ./user-service/src/main/resources/static/truncate_script.sql | podman exec -i user-service-postgres psql -U userAdmin -d userService```


### DTO class diagram
![User Service Dto Class Diagram](../docs/user_service_dtos_class_diagram.png "Use Case Diagram")
