from locust import HttpUser, between, task, events

teamServiceHost = "localhost:8081"
matchServiceHost = "localhost:8082"
matchStatisticsServiceHost = "localhost:8083"
userServiceHost = "localhost:8080"

@events.init_command_line_parser.add_listener
def _(parser):
    parser.add_argument("--challenger-token", type=str, default="", help="Challenger OAuth2Token")
    parser.add_argument("--opponent-token", type=str, default="", help="Opponent OAuth2Token")

class AuthUser(HttpUser):
    users = {}
    teams = {}
    tokens = {}
    wait_time = between(1, 10)

    def on_start(self):
        self.tokens['challenger'] = self.environment.parsed_options.challenger_token
        self.tokens['opponent'] = self.environment.parsed_options.opponent_token

    @task
    def whole_run(self):
        # register users
        response = self.client.get(f"http://{userServiceHost}/users")
        self.users = {user["name"]: user for user in response.json()["users"]}
        for name in ["challenger", "opponent"]:
            if name in self.users:
                continue
            account_data = { "name" : name}
            response = self.client.post(f"http://{userServiceHost}/user", json=account_data)
            if response.status_code == 201:
                self.users[name] = response.json()
            else:
                print(response.json())
                raise Exception(f"Failed to create {name}")
        
        headers = {
            "challenger": { "Bearer": self.tokens["challenger"] },
            "opponent": { "Bearer": self.tokens["opponent"] }
        }
        # pick teams
        for name in ["challenger", "opponent"]:
            if name in self.users and self.users[name]["teamId"] is not None:
                res = self.client.delete(f"http://{userServiceHost}/user/team", json={"username": name})
                if res.status_code != 200:
                    raise Exception(f"Failed to get teams")

            account_data = { "name" : name}
            
            res = self.client.get(f"http://{teamServiceHost}/teams/get-untaken-teams")
            if res.status_code != 200:
                raise Exception(f"Failed to get teams")
            teams = res.json()

            if len(teams) == 0:
                raise Exception("No teams available")
            
            team_id = teams[0]['id']
            res = self.client.put(f"http://{userServiceHost}/user/team", json={"teamId": team_id, "username": name})
            # res = self.client.put(f"http://{userServiceHost}/user/team", json={"teamId": team_id}, headers = headers[name])
            if res.status_code != 200:
                raise Exception(f"Failed to set team {team_id} for {name}")

            self.teams[name] = teams[0]
        
        # edit teams
        for name in ["challenger", "opponent"]:
            res = self.client.get(f"http://{teamServiceHost}/agents/agents-of-team/{self.teams[name]['id']}")
            if res.status_code != 200:
                raise Exception(f"Failed to get team {self.teams[name]['id']} agents")
            picked = res.json()

            for i in range(min(len(picked), 3)):
                res = self.client.delete(f"http://{userServiceHost}/user/team/player", json={"playerId": picked[i]['id'], "username": name})
                # res = self.client.delete(f"http://{userServiceHost}/user/team/player", json={"playerId": picked[i]['id']}, headers=headers[name])
                if res.status_code != 200:
                    raise Exception(f"Failed to delete player {picked[i]['id']} from team {self.teams[name]['id']}")

            res = self.client.get(f"http://{teamServiceHost}/agents/free-agents")
            if res.status_code != 200:
                raise Exception(f"Failed to get free agents")
            free = res.json()
            
            for i in range(max(len(free), 3)):
                res = self.client.put(f"http://{userServiceHost}/user/team/player", json={"playerId": free[i]['id'], "username": name})
                # res = self.client.put(f"{userServiceHost}/user/team/player", json={"playerId": free[i]['id']}, headers=headers[name])
                if res.status_code != 200:
                    raise Exception(f"Failed to add player {free[i]['id']} from team {self.teams[name]['id']}")

        # challenger propose match
        res = self.client.post(f"http://{matchServiceHost}/match/propose/{self.users['challenger']['id']}/{self.users['opponent']['id']}", headers=headers['opponent'])
        if res.status_code != 200:
            raise Exception(f"Failed to propose match")
        proposed = res.json()

        # opponent reject match
        res = self.client.post(f"http://{matchServiceHost}/match/{proposed['id']}/reject", headers=headers['opponent'])
        if res.status_code != 200:
            raise Exception(f"Failed to reject proposed matches for opponent")
        
        # challenger propose match
        res = self.client.post(f"http://{matchServiceHost}/match/propose/{self.users['challenger']['id']}/{self.users['opponent']['id']}", headers=headers['challenger'])
        if res.status_code != 200:
            raise Exception(f"Failed to propose match")
        proposed = res.json()

        # opponent accept match
        res = self.client.post(f"http://{matchServiceHost}/match/{proposed['id']}/accept", headers=headers['opponent'])
        if res.status_code != 200:
            raise Exception(f"Failed to reject proposed matches for opponent")

        # check accepted
        res = self.client.get(f"http://{matchServiceHost}/matches/accepted", headers=headers['opponent'])
        if res.status_code != 200:
            raise Exception(f"Failed to reject proposed matches for opponent")
        accepted = res.json()

        if len(accepted) == 0:
            raise Exception("No accepted matches")
